package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactData;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactDataRepository extends JpaRepository<ContactData, Long>, JpaSpecificationExecutor<ContactData> {
}

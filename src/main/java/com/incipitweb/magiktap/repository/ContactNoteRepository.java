package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactNote;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactNote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactNoteRepository extends JpaRepository<ContactNote, Long>, JpaSpecificationExecutor<ContactNote> {
}

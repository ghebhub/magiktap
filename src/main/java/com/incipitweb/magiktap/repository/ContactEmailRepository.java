package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactEmail;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactEmail entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactEmailRepository extends JpaRepository<ContactEmail, Long>, JpaSpecificationExecutor<ContactEmail> {
}

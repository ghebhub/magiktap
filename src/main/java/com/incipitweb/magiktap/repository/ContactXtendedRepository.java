package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactXtended;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactXtended entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactXtendedRepository extends JpaRepository<ContactXtended, Long>, JpaSpecificationExecutor<ContactXtended> {
}

package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactMailAddress;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactMailAddress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactMailAddressRepository extends JpaRepository<ContactMailAddress, Long>, JpaSpecificationExecutor<ContactMailAddress> {
}

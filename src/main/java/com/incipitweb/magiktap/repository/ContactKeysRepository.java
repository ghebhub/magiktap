package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactKeys;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactKeys entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactKeysRepository extends JpaRepository<ContactKeys, Long>, JpaSpecificationExecutor<ContactKeys> {
}

package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactCategories;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactCategories entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactCategoriesRepository extends JpaRepository<ContactCategories, Long>, JpaSpecificationExecutor<ContactCategories> {
}

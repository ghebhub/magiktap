package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactPhoneNumber;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactPhoneNumber entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactPhoneNumberRepository extends JpaRepository<ContactPhoneNumber, Long>, JpaSpecificationExecutor<ContactPhoneNumber> {
}

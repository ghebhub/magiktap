package com.incipitweb.magiktap.repository;

import com.incipitweb.magiktap.domain.ContactAgent;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ContactAgent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ContactAgentRepository extends JpaRepository<ContactAgent, Long>, JpaSpecificationExecutor<ContactAgent> {
}

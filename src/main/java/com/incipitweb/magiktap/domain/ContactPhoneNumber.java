package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactPhoneNumber.
 */
@Entity
@Table(name = "contact_phone_number")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactPhoneNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "local_number", length = 255, nullable = false)
    private String localNumber;

    @Column(name = "phone_number_type")
    private String phoneNumberType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactPhoneNumbers", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public ContactPhoneNumber localNumber(String localNumber) {
        this.localNumber = localNumber;
        return this;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    public String getPhoneNumberType() {
        return phoneNumberType;
    }

    public ContactPhoneNumber phoneNumberType(String phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
        return this;
    }

    public void setPhoneNumberType(String phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactPhoneNumber contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactPhoneNumber)) {
            return false;
        }
        return id != null && id.equals(((ContactPhoneNumber) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactPhoneNumber{" +
            "id=" + getId() +
            ", localNumber='" + getLocalNumber() + "'" +
            ", phoneNumberType='" + getPhoneNumberType() + "'" +
            "}";
    }
}

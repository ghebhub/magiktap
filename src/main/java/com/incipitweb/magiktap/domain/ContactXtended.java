package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactXtended.
 */
@Entity
@Table(name = "contact_xtended")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactXtended implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "xname", length = 255, nullable = false)
    private String xname;

    @NotNull
    @Size(max = 255)
    @Column(name = "xvalue", length = 255, nullable = false)
    private String xvalue;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactXtendeds", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getXname() {
        return xname;
    }

    public ContactXtended xname(String xname) {
        this.xname = xname;
        return this;
    }

    public void setXname(String xname) {
        this.xname = xname;
    }

    public String getXvalue() {
        return xvalue;
    }

    public ContactXtended xvalue(String xvalue) {
        this.xvalue = xvalue;
        return this;
    }

    public void setXvalue(String xvalue) {
        this.xvalue = xvalue;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactXtended contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactXtended)) {
            return false;
        }
        return id != null && id.equals(((ContactXtended) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactXtended{" +
            "id=" + getId() +
            ", xname='" + getXname() + "'" +
            ", xvalue='" + getXvalue() + "'" +
            "}";
    }
}

package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactData.
 */
@Entity
@Table(name = "contact_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 10)
    @Column(name = "data_name", length = 10, nullable = false)
    private String dataName;

    @Size(max = 255)
    @Column(name = "url", length = 255)
    private String url;

    @Size(max = 1)
    @Column(name = "inline", length = 1)
    private String inline;

    @NotNull
    @Size(max = 10)
    @Column(name = "contact_types", length = 10, nullable = false)
    private String contactTypes;

    @NotNull
    @Size(max = 10)
    @Column(name = "contact_encoding_types", length = 10, nullable = false)
    private String contactEncodingTypes;

    @Lob
    @Column(name = "contact_data")
    private byte[] contactData;

    @Column(name = "contact_data_content_type")
    private String contactDataContentType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactData", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataName() {
        return dataName;
    }

    public ContactData dataName(String dataName) {
        this.dataName = dataName;
        return this;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getUrl() {
        return url;
    }

    public ContactData url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInline() {
        return inline;
    }

    public ContactData inline(String inline) {
        this.inline = inline;
        return this;
    }

    public void setInline(String inline) {
        this.inline = inline;
    }

    public String getContactTypes() {
        return contactTypes;
    }

    public ContactData contactTypes(String contactTypes) {
        this.contactTypes = contactTypes;
        return this;
    }

    public void setContactTypes(String contactTypes) {
        this.contactTypes = contactTypes;
    }

    public String getContactEncodingTypes() {
        return contactEncodingTypes;
    }

    public ContactData contactEncodingTypes(String contactEncodingTypes) {
        this.contactEncodingTypes = contactEncodingTypes;
        return this;
    }

    public void setContactEncodingTypes(String contactEncodingTypes) {
        this.contactEncodingTypes = contactEncodingTypes;
    }

    public byte[] getContactData() {
        return contactData;
    }

    public ContactData contactData(byte[] contactData) {
        this.contactData = contactData;
        return this;
    }

    public void setContactData(byte[] contactData) {
        this.contactData = contactData;
    }

    public String getContactDataContentType() {
        return contactDataContentType;
    }

    public ContactData contactDataContentType(String contactDataContentType) {
        this.contactDataContentType = contactDataContentType;
        return this;
    }

    public void setContactDataContentType(String contactDataContentType) {
        this.contactDataContentType = contactDataContentType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactData contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactData)) {
            return false;
        }
        return id != null && id.equals(((ContactData) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactData{" +
            "id=" + getId() +
            ", dataName='" + getDataName() + "'" +
            ", url='" + getUrl() + "'" +
            ", inline='" + getInline() + "'" +
            ", contactTypes='" + getContactTypes() + "'" +
            ", contactEncodingTypes='" + getContactEncodingTypes() + "'" +
            ", contactData='" + getContactData() + "'" +
            ", contactDataContentType='" + getContactDataContentType() + "'" +
            "}";
    }
}

package com.incipitweb.magiktap.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Contact.
 */
@Entity
@Table(name = "contact")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 36)
    @Column(name = "contact_id", length = 36, nullable = false)
    private String contactId;

    @NotNull
    @Size(max = 255)
    @Column(name = "fn", length = 255, nullable = false)
    private String fn;

    @NotNull
    @Size(max = 255)
    @Column(name = "n", length = 255, nullable = false)
    private String n;

    @Size(max = 255)
    @Column(name = "nickname", length = 255)
    private String nickname;

    @NotNull
    @Column(name = "bday", nullable = false)
    private Instant bday;

    @Size(max = 50)
    @Column(name = "mailer", length = 50)
    private String mailer;

    @Column(name = "tz")
    private Integer tz;

    @Column(name = "geo_lat")
    private Double geoLat;

    @Column(name = "geo_long")
    private Double geoLong;

    @Size(max = 50)
    @Column(name = "title", length = 50)
    private String title;

    @Size(max = 50)
    @Column(name = "role", length = 50)
    private String role;

    @Size(max = 255)
    @Column(name = "prod_id", length = 255)
    private String prodId;

    @Size(max = 50)
    @Column(name = "rev", length = 50)
    private String rev;

    @Size(max = 50)
    @Column(name = "sort_string", length = 50)
    private String sortString;

    @Size(max = 255)
    @Column(name = "uid", length = 255)
    private String uid;

    @Size(max = 255)
    @Column(name = "url", length = 255)
    private String url;

    @Size(max = 10)
    @Column(name = "version", length = 10)
    private String version;

    @Size(max = 50)
    @Column(name = "class_name", length = 50)
    private String className;

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactAgent> contactAgents = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactCategories> contactCategories = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactData> contactData = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactEmail> contactEmails = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactKeys> contactKeys = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactMailAddress> contactMailAddresses = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactNote> contactNotes = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactPhoneNumber> contactPhoneNumbers = new HashSet<>();

    @OneToMany(mappedBy = "contact")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<ContactXtended> contactXtendeds = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContactId() {
        return contactId;
    }

    public Contact contactId(String contactId) {
        this.contactId = contactId;
        return this;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getFn() {
        return fn;
    }

    public Contact fn(String fn) {
        this.fn = fn;
        return this;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public String getN() {
        return n;
    }

    public Contact n(String n) {
        this.n = n;
        return this;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getNickname() {
        return nickname;
    }

    public Contact nickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Instant getBday() {
        return bday;
    }

    public Contact bday(Instant bday) {
        this.bday = bday;
        return this;
    }

    public void setBday(Instant bday) {
        this.bday = bday;
    }

    public String getMailer() {
        return mailer;
    }

    public Contact mailer(String mailer) {
        this.mailer = mailer;
        return this;
    }

    public void setMailer(String mailer) {
        this.mailer = mailer;
    }

    public Integer getTz() {
        return tz;
    }

    public Contact tz(Integer tz) {
        this.tz = tz;
        return this;
    }

    public void setTz(Integer tz) {
        this.tz = tz;
    }

    public Double getGeoLat() {
        return geoLat;
    }

    public Contact geoLat(Double geoLat) {
        this.geoLat = geoLat;
        return this;
    }

    public void setGeoLat(Double geoLat) {
        this.geoLat = geoLat;
    }

    public Double getGeoLong() {
        return geoLong;
    }

    public Contact geoLong(Double geoLong) {
        this.geoLong = geoLong;
        return this;
    }

    public void setGeoLong(Double geoLong) {
        this.geoLong = geoLong;
    }

    public String getTitle() {
        return title;
    }

    public Contact title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRole() {
        return role;
    }

    public Contact role(String role) {
        this.role = role;
        return this;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProdId() {
        return prodId;
    }

    public Contact prodId(String prodId) {
        this.prodId = prodId;
        return this;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getRev() {
        return rev;
    }

    public Contact rev(String rev) {
        this.rev = rev;
        return this;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public String getSortString() {
        return sortString;
    }

    public Contact sortString(String sortString) {
        this.sortString = sortString;
        return this;
    }

    public void setSortString(String sortString) {
        this.sortString = sortString;
    }

    public String getUid() {
        return uid;
    }

    public Contact uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public Contact url(String url) {
        this.url = url;
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public Contact version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getClassName() {
        return className;
    }

    public Contact className(String className) {
        this.className = className;
        return this;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Set<ContactAgent> getContactAgents() {
        return contactAgents;
    }

    public Contact contactAgents(Set<ContactAgent> contactAgents) {
        this.contactAgents = contactAgents;
        return this;
    }

    public Contact addContactAgent(ContactAgent contactAgent) {
        this.contactAgents.add(contactAgent);
        contactAgent.setContact(this);
        return this;
    }

    public Contact removeContactAgent(ContactAgent contactAgent) {
        this.contactAgents.remove(contactAgent);
        contactAgent.setContact(null);
        return this;
    }

    public void setContactAgents(Set<ContactAgent> contactAgents) {
        this.contactAgents = contactAgents;
    }

    public Set<ContactCategories> getContactCategories() {
        return contactCategories;
    }

    public Contact contactCategories(Set<ContactCategories> contactCategories) {
        this.contactCategories = contactCategories;
        return this;
    }

    public Contact addContactCategories(ContactCategories contactCategories) {
        this.contactCategories.add(contactCategories);
        contactCategories.setContact(this);
        return this;
    }

    public Contact removeContactCategories(ContactCategories contactCategories) {
        this.contactCategories.remove(contactCategories);
        contactCategories.setContact(null);
        return this;
    }

    public void setContactCategories(Set<ContactCategories> contactCategories) {
        this.contactCategories = contactCategories;
    }

    public Set<ContactData> getContactData() {
        return contactData;
    }

    public Contact contactData(Set<ContactData> contactData) {
        this.contactData = contactData;
        return this;
    }

    public Contact addContactData(ContactData contactData) {
        this.contactData.add(contactData);
        contactData.setContact(this);
        return this;
    }

    public Contact removeContactData(ContactData contactData) {
        this.contactData.remove(contactData);
        contactData.setContact(null);
        return this;
    }

    public void setContactData(Set<ContactData> contactData) {
        this.contactData = contactData;
    }

    public Set<ContactEmail> getContactEmails() {
        return contactEmails;
    }

    public Contact contactEmails(Set<ContactEmail> contactEmails) {
        this.contactEmails = contactEmails;
        return this;
    }

    public Contact addContactEmail(ContactEmail contactEmail) {
        this.contactEmails.add(contactEmail);
        contactEmail.setContact(this);
        return this;
    }

    public Contact removeContactEmail(ContactEmail contactEmail) {
        this.contactEmails.remove(contactEmail);
        contactEmail.setContact(null);
        return this;
    }

    public void setContactEmails(Set<ContactEmail> contactEmails) {
        this.contactEmails = contactEmails;
    }

    public Set<ContactKeys> getContactKeys() {
        return contactKeys;
    }

    public Contact contactKeys(Set<ContactKeys> contactKeys) {
        this.contactKeys = contactKeys;
        return this;
    }

    public Contact addContactKeys(ContactKeys contactKeys) {
        this.contactKeys.add(contactKeys);
        contactKeys.setContact(this);
        return this;
    }

    public Contact removeContactKeys(ContactKeys contactKeys) {
        this.contactKeys.remove(contactKeys);
        contactKeys.setContact(null);
        return this;
    }

    public void setContactKeys(Set<ContactKeys> contactKeys) {
        this.contactKeys = contactKeys;
    }

    public Set<ContactMailAddress> getContactMailAddresses() {
        return contactMailAddresses;
    }

    public Contact contactMailAddresses(Set<ContactMailAddress> contactMailAddresses) {
        this.contactMailAddresses = contactMailAddresses;
        return this;
    }

    public Contact addContactMailAddress(ContactMailAddress contactMailAddress) {
        this.contactMailAddresses.add(contactMailAddress);
        contactMailAddress.setContact(this);
        return this;
    }

    public Contact removeContactMailAddress(ContactMailAddress contactMailAddress) {
        this.contactMailAddresses.remove(contactMailAddress);
        contactMailAddress.setContact(null);
        return this;
    }

    public void setContactMailAddresses(Set<ContactMailAddress> contactMailAddresses) {
        this.contactMailAddresses = contactMailAddresses;
    }

    public Set<ContactNote> getContactNotes() {
        return contactNotes;
    }

    public Contact contactNotes(Set<ContactNote> contactNotes) {
        this.contactNotes = contactNotes;
        return this;
    }

    public Contact addContactNote(ContactNote contactNote) {
        this.contactNotes.add(contactNote);
        contactNote.setContact(this);
        return this;
    }

    public Contact removeContactNote(ContactNote contactNote) {
        this.contactNotes.remove(contactNote);
        contactNote.setContact(null);
        return this;
    }

    public void setContactNotes(Set<ContactNote> contactNotes) {
        this.contactNotes = contactNotes;
    }

    public Set<ContactPhoneNumber> getContactPhoneNumbers() {
        return contactPhoneNumbers;
    }

    public Contact contactPhoneNumbers(Set<ContactPhoneNumber> contactPhoneNumbers) {
        this.contactPhoneNumbers = contactPhoneNumbers;
        return this;
    }

    public Contact addContactPhoneNumber(ContactPhoneNumber contactPhoneNumber) {
        this.contactPhoneNumbers.add(contactPhoneNumber);
        contactPhoneNumber.setContact(this);
        return this;
    }

    public Contact removeContactPhoneNumber(ContactPhoneNumber contactPhoneNumber) {
        this.contactPhoneNumbers.remove(contactPhoneNumber);
        contactPhoneNumber.setContact(null);
        return this;
    }

    public void setContactPhoneNumbers(Set<ContactPhoneNumber> contactPhoneNumbers) {
        this.contactPhoneNumbers = contactPhoneNumbers;
    }

    public Set<ContactXtended> getContactXtendeds() {
        return contactXtendeds;
    }

    public Contact contactXtendeds(Set<ContactXtended> contactXtendeds) {
        this.contactXtendeds = contactXtendeds;
        return this;
    }

    public Contact addContactXtended(ContactXtended contactXtended) {
        this.contactXtendeds.add(contactXtended);
        contactXtended.setContact(this);
        return this;
    }

    public Contact removeContactXtended(ContactXtended contactXtended) {
        this.contactXtendeds.remove(contactXtended);
        contactXtended.setContact(null);
        return this;
    }

    public void setContactXtendeds(Set<ContactXtended> contactXtendeds) {
        this.contactXtendeds = contactXtendeds;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contact)) {
            return false;
        }
        return id != null && id.equals(((Contact) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Contact{" +
            "id=" + getId() +
            ", contactId='" + getContactId() + "'" +
            ", fn='" + getFn() + "'" +
            ", n='" + getN() + "'" +
            ", nickname='" + getNickname() + "'" +
            ", bday='" + getBday() + "'" +
            ", mailer='" + getMailer() + "'" +
            ", tz=" + getTz() +
            ", geoLat=" + getGeoLat() +
            ", geoLong=" + getGeoLong() +
            ", title='" + getTitle() + "'" +
            ", role='" + getRole() + "'" +
            ", prodId='" + getProdId() + "'" +
            ", rev='" + getRev() + "'" +
            ", sortString='" + getSortString() + "'" +
            ", uid='" + getUid() + "'" +
            ", url='" + getUrl() + "'" +
            ", version='" + getVersion() + "'" +
            ", className='" + getClassName() + "'" +
            "}";
    }
}

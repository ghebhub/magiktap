package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactNote.
 */
@Entity
@Table(name = "contact_note")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactNote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "note", length = 255, nullable = false)
    private String note;

    @Column(name = "contact_note_type")
    private String contactNoteType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactNotes", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public ContactNote note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getContactNoteType() {
        return contactNoteType;
    }

    public ContactNote contactNoteType(String contactNoteType) {
        this.contactNoteType = contactNoteType;
        return this;
    }

    public void setContactNoteType(String contactNoteType) {
        this.contactNoteType = contactNoteType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactNote contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactNote)) {
            return false;
        }
        return id != null && id.equals(((ContactNote) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactNote{" +
            "id=" + getId() +
            ", note='" + getNote() + "'" +
            ", contactNoteType='" + getContactNoteType() + "'" +
            "}";
    }
}

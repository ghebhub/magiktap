package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactKeys.
 */
@Entity
@Table(name = "contact_keys")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactKeys implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "key_data", length = 255, nullable = false)
    private String keyData;

    @Column(name = "key_type")
    private String keyType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactKeys", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyData() {
        return keyData;
    }

    public ContactKeys keyData(String keyData) {
        this.keyData = keyData;
        return this;
    }

    public void setKeyData(String keyData) {
        this.keyData = keyData;
    }

    public String getKeyType() {
        return keyType;
    }

    public ContactKeys keyType(String keyType) {
        this.keyType = keyType;
        return this;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactKeys contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactKeys)) {
            return false;
        }
        return id != null && id.equals(((ContactKeys) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactKeys{" +
            "id=" + getId() +
            ", keyData='" + getKeyData() + "'" +
            ", keyType='" + getKeyType() + "'" +
            "}";
    }
}

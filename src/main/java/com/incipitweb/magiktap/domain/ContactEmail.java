package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactEmail.
 */
@Entity
@Table(name = "contact_email")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactEmail implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255)
    @Column(name = "email_address", length = 255, nullable = false)
    private String emailAddress;

    @Column(name = "email_type")
    private String emailType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactEmails", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public ContactEmail emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailType() {
        return emailType;
    }

    public ContactEmail emailType(String emailType) {
        this.emailType = emailType;
        return this;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactEmail contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactEmail)) {
            return false;
        }
        return id != null && id.equals(((ContactEmail) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactEmail{" +
            "id=" + getId() +
            ", emailAddress='" + getEmailAddress() + "'" +
            ", emailType='" + getEmailType() + "'" +
            "}";
    }
}

package com.incipitweb.magiktap.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A ContactMailAddress.
 */
@Entity
@Table(name = "contact_mail_address")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ContactMailAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 30)
    @Column(name = "pobox", length = 30)
    private String pobox;

    @Size(max = 255)
    @Column(name = "extended_address", length = 255)
    private String extendedAddress;

    @NotNull
    @Size(max = 255)
    @Column(name = "street", length = 255, nullable = false)
    private String street;

    @Size(max = 50)
    @Column(name = "locality", length = 50)
    private String locality;

    @Size(max = 50)
    @Column(name = "region", length = 50)
    private String region;

    @Size(max = 30)
    @Column(name = "postal_code", length = 30)
    private String postalCode;

    @Size(max = 50)
    @Column(name = "country", length = 50)
    private String country;

    @Column(name = "mail_address_type")
    private String mailAddressType;

    @ManyToOne
    @JsonIgnoreProperties(value = "contactMailAddresses", allowSetters = true)
    private Contact contact;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPobox() {
        return pobox;
    }

    public ContactMailAddress pobox(String pobox) {
        this.pobox = pobox;
        return this;
    }

    public void setPobox(String pobox) {
        this.pobox = pobox;
    }

    public String getExtendedAddress() {
        return extendedAddress;
    }

    public ContactMailAddress extendedAddress(String extendedAddress) {
        this.extendedAddress = extendedAddress;
        return this;
    }

    public void setExtendedAddress(String extendedAddress) {
        this.extendedAddress = extendedAddress;
    }

    public String getStreet() {
        return street;
    }

    public ContactMailAddress street(String street) {
        this.street = street;
        return this;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLocality() {
        return locality;
    }

    public ContactMailAddress locality(String locality) {
        this.locality = locality;
        return this;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public ContactMailAddress region(String region) {
        this.region = region;
        return this;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public ContactMailAddress postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public ContactMailAddress country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMailAddressType() {
        return mailAddressType;
    }

    public ContactMailAddress mailAddressType(String mailAddressType) {
        this.mailAddressType = mailAddressType;
        return this;
    }

    public void setMailAddressType(String mailAddressType) {
        this.mailAddressType = mailAddressType;
    }

    public Contact getContact() {
        return contact;
    }

    public ContactMailAddress contact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactMailAddress)) {
            return false;
        }
        return id != null && id.equals(((ContactMailAddress) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMailAddress{" +
            "id=" + getId() +
            ", pobox='" + getPobox() + "'" +
            ", extendedAddress='" + getExtendedAddress() + "'" +
            ", street='" + getStreet() + "'" +
            ", locality='" + getLocality() + "'" +
            ", region='" + getRegion() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", mailAddressType='" + getMailAddressType() + "'" +
            "}";
    }
}

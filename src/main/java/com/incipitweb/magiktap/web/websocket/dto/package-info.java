/**
 * Data Access Objects used by WebSocket services.
 */
package com.incipitweb.magiktap.web.websocket.dto;

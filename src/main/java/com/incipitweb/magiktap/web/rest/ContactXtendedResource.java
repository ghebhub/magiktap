package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactXtendedService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactXtendedDTO;
import com.incipitweb.magiktap.service.dto.ContactXtendedCriteria;
import com.incipitweb.magiktap.service.ContactXtendedQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactXtended}.
 */
@RestController
@RequestMapping("/api")
public class ContactXtendedResource {

    private final Logger log = LoggerFactory.getLogger(ContactXtendedResource.class);

    private static final String ENTITY_NAME = "contactXtended";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactXtendedService contactXtendedService;

    private final ContactXtendedQueryService contactXtendedQueryService;

    public ContactXtendedResource(ContactXtendedService contactXtendedService, ContactXtendedQueryService contactXtendedQueryService) {
        this.contactXtendedService = contactXtendedService;
        this.contactXtendedQueryService = contactXtendedQueryService;
    }

    /**
     * {@code POST  /contact-xtendeds} : Create a new contactXtended.
     *
     * @param contactXtendedDTO the contactXtendedDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactXtendedDTO, or with status {@code 400 (Bad Request)} if the contactXtended has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-xtendeds")
    public ResponseEntity<ContactXtendedDTO> createContactXtended(@Valid @RequestBody ContactXtendedDTO contactXtendedDTO) throws URISyntaxException {
        log.debug("REST request to save ContactXtended : {}", contactXtendedDTO);
        if (contactXtendedDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactXtended cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactXtendedDTO result = contactXtendedService.save(contactXtendedDTO);
        return ResponseEntity.created(new URI("/api/contact-xtendeds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-xtendeds} : Updates an existing contactXtended.
     *
     * @param contactXtendedDTO the contactXtendedDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactXtendedDTO,
     * or with status {@code 400 (Bad Request)} if the contactXtendedDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactXtendedDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-xtendeds")
    public ResponseEntity<ContactXtendedDTO> updateContactXtended(@Valid @RequestBody ContactXtendedDTO contactXtendedDTO) throws URISyntaxException {
        log.debug("REST request to update ContactXtended : {}", contactXtendedDTO);
        if (contactXtendedDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactXtendedDTO result = contactXtendedService.save(contactXtendedDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactXtendedDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-xtendeds} : get all the contactXtendeds.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactXtendeds in body.
     */
    @GetMapping("/contact-xtendeds")
    public ResponseEntity<List<ContactXtendedDTO>> getAllContactXtendeds(ContactXtendedCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactXtendeds by criteria: {}", criteria);
        Page<ContactXtendedDTO> page = contactXtendedQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-xtendeds/count} : count all the contactXtendeds.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-xtendeds/count")
    public ResponseEntity<Long> countContactXtendeds(ContactXtendedCriteria criteria) {
        log.debug("REST request to count ContactXtendeds by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactXtendedQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-xtendeds/:id} : get the "id" contactXtended.
     *
     * @param id the id of the contactXtendedDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactXtendedDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-xtendeds/{id}")
    public ResponseEntity<ContactXtendedDTO> getContactXtended(@PathVariable Long id) {
        log.debug("REST request to get ContactXtended : {}", id);
        Optional<ContactXtendedDTO> contactXtendedDTO = contactXtendedService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactXtendedDTO);
    }

    /**
     * {@code DELETE  /contact-xtendeds/:id} : delete the "id" contactXtended.
     *
     * @param id the id of the contactXtendedDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-xtendeds/{id}")
    public ResponseEntity<Void> deleteContactXtended(@PathVariable Long id) {
        log.debug("REST request to delete ContactXtended : {}", id);
        contactXtendedService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactDataService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactDataDTO;
import com.incipitweb.magiktap.service.dto.ContactDataCriteria;
import com.incipitweb.magiktap.service.ContactDataQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactData}.
 */
@RestController
@RequestMapping("/api")
public class ContactDataResource {

    private final Logger log = LoggerFactory.getLogger(ContactDataResource.class);

    private static final String ENTITY_NAME = "contactData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactDataService contactDataService;

    private final ContactDataQueryService contactDataQueryService;

    public ContactDataResource(ContactDataService contactDataService, ContactDataQueryService contactDataQueryService) {
        this.contactDataService = contactDataService;
        this.contactDataQueryService = contactDataQueryService;
    }

    /**
     * {@code POST  /contact-data} : Create a new contactData.
     *
     * @param contactDataDTO the contactDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactDataDTO, or with status {@code 400 (Bad Request)} if the contactData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-data")
    public ResponseEntity<ContactDataDTO> createContactData(@Valid @RequestBody ContactDataDTO contactDataDTO) throws URISyntaxException {
        log.debug("REST request to save ContactData : {}", contactDataDTO);
        if (contactDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactDataDTO result = contactDataService.save(contactDataDTO);
        return ResponseEntity.created(new URI("/api/contact-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-data} : Updates an existing contactData.
     *
     * @param contactDataDTO the contactDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactDataDTO,
     * or with status {@code 400 (Bad Request)} if the contactDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-data")
    public ResponseEntity<ContactDataDTO> updateContactData(@Valid @RequestBody ContactDataDTO contactDataDTO) throws URISyntaxException {
        log.debug("REST request to update ContactData : {}", contactDataDTO);
        if (contactDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactDataDTO result = contactDataService.save(contactDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-data} : get all the contactData.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactData in body.
     */
    @GetMapping("/contact-data")
    public ResponseEntity<List<ContactDataDTO>> getAllContactData(ContactDataCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactData by criteria: {}", criteria);
        Page<ContactDataDTO> page = contactDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-data/count} : count all the contactData.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-data/count")
    public ResponseEntity<Long> countContactData(ContactDataCriteria criteria) {
        log.debug("REST request to count ContactData by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-data/:id} : get the "id" contactData.
     *
     * @param id the id of the contactDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-data/{id}")
    public ResponseEntity<ContactDataDTO> getContactData(@PathVariable Long id) {
        log.debug("REST request to get ContactData : {}", id);
        Optional<ContactDataDTO> contactDataDTO = contactDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactDataDTO);
    }

    /**
     * {@code DELETE  /contact-data/:id} : delete the "id" contactData.
     *
     * @param id the id of the contactDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-data/{id}")
    public ResponseEntity<Void> deleteContactData(@PathVariable Long id) {
        log.debug("REST request to delete ContactData : {}", id);
        contactDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

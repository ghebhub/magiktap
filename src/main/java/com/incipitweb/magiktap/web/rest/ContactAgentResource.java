package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactAgentService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactAgentDTO;
import com.incipitweb.magiktap.service.dto.ContactAgentCriteria;
import com.incipitweb.magiktap.service.ContactAgentQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactAgent}.
 */
@RestController
@RequestMapping("/api")
public class ContactAgentResource {

    private final Logger log = LoggerFactory.getLogger(ContactAgentResource.class);

    private static final String ENTITY_NAME = "contactAgent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactAgentService contactAgentService;

    private final ContactAgentQueryService contactAgentQueryService;

    public ContactAgentResource(ContactAgentService contactAgentService, ContactAgentQueryService contactAgentQueryService) {
        this.contactAgentService = contactAgentService;
        this.contactAgentQueryService = contactAgentQueryService;
    }

    /**
     * {@code POST  /contact-agents} : Create a new contactAgent.
     *
     * @param contactAgentDTO the contactAgentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactAgentDTO, or with status {@code 400 (Bad Request)} if the contactAgent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-agents")
    public ResponseEntity<ContactAgentDTO> createContactAgent(@Valid @RequestBody ContactAgentDTO contactAgentDTO) throws URISyntaxException {
        log.debug("REST request to save ContactAgent : {}", contactAgentDTO);
        if (contactAgentDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactAgent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactAgentDTO result = contactAgentService.save(contactAgentDTO);
        return ResponseEntity.created(new URI("/api/contact-agents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-agents} : Updates an existing contactAgent.
     *
     * @param contactAgentDTO the contactAgentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactAgentDTO,
     * or with status {@code 400 (Bad Request)} if the contactAgentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactAgentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-agents")
    public ResponseEntity<ContactAgentDTO> updateContactAgent(@Valid @RequestBody ContactAgentDTO contactAgentDTO) throws URISyntaxException {
        log.debug("REST request to update ContactAgent : {}", contactAgentDTO);
        if (contactAgentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactAgentDTO result = contactAgentService.save(contactAgentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactAgentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-agents} : get all the contactAgents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactAgents in body.
     */
    @GetMapping("/contact-agents")
    public ResponseEntity<List<ContactAgentDTO>> getAllContactAgents(ContactAgentCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactAgents by criteria: {}", criteria);
        Page<ContactAgentDTO> page = contactAgentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-agents/count} : count all the contactAgents.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-agents/count")
    public ResponseEntity<Long> countContactAgents(ContactAgentCriteria criteria) {
        log.debug("REST request to count ContactAgents by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactAgentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-agents/:id} : get the "id" contactAgent.
     *
     * @param id the id of the contactAgentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactAgentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-agents/{id}")
    public ResponseEntity<ContactAgentDTO> getContactAgent(@PathVariable Long id) {
        log.debug("REST request to get ContactAgent : {}", id);
        Optional<ContactAgentDTO> contactAgentDTO = contactAgentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactAgentDTO);
    }

    /**
     * {@code DELETE  /contact-agents/:id} : delete the "id" contactAgent.
     *
     * @param id the id of the contactAgentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-agents/{id}")
    public ResponseEntity<Void> deleteContactAgent(@PathVariable Long id) {
        log.debug("REST request to delete ContactAgent : {}", id);
        contactAgentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

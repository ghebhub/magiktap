package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactPhoneNumberService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberDTO;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberCriteria;
import com.incipitweb.magiktap.service.ContactPhoneNumberQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactPhoneNumber}.
 */
@RestController
@RequestMapping("/api")
public class ContactPhoneNumberResource {

    private final Logger log = LoggerFactory.getLogger(ContactPhoneNumberResource.class);

    private static final String ENTITY_NAME = "contactPhoneNumber";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactPhoneNumberService contactPhoneNumberService;

    private final ContactPhoneNumberQueryService contactPhoneNumberQueryService;

    public ContactPhoneNumberResource(ContactPhoneNumberService contactPhoneNumberService, ContactPhoneNumberQueryService contactPhoneNumberQueryService) {
        this.contactPhoneNumberService = contactPhoneNumberService;
        this.contactPhoneNumberQueryService = contactPhoneNumberQueryService;
    }

    /**
     * {@code POST  /contact-phone-numbers} : Create a new contactPhoneNumber.
     *
     * @param contactPhoneNumberDTO the contactPhoneNumberDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactPhoneNumberDTO, or with status {@code 400 (Bad Request)} if the contactPhoneNumber has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-phone-numbers")
    public ResponseEntity<ContactPhoneNumberDTO> createContactPhoneNumber(@Valid @RequestBody ContactPhoneNumberDTO contactPhoneNumberDTO) throws URISyntaxException {
        log.debug("REST request to save ContactPhoneNumber : {}", contactPhoneNumberDTO);
        if (contactPhoneNumberDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactPhoneNumber cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactPhoneNumberDTO result = contactPhoneNumberService.save(contactPhoneNumberDTO);
        return ResponseEntity.created(new URI("/api/contact-phone-numbers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-phone-numbers} : Updates an existing contactPhoneNumber.
     *
     * @param contactPhoneNumberDTO the contactPhoneNumberDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactPhoneNumberDTO,
     * or with status {@code 400 (Bad Request)} if the contactPhoneNumberDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactPhoneNumberDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-phone-numbers")
    public ResponseEntity<ContactPhoneNumberDTO> updateContactPhoneNumber(@Valid @RequestBody ContactPhoneNumberDTO contactPhoneNumberDTO) throws URISyntaxException {
        log.debug("REST request to update ContactPhoneNumber : {}", contactPhoneNumberDTO);
        if (contactPhoneNumberDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactPhoneNumberDTO result = contactPhoneNumberService.save(contactPhoneNumberDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactPhoneNumberDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-phone-numbers} : get all the contactPhoneNumbers.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactPhoneNumbers in body.
     */
    @GetMapping("/contact-phone-numbers")
    public ResponseEntity<List<ContactPhoneNumberDTO>> getAllContactPhoneNumbers(ContactPhoneNumberCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactPhoneNumbers by criteria: {}", criteria);
        Page<ContactPhoneNumberDTO> page = contactPhoneNumberQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-phone-numbers/count} : count all the contactPhoneNumbers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-phone-numbers/count")
    public ResponseEntity<Long> countContactPhoneNumbers(ContactPhoneNumberCriteria criteria) {
        log.debug("REST request to count ContactPhoneNumbers by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactPhoneNumberQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-phone-numbers/:id} : get the "id" contactPhoneNumber.
     *
     * @param id the id of the contactPhoneNumberDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactPhoneNumberDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-phone-numbers/{id}")
    public ResponseEntity<ContactPhoneNumberDTO> getContactPhoneNumber(@PathVariable Long id) {
        log.debug("REST request to get ContactPhoneNumber : {}", id);
        Optional<ContactPhoneNumberDTO> contactPhoneNumberDTO = contactPhoneNumberService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactPhoneNumberDTO);
    }

    /**
     * {@code DELETE  /contact-phone-numbers/:id} : delete the "id" contactPhoneNumber.
     *
     * @param id the id of the contactPhoneNumberDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-phone-numbers/{id}")
    public ResponseEntity<Void> deleteContactPhoneNumber(@PathVariable Long id) {
        log.debug("REST request to delete ContactPhoneNumber : {}", id);
        contactPhoneNumberService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

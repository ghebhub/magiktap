/**
 * View Models used by Spring MVC REST controllers.
 */
package com.incipitweb.magiktap.web.rest.vm;

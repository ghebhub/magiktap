package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactCategoriesService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactCategoriesDTO;
import com.incipitweb.magiktap.service.dto.ContactCategoriesCriteria;
import com.incipitweb.magiktap.service.ContactCategoriesQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactCategories}.
 */
@RestController
@RequestMapping("/api")
public class ContactCategoriesResource {

    private final Logger log = LoggerFactory.getLogger(ContactCategoriesResource.class);

    private static final String ENTITY_NAME = "contactCategories";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactCategoriesService contactCategoriesService;

    private final ContactCategoriesQueryService contactCategoriesQueryService;

    public ContactCategoriesResource(ContactCategoriesService contactCategoriesService, ContactCategoriesQueryService contactCategoriesQueryService) {
        this.contactCategoriesService = contactCategoriesService;
        this.contactCategoriesQueryService = contactCategoriesQueryService;
    }

    /**
     * {@code POST  /contact-categories} : Create a new contactCategories.
     *
     * @param contactCategoriesDTO the contactCategoriesDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactCategoriesDTO, or with status {@code 400 (Bad Request)} if the contactCategories has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-categories")
    public ResponseEntity<ContactCategoriesDTO> createContactCategories(@Valid @RequestBody ContactCategoriesDTO contactCategoriesDTO) throws URISyntaxException {
        log.debug("REST request to save ContactCategories : {}", contactCategoriesDTO);
        if (contactCategoriesDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactCategories cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactCategoriesDTO result = contactCategoriesService.save(contactCategoriesDTO);
        return ResponseEntity.created(new URI("/api/contact-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-categories} : Updates an existing contactCategories.
     *
     * @param contactCategoriesDTO the contactCategoriesDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactCategoriesDTO,
     * or with status {@code 400 (Bad Request)} if the contactCategoriesDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactCategoriesDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-categories")
    public ResponseEntity<ContactCategoriesDTO> updateContactCategories(@Valid @RequestBody ContactCategoriesDTO contactCategoriesDTO) throws URISyntaxException {
        log.debug("REST request to update ContactCategories : {}", contactCategoriesDTO);
        if (contactCategoriesDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactCategoriesDTO result = contactCategoriesService.save(contactCategoriesDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactCategoriesDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-categories} : get all the contactCategories.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactCategories in body.
     */
    @GetMapping("/contact-categories")
    public ResponseEntity<List<ContactCategoriesDTO>> getAllContactCategories(ContactCategoriesCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactCategories by criteria: {}", criteria);
        Page<ContactCategoriesDTO> page = contactCategoriesQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-categories/count} : count all the contactCategories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-categories/count")
    public ResponseEntity<Long> countContactCategories(ContactCategoriesCriteria criteria) {
        log.debug("REST request to count ContactCategories by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactCategoriesQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-categories/:id} : get the "id" contactCategories.
     *
     * @param id the id of the contactCategoriesDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactCategoriesDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-categories/{id}")
    public ResponseEntity<ContactCategoriesDTO> getContactCategories(@PathVariable Long id) {
        log.debug("REST request to get ContactCategories : {}", id);
        Optional<ContactCategoriesDTO> contactCategoriesDTO = contactCategoriesService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactCategoriesDTO);
    }

    /**
     * {@code DELETE  /contact-categories/:id} : delete the "id" contactCategories.
     *
     * @param id the id of the contactCategoriesDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-categories/{id}")
    public ResponseEntity<Void> deleteContactCategories(@PathVariable Long id) {
        log.debug("REST request to delete ContactCategories : {}", id);
        contactCategoriesService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

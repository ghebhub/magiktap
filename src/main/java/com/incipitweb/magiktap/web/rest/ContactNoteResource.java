package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactNoteService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactNoteDTO;
import com.incipitweb.magiktap.service.dto.ContactNoteCriteria;
import com.incipitweb.magiktap.service.ContactNoteQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactNote}.
 */
@RestController
@RequestMapping("/api")
public class ContactNoteResource {

    private final Logger log = LoggerFactory.getLogger(ContactNoteResource.class);

    private static final String ENTITY_NAME = "contactNote";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactNoteService contactNoteService;

    private final ContactNoteQueryService contactNoteQueryService;

    public ContactNoteResource(ContactNoteService contactNoteService, ContactNoteQueryService contactNoteQueryService) {
        this.contactNoteService = contactNoteService;
        this.contactNoteQueryService = contactNoteQueryService;
    }

    /**
     * {@code POST  /contact-notes} : Create a new contactNote.
     *
     * @param contactNoteDTO the contactNoteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactNoteDTO, or with status {@code 400 (Bad Request)} if the contactNote has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-notes")
    public ResponseEntity<ContactNoteDTO> createContactNote(@Valid @RequestBody ContactNoteDTO contactNoteDTO) throws URISyntaxException {
        log.debug("REST request to save ContactNote : {}", contactNoteDTO);
        if (contactNoteDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactNote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactNoteDTO result = contactNoteService.save(contactNoteDTO);
        return ResponseEntity.created(new URI("/api/contact-notes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-notes} : Updates an existing contactNote.
     *
     * @param contactNoteDTO the contactNoteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactNoteDTO,
     * or with status {@code 400 (Bad Request)} if the contactNoteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactNoteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-notes")
    public ResponseEntity<ContactNoteDTO> updateContactNote(@Valid @RequestBody ContactNoteDTO contactNoteDTO) throws URISyntaxException {
        log.debug("REST request to update ContactNote : {}", contactNoteDTO);
        if (contactNoteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactNoteDTO result = contactNoteService.save(contactNoteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactNoteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-notes} : get all the contactNotes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactNotes in body.
     */
    @GetMapping("/contact-notes")
    public ResponseEntity<List<ContactNoteDTO>> getAllContactNotes(ContactNoteCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactNotes by criteria: {}", criteria);
        Page<ContactNoteDTO> page = contactNoteQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-notes/count} : count all the contactNotes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-notes/count")
    public ResponseEntity<Long> countContactNotes(ContactNoteCriteria criteria) {
        log.debug("REST request to count ContactNotes by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactNoteQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-notes/:id} : get the "id" contactNote.
     *
     * @param id the id of the contactNoteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactNoteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-notes/{id}")
    public ResponseEntity<ContactNoteDTO> getContactNote(@PathVariable Long id) {
        log.debug("REST request to get ContactNote : {}", id);
        Optional<ContactNoteDTO> contactNoteDTO = contactNoteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactNoteDTO);
    }

    /**
     * {@code DELETE  /contact-notes/:id} : delete the "id" contactNote.
     *
     * @param id the id of the contactNoteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-notes/{id}")
    public ResponseEntity<Void> deleteContactNote(@PathVariable Long id) {
        log.debug("REST request to delete ContactNote : {}", id);
        contactNoteService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

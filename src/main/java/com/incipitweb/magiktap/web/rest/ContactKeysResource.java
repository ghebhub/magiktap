package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactKeysService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactKeysDTO;
import com.incipitweb.magiktap.service.dto.ContactKeysCriteria;
import com.incipitweb.magiktap.service.ContactKeysQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactKeys}.
 */
@RestController
@RequestMapping("/api")
public class ContactKeysResource {

    private final Logger log = LoggerFactory.getLogger(ContactKeysResource.class);

    private static final String ENTITY_NAME = "contactKeys";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactKeysService contactKeysService;

    private final ContactKeysQueryService contactKeysQueryService;

    public ContactKeysResource(ContactKeysService contactKeysService, ContactKeysQueryService contactKeysQueryService) {
        this.contactKeysService = contactKeysService;
        this.contactKeysQueryService = contactKeysQueryService;
    }

    /**
     * {@code POST  /contact-keys} : Create a new contactKeys.
     *
     * @param contactKeysDTO the contactKeysDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactKeysDTO, or with status {@code 400 (Bad Request)} if the contactKeys has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-keys")
    public ResponseEntity<ContactKeysDTO> createContactKeys(@Valid @RequestBody ContactKeysDTO contactKeysDTO) throws URISyntaxException {
        log.debug("REST request to save ContactKeys : {}", contactKeysDTO);
        if (contactKeysDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactKeys cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactKeysDTO result = contactKeysService.save(contactKeysDTO);
        return ResponseEntity.created(new URI("/api/contact-keys/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-keys} : Updates an existing contactKeys.
     *
     * @param contactKeysDTO the contactKeysDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactKeysDTO,
     * or with status {@code 400 (Bad Request)} if the contactKeysDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactKeysDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-keys")
    public ResponseEntity<ContactKeysDTO> updateContactKeys(@Valid @RequestBody ContactKeysDTO contactKeysDTO) throws URISyntaxException {
        log.debug("REST request to update ContactKeys : {}", contactKeysDTO);
        if (contactKeysDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactKeysDTO result = contactKeysService.save(contactKeysDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactKeysDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-keys} : get all the contactKeys.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactKeys in body.
     */
    @GetMapping("/contact-keys")
    public ResponseEntity<List<ContactKeysDTO>> getAllContactKeys(ContactKeysCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactKeys by criteria: {}", criteria);
        Page<ContactKeysDTO> page = contactKeysQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-keys/count} : count all the contactKeys.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-keys/count")
    public ResponseEntity<Long> countContactKeys(ContactKeysCriteria criteria) {
        log.debug("REST request to count ContactKeys by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactKeysQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-keys/:id} : get the "id" contactKeys.
     *
     * @param id the id of the contactKeysDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactKeysDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-keys/{id}")
    public ResponseEntity<ContactKeysDTO> getContactKeys(@PathVariable Long id) {
        log.debug("REST request to get ContactKeys : {}", id);
        Optional<ContactKeysDTO> contactKeysDTO = contactKeysService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactKeysDTO);
    }

    /**
     * {@code DELETE  /contact-keys/:id} : delete the "id" contactKeys.
     *
     * @param id the id of the contactKeysDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-keys/{id}")
    public ResponseEntity<Void> deleteContactKeys(@PathVariable Long id) {
        log.debug("REST request to delete ContactKeys : {}", id);
        contactKeysService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

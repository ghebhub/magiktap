package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactEmailService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactEmailDTO;
import com.incipitweb.magiktap.service.dto.ContactEmailCriteria;
import com.incipitweb.magiktap.service.ContactEmailQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactEmail}.
 */
@RestController
@RequestMapping("/api")
public class ContactEmailResource {

    private final Logger log = LoggerFactory.getLogger(ContactEmailResource.class);

    private static final String ENTITY_NAME = "contactEmail";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactEmailService contactEmailService;

    private final ContactEmailQueryService contactEmailQueryService;

    public ContactEmailResource(ContactEmailService contactEmailService, ContactEmailQueryService contactEmailQueryService) {
        this.contactEmailService = contactEmailService;
        this.contactEmailQueryService = contactEmailQueryService;
    }

    /**
     * {@code POST  /contact-emails} : Create a new contactEmail.
     *
     * @param contactEmailDTO the contactEmailDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactEmailDTO, or with status {@code 400 (Bad Request)} if the contactEmail has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-emails")
    public ResponseEntity<ContactEmailDTO> createContactEmail(@Valid @RequestBody ContactEmailDTO contactEmailDTO) throws URISyntaxException {
        log.debug("REST request to save ContactEmail : {}", contactEmailDTO);
        if (contactEmailDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactEmail cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactEmailDTO result = contactEmailService.save(contactEmailDTO);
        return ResponseEntity.created(new URI("/api/contact-emails/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-emails} : Updates an existing contactEmail.
     *
     * @param contactEmailDTO the contactEmailDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactEmailDTO,
     * or with status {@code 400 (Bad Request)} if the contactEmailDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactEmailDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-emails")
    public ResponseEntity<ContactEmailDTO> updateContactEmail(@Valid @RequestBody ContactEmailDTO contactEmailDTO) throws URISyntaxException {
        log.debug("REST request to update ContactEmail : {}", contactEmailDTO);
        if (contactEmailDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactEmailDTO result = contactEmailService.save(contactEmailDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactEmailDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-emails} : get all the contactEmails.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactEmails in body.
     */
    @GetMapping("/contact-emails")
    public ResponseEntity<List<ContactEmailDTO>> getAllContactEmails(ContactEmailCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactEmails by criteria: {}", criteria);
        Page<ContactEmailDTO> page = contactEmailQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-emails/count} : count all the contactEmails.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-emails/count")
    public ResponseEntity<Long> countContactEmails(ContactEmailCriteria criteria) {
        log.debug("REST request to count ContactEmails by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactEmailQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-emails/:id} : get the "id" contactEmail.
     *
     * @param id the id of the contactEmailDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactEmailDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-emails/{id}")
    public ResponseEntity<ContactEmailDTO> getContactEmail(@PathVariable Long id) {
        log.debug("REST request to get ContactEmail : {}", id);
        Optional<ContactEmailDTO> contactEmailDTO = contactEmailService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactEmailDTO);
    }

    /**
     * {@code DELETE  /contact-emails/:id} : delete the "id" contactEmail.
     *
     * @param id the id of the contactEmailDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-emails/{id}")
    public ResponseEntity<Void> deleteContactEmail(@PathVariable Long id) {
        log.debug("REST request to delete ContactEmail : {}", id);
        contactEmailService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

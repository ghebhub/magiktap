package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.service.ContactMailAddressService;
import com.incipitweb.magiktap.web.rest.errors.BadRequestAlertException;
import com.incipitweb.magiktap.service.dto.ContactMailAddressDTO;
import com.incipitweb.magiktap.service.dto.ContactMailAddressCriteria;
import com.incipitweb.magiktap.service.ContactMailAddressQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.incipitweb.magiktap.domain.ContactMailAddress}.
 */
@RestController
@RequestMapping("/api")
public class ContactMailAddressResource {

    private final Logger log = LoggerFactory.getLogger(ContactMailAddressResource.class);

    private static final String ENTITY_NAME = "contactMailAddress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ContactMailAddressService contactMailAddressService;

    private final ContactMailAddressQueryService contactMailAddressQueryService;

    public ContactMailAddressResource(ContactMailAddressService contactMailAddressService, ContactMailAddressQueryService contactMailAddressQueryService) {
        this.contactMailAddressService = contactMailAddressService;
        this.contactMailAddressQueryService = contactMailAddressQueryService;
    }

    /**
     * {@code POST  /contact-mail-addresses} : Create a new contactMailAddress.
     *
     * @param contactMailAddressDTO the contactMailAddressDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new contactMailAddressDTO, or with status {@code 400 (Bad Request)} if the contactMailAddress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/contact-mail-addresses")
    public ResponseEntity<ContactMailAddressDTO> createContactMailAddress(@Valid @RequestBody ContactMailAddressDTO contactMailAddressDTO) throws URISyntaxException {
        log.debug("REST request to save ContactMailAddress : {}", contactMailAddressDTO);
        if (contactMailAddressDTO.getId() != null) {
            throw new BadRequestAlertException("A new contactMailAddress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ContactMailAddressDTO result = contactMailAddressService.save(contactMailAddressDTO);
        return ResponseEntity.created(new URI("/api/contact-mail-addresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /contact-mail-addresses} : Updates an existing contactMailAddress.
     *
     * @param contactMailAddressDTO the contactMailAddressDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated contactMailAddressDTO,
     * or with status {@code 400 (Bad Request)} if the contactMailAddressDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the contactMailAddressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/contact-mail-addresses")
    public ResponseEntity<ContactMailAddressDTO> updateContactMailAddress(@Valid @RequestBody ContactMailAddressDTO contactMailAddressDTO) throws URISyntaxException {
        log.debug("REST request to update ContactMailAddress : {}", contactMailAddressDTO);
        if (contactMailAddressDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ContactMailAddressDTO result = contactMailAddressService.save(contactMailAddressDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, contactMailAddressDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /contact-mail-addresses} : get all the contactMailAddresses.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of contactMailAddresses in body.
     */
    @GetMapping("/contact-mail-addresses")
    public ResponseEntity<List<ContactMailAddressDTO>> getAllContactMailAddresses(ContactMailAddressCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ContactMailAddresses by criteria: {}", criteria);
        Page<ContactMailAddressDTO> page = contactMailAddressQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /contact-mail-addresses/count} : count all the contactMailAddresses.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/contact-mail-addresses/count")
    public ResponseEntity<Long> countContactMailAddresses(ContactMailAddressCriteria criteria) {
        log.debug("REST request to count ContactMailAddresses by criteria: {}", criteria);
        return ResponseEntity.ok().body(contactMailAddressQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /contact-mail-addresses/:id} : get the "id" contactMailAddress.
     *
     * @param id the id of the contactMailAddressDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the contactMailAddressDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/contact-mail-addresses/{id}")
    public ResponseEntity<ContactMailAddressDTO> getContactMailAddress(@PathVariable Long id) {
        log.debug("REST request to get ContactMailAddress : {}", id);
        Optional<ContactMailAddressDTO> contactMailAddressDTO = contactMailAddressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(contactMailAddressDTO);
    }

    /**
     * {@code DELETE  /contact-mail-addresses/:id} : delete the "id" contactMailAddress.
     *
     * @param id the id of the contactMailAddressDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/contact-mail-addresses/{id}")
    public ResponseEntity<Void> deleteContactMailAddress(@PathVariable Long id) {
        log.debug("REST request to delete ContactMailAddress : {}", id);
        contactMailAddressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

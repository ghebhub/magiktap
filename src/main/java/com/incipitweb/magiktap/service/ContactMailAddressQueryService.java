package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactMailAddress;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactMailAddressRepository;
import com.incipitweb.magiktap.service.dto.ContactMailAddressCriteria;
import com.incipitweb.magiktap.service.dto.ContactMailAddressDTO;
import com.incipitweb.magiktap.service.mapper.ContactMailAddressMapper;

/**
 * Service for executing complex queries for {@link ContactMailAddress} entities in the database.
 * The main input is a {@link ContactMailAddressCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactMailAddressDTO} or a {@link Page} of {@link ContactMailAddressDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactMailAddressQueryService extends QueryService<ContactMailAddress> {

    private final Logger log = LoggerFactory.getLogger(ContactMailAddressQueryService.class);

    private final ContactMailAddressRepository contactMailAddressRepository;

    private final ContactMailAddressMapper contactMailAddressMapper;

    public ContactMailAddressQueryService(ContactMailAddressRepository contactMailAddressRepository, ContactMailAddressMapper contactMailAddressMapper) {
        this.contactMailAddressRepository = contactMailAddressRepository;
        this.contactMailAddressMapper = contactMailAddressMapper;
    }

    /**
     * Return a {@link List} of {@link ContactMailAddressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactMailAddressDTO> findByCriteria(ContactMailAddressCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactMailAddress> specification = createSpecification(criteria);
        return contactMailAddressMapper.toDto(contactMailAddressRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactMailAddressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMailAddressDTO> findByCriteria(ContactMailAddressCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactMailAddress> specification = createSpecification(criteria);
        return contactMailAddressRepository.findAll(specification, page)
            .map(contactMailAddressMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactMailAddressCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactMailAddress> specification = createSpecification(criteria);
        return contactMailAddressRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactMailAddressCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactMailAddress> createSpecification(ContactMailAddressCriteria criteria) {
        Specification<ContactMailAddress> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactMailAddress_.id));
            }
            if (criteria.getPobox() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPobox(), ContactMailAddress_.pobox));
            }
            if (criteria.getExtendedAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExtendedAddress(), ContactMailAddress_.extendedAddress));
            }
            if (criteria.getStreet() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStreet(), ContactMailAddress_.street));
            }
            if (criteria.getLocality() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocality(), ContactMailAddress_.locality));
            }
            if (criteria.getRegion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRegion(), ContactMailAddress_.region));
            }
            if (criteria.getPostalCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPostalCode(), ContactMailAddress_.postalCode));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), ContactMailAddress_.country));
            }
            if (criteria.getMailAddressType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMailAddressType(), ContactMailAddress_.mailAddressType));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactMailAddress_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

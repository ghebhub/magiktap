package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactNote;
import com.incipitweb.magiktap.repository.ContactNoteRepository;
import com.incipitweb.magiktap.service.dto.ContactNoteDTO;
import com.incipitweb.magiktap.service.mapper.ContactNoteMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactNote}.
 */
@Service
@Transactional
public class ContactNoteService {

    private final Logger log = LoggerFactory.getLogger(ContactNoteService.class);

    private final ContactNoteRepository contactNoteRepository;

    private final ContactNoteMapper contactNoteMapper;

    public ContactNoteService(ContactNoteRepository contactNoteRepository, ContactNoteMapper contactNoteMapper) {
        this.contactNoteRepository = contactNoteRepository;
        this.contactNoteMapper = contactNoteMapper;
    }

    /**
     * Save a contactNote.
     *
     * @param contactNoteDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactNoteDTO save(ContactNoteDTO contactNoteDTO) {
        log.debug("Request to save ContactNote : {}", contactNoteDTO);
        ContactNote contactNote = contactNoteMapper.toEntity(contactNoteDTO);
        contactNote = contactNoteRepository.save(contactNote);
        return contactNoteMapper.toDto(contactNote);
    }

    /**
     * Get all the contactNotes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactNoteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactNotes");
        return contactNoteRepository.findAll(pageable)
            .map(contactNoteMapper::toDto);
    }


    /**
     * Get one contactNote by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactNoteDTO> findOne(Long id) {
        log.debug("Request to get ContactNote : {}", id);
        return contactNoteRepository.findById(id)
            .map(contactNoteMapper::toDto);
    }

    /**
     * Delete the contactNote by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactNote : {}", id);
        contactNoteRepository.deleteById(id);
    }
}

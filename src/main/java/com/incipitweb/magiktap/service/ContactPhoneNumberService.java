package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactPhoneNumber;
import com.incipitweb.magiktap.repository.ContactPhoneNumberRepository;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberDTO;
import com.incipitweb.magiktap.service.mapper.ContactPhoneNumberMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactPhoneNumber}.
 */
@Service
@Transactional
public class ContactPhoneNumberService {

    private final Logger log = LoggerFactory.getLogger(ContactPhoneNumberService.class);

    private final ContactPhoneNumberRepository contactPhoneNumberRepository;

    private final ContactPhoneNumberMapper contactPhoneNumberMapper;

    public ContactPhoneNumberService(ContactPhoneNumberRepository contactPhoneNumberRepository, ContactPhoneNumberMapper contactPhoneNumberMapper) {
        this.contactPhoneNumberRepository = contactPhoneNumberRepository;
        this.contactPhoneNumberMapper = contactPhoneNumberMapper;
    }

    /**
     * Save a contactPhoneNumber.
     *
     * @param contactPhoneNumberDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactPhoneNumberDTO save(ContactPhoneNumberDTO contactPhoneNumberDTO) {
        log.debug("Request to save ContactPhoneNumber : {}", contactPhoneNumberDTO);
        ContactPhoneNumber contactPhoneNumber = contactPhoneNumberMapper.toEntity(contactPhoneNumberDTO);
        contactPhoneNumber = contactPhoneNumberRepository.save(contactPhoneNumber);
        return contactPhoneNumberMapper.toDto(contactPhoneNumber);
    }

    /**
     * Get all the contactPhoneNumbers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactPhoneNumberDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactPhoneNumbers");
        return contactPhoneNumberRepository.findAll(pageable)
            .map(contactPhoneNumberMapper::toDto);
    }


    /**
     * Get one contactPhoneNumber by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactPhoneNumberDTO> findOne(Long id) {
        log.debug("Request to get ContactPhoneNumber : {}", id);
        return contactPhoneNumberRepository.findById(id)
            .map(contactPhoneNumberMapper::toDto);
    }

    /**
     * Delete the contactPhoneNumber by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactPhoneNumber : {}", id);
        contactPhoneNumberRepository.deleteById(id);
    }
}

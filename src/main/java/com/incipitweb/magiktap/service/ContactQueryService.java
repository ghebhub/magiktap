package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactRepository;
import com.incipitweb.magiktap.service.dto.ContactCriteria;
import com.incipitweb.magiktap.service.dto.ContactDTO;
import com.incipitweb.magiktap.service.mapper.ContactMapper;

/**
 * Service for executing complex queries for {@link Contact} entities in the database.
 * The main input is a {@link ContactCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactDTO} or a {@link Page} of {@link ContactDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactQueryService extends QueryService<Contact> {

    private final Logger log = LoggerFactory.getLogger(ContactQueryService.class);

    private final ContactRepository contactRepository;

    private final ContactMapper contactMapper;

    public ContactQueryService(ContactRepository contactRepository, ContactMapper contactMapper) {
        this.contactRepository = contactRepository;
        this.contactMapper = contactMapper;
    }

    /**
     * Return a {@link List} of {@link ContactDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactDTO> findByCriteria(ContactCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Contact> specification = createSpecification(criteria);
        return contactMapper.toDto(contactRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactDTO> findByCriteria(ContactCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Contact> specification = createSpecification(criteria);
        return contactRepository.findAll(specification, page)
            .map(contactMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Contact> specification = createSpecification(criteria);
        return contactRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Contact> createSpecification(ContactCriteria criteria) {
        Specification<Contact> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Contact_.id));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactId(), Contact_.contactId));
            }
            if (criteria.getFn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFn(), Contact_.fn));
            }
            if (criteria.getN() != null) {
                specification = specification.and(buildStringSpecification(criteria.getN(), Contact_.n));
            }
            if (criteria.getNickname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNickname(), Contact_.nickname));
            }
            if (criteria.getBday() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBday(), Contact_.bday));
            }
            if (criteria.getMailer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMailer(), Contact_.mailer));
            }
            if (criteria.getTz() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTz(), Contact_.tz));
            }
            if (criteria.getGeoLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGeoLat(), Contact_.geoLat));
            }
            if (criteria.getGeoLong() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getGeoLong(), Contact_.geoLong));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Contact_.title));
            }
            if (criteria.getRole() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRole(), Contact_.role));
            }
            if (criteria.getProdId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProdId(), Contact_.prodId));
            }
            if (criteria.getRev() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRev(), Contact_.rev));
            }
            if (criteria.getSortString() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSortString(), Contact_.sortString));
            }
            if (criteria.getUid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUid(), Contact_.uid));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), Contact_.url));
            }
            if (criteria.getVersion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVersion(), Contact_.version));
            }
            if (criteria.getClassName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClassName(), Contact_.className));
            }
            if (criteria.getContactAgentId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactAgentId(),
                    root -> root.join(Contact_.contactAgents, JoinType.LEFT).get(ContactAgent_.id)));
            }
            if (criteria.getContactCategoriesId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactCategoriesId(),
                    root -> root.join(Contact_.contactCategories, JoinType.LEFT).get(ContactCategories_.id)));
            }
            if (criteria.getContactDataId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactDataId(),
                    root -> root.join(Contact_.contactData, JoinType.LEFT).get(ContactData_.id)));
            }
            if (criteria.getContactEmailId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactEmailId(),
                    root -> root.join(Contact_.contactEmails, JoinType.LEFT).get(ContactEmail_.id)));
            }
            if (criteria.getContactKeysId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactKeysId(),
                    root -> root.join(Contact_.contactKeys, JoinType.LEFT).get(ContactKeys_.id)));
            }
            if (criteria.getContactMailAddressId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactMailAddressId(),
                    root -> root.join(Contact_.contactMailAddresses, JoinType.LEFT).get(ContactMailAddress_.id)));
            }
            if (criteria.getContactNoteId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactNoteId(),
                    root -> root.join(Contact_.contactNotes, JoinType.LEFT).get(ContactNote_.id)));
            }
            if (criteria.getContactPhoneNumberId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactPhoneNumberId(),
                    root -> root.join(Contact_.contactPhoneNumbers, JoinType.LEFT).get(ContactPhoneNumber_.id)));
            }
            if (criteria.getContactXtendedId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactXtendedId(),
                    root -> root.join(Contact_.contactXtendeds, JoinType.LEFT).get(ContactXtended_.id)));
            }
        }
        return specification;
    }
}

package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactEmail;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactEmailRepository;
import com.incipitweb.magiktap.service.dto.ContactEmailCriteria;
import com.incipitweb.magiktap.service.dto.ContactEmailDTO;
import com.incipitweb.magiktap.service.mapper.ContactEmailMapper;

/**
 * Service for executing complex queries for {@link ContactEmail} entities in the database.
 * The main input is a {@link ContactEmailCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactEmailDTO} or a {@link Page} of {@link ContactEmailDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactEmailQueryService extends QueryService<ContactEmail> {

    private final Logger log = LoggerFactory.getLogger(ContactEmailQueryService.class);

    private final ContactEmailRepository contactEmailRepository;

    private final ContactEmailMapper contactEmailMapper;

    public ContactEmailQueryService(ContactEmailRepository contactEmailRepository, ContactEmailMapper contactEmailMapper) {
        this.contactEmailRepository = contactEmailRepository;
        this.contactEmailMapper = contactEmailMapper;
    }

    /**
     * Return a {@link List} of {@link ContactEmailDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactEmailDTO> findByCriteria(ContactEmailCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactEmail> specification = createSpecification(criteria);
        return contactEmailMapper.toDto(contactEmailRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactEmailDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactEmailDTO> findByCriteria(ContactEmailCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactEmail> specification = createSpecification(criteria);
        return contactEmailRepository.findAll(specification, page)
            .map(contactEmailMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactEmailCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactEmail> specification = createSpecification(criteria);
        return contactEmailRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactEmailCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactEmail> createSpecification(ContactEmailCriteria criteria) {
        Specification<ContactEmail> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactEmail_.id));
            }
            if (criteria.getEmailAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailAddress(), ContactEmail_.emailAddress));
            }
            if (criteria.getEmailType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmailType(), ContactEmail_.emailType));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactEmail_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactNote} entity.
 */
public class ContactNoteDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String note;

    private String contactNoteType;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getContactNoteType() {
        return contactNoteType;
    }

    public void setContactNoteType(String contactNoteType) {
        this.contactNoteType = contactNoteType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactNoteDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactNoteDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactNoteDTO{" +
            "id=" + getId() +
            ", note='" + getNote() + "'" +
            ", contactNoteType='" + getContactNoteType() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

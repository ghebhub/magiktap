package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactPhoneNumber} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactPhoneNumberResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-phone-numbers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactPhoneNumberCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter localNumber;

    private StringFilter phoneNumberType;

    private LongFilter contactId;

    public ContactPhoneNumberCriteria() {
    }

    public ContactPhoneNumberCriteria(ContactPhoneNumberCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.localNumber = other.localNumber == null ? null : other.localNumber.copy();
        this.phoneNumberType = other.phoneNumberType == null ? null : other.phoneNumberType.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactPhoneNumberCriteria copy() {
        return new ContactPhoneNumberCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(StringFilter localNumber) {
        this.localNumber = localNumber;
    }

    public StringFilter getPhoneNumberType() {
        return phoneNumberType;
    }

    public void setPhoneNumberType(StringFilter phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactPhoneNumberCriteria that = (ContactPhoneNumberCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(localNumber, that.localNumber) &&
            Objects.equals(phoneNumberType, that.phoneNumberType) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        localNumber,
        phoneNumberType,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactPhoneNumberCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (localNumber != null ? "localNumber=" + localNumber + ", " : "") +
                (phoneNumberType != null ? "phoneNumberType=" + phoneNumberType + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

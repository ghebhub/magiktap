package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactKeys} entity.
 */
public class ContactKeysDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String keyData;

    private String keyType;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKeyData() {
        return keyData;
    }

    public void setKeyData(String keyData) {
        this.keyData = keyData;
    }

    public String getKeyType() {
        return keyType;
    }

    public void setKeyType(String keyType) {
        this.keyType = keyType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactKeysDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactKeysDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactKeysDTO{" +
            "id=" + getId() +
            ", keyData='" + getKeyData() + "'" +
            ", keyType='" + getKeyType() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

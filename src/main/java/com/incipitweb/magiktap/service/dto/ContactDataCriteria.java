package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactData} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactDataResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-data?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactDataCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter dataName;

    private StringFilter url;

    private StringFilter inline;

    private StringFilter contactTypes;

    private StringFilter contactEncodingTypes;

    private LongFilter contactId;

    public ContactDataCriteria() {
    }

    public ContactDataCriteria(ContactDataCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dataName = other.dataName == null ? null : other.dataName.copy();
        this.url = other.url == null ? null : other.url.copy();
        this.inline = other.inline == null ? null : other.inline.copy();
        this.contactTypes = other.contactTypes == null ? null : other.contactTypes.copy();
        this.contactEncodingTypes = other.contactEncodingTypes == null ? null : other.contactEncodingTypes.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactDataCriteria copy() {
        return new ContactDataCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDataName() {
        return dataName;
    }

    public void setDataName(StringFilter dataName) {
        this.dataName = dataName;
    }

    public StringFilter getUrl() {
        return url;
    }

    public void setUrl(StringFilter url) {
        this.url = url;
    }

    public StringFilter getInline() {
        return inline;
    }

    public void setInline(StringFilter inline) {
        this.inline = inline;
    }

    public StringFilter getContactTypes() {
        return contactTypes;
    }

    public void setContactTypes(StringFilter contactTypes) {
        this.contactTypes = contactTypes;
    }

    public StringFilter getContactEncodingTypes() {
        return contactEncodingTypes;
    }

    public void setContactEncodingTypes(StringFilter contactEncodingTypes) {
        this.contactEncodingTypes = contactEncodingTypes;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactDataCriteria that = (ContactDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dataName, that.dataName) &&
            Objects.equals(url, that.url) &&
            Objects.equals(inline, that.inline) &&
            Objects.equals(contactTypes, that.contactTypes) &&
            Objects.equals(contactEncodingTypes, that.contactEncodingTypes) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dataName,
        url,
        inline,
        contactTypes,
        contactEncodingTypes,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dataName != null ? "dataName=" + dataName + ", " : "") +
                (url != null ? "url=" + url + ", " : "") +
                (inline != null ? "inline=" + inline + ", " : "") +
                (contactTypes != null ? "contactTypes=" + contactTypes + ", " : "") +
                (contactEncodingTypes != null ? "contactEncodingTypes=" + contactEncodingTypes + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

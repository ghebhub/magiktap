package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactData} entity.
 */
public class ContactDataDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 10)
    private String dataName;

    @Size(max = 255)
    private String url;

    @Size(max = 1)
    private String inline;

    @NotNull
    @Size(max = 10)
    private String contactTypes;

    @NotNull
    @Size(max = 10)
    private String contactEncodingTypes;

    @Lob
    private byte[] contactData;

    private String contactDataContentType;

    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataName() {
        return dataName;
    }

    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInline() {
        return inline;
    }

    public void setInline(String inline) {
        this.inline = inline;
    }

    public String getContactTypes() {
        return contactTypes;
    }

    public void setContactTypes(String contactTypes) {
        this.contactTypes = contactTypes;
    }

    public String getContactEncodingTypes() {
        return contactEncodingTypes;
    }

    public void setContactEncodingTypes(String contactEncodingTypes) {
        this.contactEncodingTypes = contactEncodingTypes;
    }

    public byte[] getContactData() {
        return contactData;
    }

    public void setContactData(byte[] contactData) {
        this.contactData = contactData;
    }

    public String getContactDataContentType() {
        return contactDataContentType;
    }

    public void setContactDataContentType(String contactDataContentType) {
        this.contactDataContentType = contactDataContentType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactDataDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactDataDTO{" +
            "id=" + getId() +
            ", dataName='" + getDataName() + "'" +
            ", url='" + getUrl() + "'" +
            ", inline='" + getInline() + "'" +
            ", contactTypes='" + getContactTypes() + "'" +
            ", contactEncodingTypes='" + getContactEncodingTypes() + "'" +
            ", contactData='" + getContactData() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

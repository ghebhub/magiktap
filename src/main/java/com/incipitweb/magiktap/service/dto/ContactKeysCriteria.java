package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactKeys} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactKeysResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-keys?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactKeysCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter keyData;

    private StringFilter keyType;

    private LongFilter contactId;

    public ContactKeysCriteria() {
    }

    public ContactKeysCriteria(ContactKeysCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.keyData = other.keyData == null ? null : other.keyData.copy();
        this.keyType = other.keyType == null ? null : other.keyType.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactKeysCriteria copy() {
        return new ContactKeysCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getKeyData() {
        return keyData;
    }

    public void setKeyData(StringFilter keyData) {
        this.keyData = keyData;
    }

    public StringFilter getKeyType() {
        return keyType;
    }

    public void setKeyType(StringFilter keyType) {
        this.keyType = keyType;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactKeysCriteria that = (ContactKeysCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(keyData, that.keyData) &&
            Objects.equals(keyType, that.keyType) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        keyData,
        keyType,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactKeysCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (keyData != null ? "keyData=" + keyData + ", " : "") +
                (keyType != null ? "keyType=" + keyType + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactXtended} entity.
 */
public class ContactXtendedDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String xname;

    @NotNull
    @Size(max = 255)
    private String xvalue;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getXname() {
        return xname;
    }

    public void setXname(String xname) {
        this.xname = xname;
    }

    public String getXvalue() {
        return xvalue;
    }

    public void setXvalue(String xvalue) {
        this.xvalue = xvalue;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactXtendedDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactXtendedDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactXtendedDTO{" +
            "id=" + getId() +
            ", xname='" + getXname() + "'" +
            ", xvalue='" + getXvalue() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

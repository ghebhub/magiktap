package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactAgent} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactAgentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-agents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactAgentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter uri;

    private LongFilter contactId;

    public ContactAgentCriteria() {
    }

    public ContactAgentCriteria(ContactAgentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.uri = other.uri == null ? null : other.uri.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactAgentCriteria copy() {
        return new ContactAgentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUri() {
        return uri;
    }

    public void setUri(StringFilter uri) {
        this.uri = uri;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactAgentCriteria that = (ContactAgentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(uri, that.uri) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        uri,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactAgentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (uri != null ? "uri=" + uri + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

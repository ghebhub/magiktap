package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactMailAddress} entity.
 */
public class ContactMailAddressDTO implements Serializable {
    
    private Long id;

    @Size(max = 30)
    private String pobox;

    @Size(max = 255)
    private String extendedAddress;

    @NotNull
    @Size(max = 255)
    private String street;

    @Size(max = 50)
    private String locality;

    @Size(max = 50)
    private String region;

    @Size(max = 30)
    private String postalCode;

    @Size(max = 50)
    private String country;

    private String mailAddressType;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPobox() {
        return pobox;
    }

    public void setPobox(String pobox) {
        this.pobox = pobox;
    }

    public String getExtendedAddress() {
        return extendedAddress;
    }

    public void setExtendedAddress(String extendedAddress) {
        this.extendedAddress = extendedAddress;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMailAddressType() {
        return mailAddressType;
    }

    public void setMailAddressType(String mailAddressType) {
        this.mailAddressType = mailAddressType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactMailAddressDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactMailAddressDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMailAddressDTO{" +
            "id=" + getId() +
            ", pobox='" + getPobox() + "'" +
            ", extendedAddress='" + getExtendedAddress() + "'" +
            ", street='" + getStreet() + "'" +
            ", locality='" + getLocality() + "'" +
            ", region='" + getRegion() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", country='" + getCountry() + "'" +
            ", mailAddressType='" + getMailAddressType() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

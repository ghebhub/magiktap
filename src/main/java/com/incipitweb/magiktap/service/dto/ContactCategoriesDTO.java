package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactCategories} entity.
 */
public class ContactCategoriesDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String categoryName;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactCategoriesDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactCategoriesDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactCategoriesDTO{" +
            "id=" + getId() +
            ", categoryName='" + getCategoryName() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

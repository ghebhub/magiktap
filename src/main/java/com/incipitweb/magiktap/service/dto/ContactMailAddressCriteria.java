package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactMailAddress} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactMailAddressResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-mail-addresses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactMailAddressCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter pobox;

    private StringFilter extendedAddress;

    private StringFilter street;

    private StringFilter locality;

    private StringFilter region;

    private StringFilter postalCode;

    private StringFilter country;

    private StringFilter mailAddressType;

    private LongFilter contactId;

    public ContactMailAddressCriteria() {
    }

    public ContactMailAddressCriteria(ContactMailAddressCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.pobox = other.pobox == null ? null : other.pobox.copy();
        this.extendedAddress = other.extendedAddress == null ? null : other.extendedAddress.copy();
        this.street = other.street == null ? null : other.street.copy();
        this.locality = other.locality == null ? null : other.locality.copy();
        this.region = other.region == null ? null : other.region.copy();
        this.postalCode = other.postalCode == null ? null : other.postalCode.copy();
        this.country = other.country == null ? null : other.country.copy();
        this.mailAddressType = other.mailAddressType == null ? null : other.mailAddressType.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactMailAddressCriteria copy() {
        return new ContactMailAddressCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPobox() {
        return pobox;
    }

    public void setPobox(StringFilter pobox) {
        this.pobox = pobox;
    }

    public StringFilter getExtendedAddress() {
        return extendedAddress;
    }

    public void setExtendedAddress(StringFilter extendedAddress) {
        this.extendedAddress = extendedAddress;
    }

    public StringFilter getStreet() {
        return street;
    }

    public void setStreet(StringFilter street) {
        this.street = street;
    }

    public StringFilter getLocality() {
        return locality;
    }

    public void setLocality(StringFilter locality) {
        this.locality = locality;
    }

    public StringFilter getRegion() {
        return region;
    }

    public void setRegion(StringFilter region) {
        this.region = region;
    }

    public StringFilter getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(StringFilter postalCode) {
        this.postalCode = postalCode;
    }

    public StringFilter getCountry() {
        return country;
    }

    public void setCountry(StringFilter country) {
        this.country = country;
    }

    public StringFilter getMailAddressType() {
        return mailAddressType;
    }

    public void setMailAddressType(StringFilter mailAddressType) {
        this.mailAddressType = mailAddressType;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactMailAddressCriteria that = (ContactMailAddressCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(pobox, that.pobox) &&
            Objects.equals(extendedAddress, that.extendedAddress) &&
            Objects.equals(street, that.street) &&
            Objects.equals(locality, that.locality) &&
            Objects.equals(region, that.region) &&
            Objects.equals(postalCode, that.postalCode) &&
            Objects.equals(country, that.country) &&
            Objects.equals(mailAddressType, that.mailAddressType) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        pobox,
        extendedAddress,
        street,
        locality,
        region,
        postalCode,
        country,
        mailAddressType,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactMailAddressCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (pobox != null ? "pobox=" + pobox + ", " : "") +
                (extendedAddress != null ? "extendedAddress=" + extendedAddress + ", " : "") +
                (street != null ? "street=" + street + ", " : "") +
                (locality != null ? "locality=" + locality + ", " : "") +
                (region != null ? "region=" + region + ", " : "") +
                (postalCode != null ? "postalCode=" + postalCode + ", " : "") +
                (country != null ? "country=" + country + ", " : "") +
                (mailAddressType != null ? "mailAddressType=" + mailAddressType + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactAgent} entity.
 */
public class ContactAgentDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String uri;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactAgentDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactAgentDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactAgentDTO{" +
            "id=" + getId() +
            ", uri='" + getUri() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

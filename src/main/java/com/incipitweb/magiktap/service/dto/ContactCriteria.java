package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.Contact} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contacts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter contactId;

    private StringFilter fn;

    private StringFilter n;

    private StringFilter nickname;

    private InstantFilter bday;

    private StringFilter mailer;

    private IntegerFilter tz;

    private DoubleFilter geoLat;

    private DoubleFilter geoLong;

    private StringFilter title;

    private StringFilter role;

    private StringFilter prodId;

    private StringFilter rev;

    private StringFilter sortString;

    private StringFilter uid;

    private StringFilter url;

    private StringFilter version;

    private StringFilter className;

    private LongFilter contactAgentId;

    private LongFilter contactCategoriesId;

    private LongFilter contactDataId;

    private LongFilter contactEmailId;

    private LongFilter contactKeysId;

    private LongFilter contactMailAddressId;

    private LongFilter contactNoteId;

    private LongFilter contactPhoneNumberId;

    private LongFilter contactXtendedId;

    public ContactCriteria() {
    }

    public ContactCriteria(ContactCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
        this.fn = other.fn == null ? null : other.fn.copy();
        this.n = other.n == null ? null : other.n.copy();
        this.nickname = other.nickname == null ? null : other.nickname.copy();
        this.bday = other.bday == null ? null : other.bday.copy();
        this.mailer = other.mailer == null ? null : other.mailer.copy();
        this.tz = other.tz == null ? null : other.tz.copy();
        this.geoLat = other.geoLat == null ? null : other.geoLat.copy();
        this.geoLong = other.geoLong == null ? null : other.geoLong.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.role = other.role == null ? null : other.role.copy();
        this.prodId = other.prodId == null ? null : other.prodId.copy();
        this.rev = other.rev == null ? null : other.rev.copy();
        this.sortString = other.sortString == null ? null : other.sortString.copy();
        this.uid = other.uid == null ? null : other.uid.copy();
        this.url = other.url == null ? null : other.url.copy();
        this.version = other.version == null ? null : other.version.copy();
        this.className = other.className == null ? null : other.className.copy();
        this.contactAgentId = other.contactAgentId == null ? null : other.contactAgentId.copy();
        this.contactCategoriesId = other.contactCategoriesId == null ? null : other.contactCategoriesId.copy();
        this.contactDataId = other.contactDataId == null ? null : other.contactDataId.copy();
        this.contactEmailId = other.contactEmailId == null ? null : other.contactEmailId.copy();
        this.contactKeysId = other.contactKeysId == null ? null : other.contactKeysId.copy();
        this.contactMailAddressId = other.contactMailAddressId == null ? null : other.contactMailAddressId.copy();
        this.contactNoteId = other.contactNoteId == null ? null : other.contactNoteId.copy();
        this.contactPhoneNumberId = other.contactPhoneNumberId == null ? null : other.contactPhoneNumberId.copy();
        this.contactXtendedId = other.contactXtendedId == null ? null : other.contactXtendedId.copy();
    }

    @Override
    public ContactCriteria copy() {
        return new ContactCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContactId() {
        return contactId;
    }

    public void setContactId(StringFilter contactId) {
        this.contactId = contactId;
    }

    public StringFilter getFn() {
        return fn;
    }

    public void setFn(StringFilter fn) {
        this.fn = fn;
    }

    public StringFilter getN() {
        return n;
    }

    public void setN(StringFilter n) {
        this.n = n;
    }

    public StringFilter getNickname() {
        return nickname;
    }

    public void setNickname(StringFilter nickname) {
        this.nickname = nickname;
    }

    public InstantFilter getBday() {
        return bday;
    }

    public void setBday(InstantFilter bday) {
        this.bday = bday;
    }

    public StringFilter getMailer() {
        return mailer;
    }

    public void setMailer(StringFilter mailer) {
        this.mailer = mailer;
    }

    public IntegerFilter getTz() {
        return tz;
    }

    public void setTz(IntegerFilter tz) {
        this.tz = tz;
    }

    public DoubleFilter getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(DoubleFilter geoLat) {
        this.geoLat = geoLat;
    }

    public DoubleFilter getGeoLong() {
        return geoLong;
    }

    public void setGeoLong(DoubleFilter geoLong) {
        this.geoLong = geoLong;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getRole() {
        return role;
    }

    public void setRole(StringFilter role) {
        this.role = role;
    }

    public StringFilter getProdId() {
        return prodId;
    }

    public void setProdId(StringFilter prodId) {
        this.prodId = prodId;
    }

    public StringFilter getRev() {
        return rev;
    }

    public void setRev(StringFilter rev) {
        this.rev = rev;
    }

    public StringFilter getSortString() {
        return sortString;
    }

    public void setSortString(StringFilter sortString) {
        this.sortString = sortString;
    }

    public StringFilter getUid() {
        return uid;
    }

    public void setUid(StringFilter uid) {
        this.uid = uid;
    }

    public StringFilter getUrl() {
        return url;
    }

    public void setUrl(StringFilter url) {
        this.url = url;
    }

    public StringFilter getVersion() {
        return version;
    }

    public void setVersion(StringFilter version) {
        this.version = version;
    }

    public StringFilter getClassName() {
        return className;
    }

    public void setClassName(StringFilter className) {
        this.className = className;
    }

    public LongFilter getContactAgentId() {
        return contactAgentId;
    }

    public void setContactAgentId(LongFilter contactAgentId) {
        this.contactAgentId = contactAgentId;
    }

    public LongFilter getContactCategoriesId() {
        return contactCategoriesId;
    }

    public void setContactCategoriesId(LongFilter contactCategoriesId) {
        this.contactCategoriesId = contactCategoriesId;
    }

    public LongFilter getContactDataId() {
        return contactDataId;
    }

    public void setContactDataId(LongFilter contactDataId) {
        this.contactDataId = contactDataId;
    }

    public LongFilter getContactEmailId() {
        return contactEmailId;
    }

    public void setContactEmailId(LongFilter contactEmailId) {
        this.contactEmailId = contactEmailId;
    }

    public LongFilter getContactKeysId() {
        return contactKeysId;
    }

    public void setContactKeysId(LongFilter contactKeysId) {
        this.contactKeysId = contactKeysId;
    }

    public LongFilter getContactMailAddressId() {
        return contactMailAddressId;
    }

    public void setContactMailAddressId(LongFilter contactMailAddressId) {
        this.contactMailAddressId = contactMailAddressId;
    }

    public LongFilter getContactNoteId() {
        return contactNoteId;
    }

    public void setContactNoteId(LongFilter contactNoteId) {
        this.contactNoteId = contactNoteId;
    }

    public LongFilter getContactPhoneNumberId() {
        return contactPhoneNumberId;
    }

    public void setContactPhoneNumberId(LongFilter contactPhoneNumberId) {
        this.contactPhoneNumberId = contactPhoneNumberId;
    }

    public LongFilter getContactXtendedId() {
        return contactXtendedId;
    }

    public void setContactXtendedId(LongFilter contactXtendedId) {
        this.contactXtendedId = contactXtendedId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactCriteria that = (ContactCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(contactId, that.contactId) &&
            Objects.equals(fn, that.fn) &&
            Objects.equals(n, that.n) &&
            Objects.equals(nickname, that.nickname) &&
            Objects.equals(bday, that.bday) &&
            Objects.equals(mailer, that.mailer) &&
            Objects.equals(tz, that.tz) &&
            Objects.equals(geoLat, that.geoLat) &&
            Objects.equals(geoLong, that.geoLong) &&
            Objects.equals(title, that.title) &&
            Objects.equals(role, that.role) &&
            Objects.equals(prodId, that.prodId) &&
            Objects.equals(rev, that.rev) &&
            Objects.equals(sortString, that.sortString) &&
            Objects.equals(uid, that.uid) &&
            Objects.equals(url, that.url) &&
            Objects.equals(version, that.version) &&
            Objects.equals(className, that.className) &&
            Objects.equals(contactAgentId, that.contactAgentId) &&
            Objects.equals(contactCategoriesId, that.contactCategoriesId) &&
            Objects.equals(contactDataId, that.contactDataId) &&
            Objects.equals(contactEmailId, that.contactEmailId) &&
            Objects.equals(contactKeysId, that.contactKeysId) &&
            Objects.equals(contactMailAddressId, that.contactMailAddressId) &&
            Objects.equals(contactNoteId, that.contactNoteId) &&
            Objects.equals(contactPhoneNumberId, that.contactPhoneNumberId) &&
            Objects.equals(contactXtendedId, that.contactXtendedId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        contactId,
        fn,
        n,
        nickname,
        bday,
        mailer,
        tz,
        geoLat,
        geoLong,
        title,
        role,
        prodId,
        rev,
        sortString,
        uid,
        url,
        version,
        className,
        contactAgentId,
        contactCategoriesId,
        contactDataId,
        contactEmailId,
        contactKeysId,
        contactMailAddressId,
        contactNoteId,
        contactPhoneNumberId,
        contactXtendedId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
                (fn != null ? "fn=" + fn + ", " : "") +
                (n != null ? "n=" + n + ", " : "") +
                (nickname != null ? "nickname=" + nickname + ", " : "") +
                (bday != null ? "bday=" + bday + ", " : "") +
                (mailer != null ? "mailer=" + mailer + ", " : "") +
                (tz != null ? "tz=" + tz + ", " : "") +
                (geoLat != null ? "geoLat=" + geoLat + ", " : "") +
                (geoLong != null ? "geoLong=" + geoLong + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (role != null ? "role=" + role + ", " : "") +
                (prodId != null ? "prodId=" + prodId + ", " : "") +
                (rev != null ? "rev=" + rev + ", " : "") +
                (sortString != null ? "sortString=" + sortString + ", " : "") +
                (uid != null ? "uid=" + uid + ", " : "") +
                (url != null ? "url=" + url + ", " : "") +
                (version != null ? "version=" + version + ", " : "") +
                (className != null ? "className=" + className + ", " : "") +
                (contactAgentId != null ? "contactAgentId=" + contactAgentId + ", " : "") +
                (contactCategoriesId != null ? "contactCategoriesId=" + contactCategoriesId + ", " : "") +
                (contactDataId != null ? "contactDataId=" + contactDataId + ", " : "") +
                (contactEmailId != null ? "contactEmailId=" + contactEmailId + ", " : "") +
                (contactKeysId != null ? "contactKeysId=" + contactKeysId + ", " : "") +
                (contactMailAddressId != null ? "contactMailAddressId=" + contactMailAddressId + ", " : "") +
                (contactNoteId != null ? "contactNoteId=" + contactNoteId + ", " : "") +
                (contactPhoneNumberId != null ? "contactPhoneNumberId=" + contactPhoneNumberId + ", " : "") +
                (contactXtendedId != null ? "contactXtendedId=" + contactXtendedId + ", " : "") +
            "}";
    }

}

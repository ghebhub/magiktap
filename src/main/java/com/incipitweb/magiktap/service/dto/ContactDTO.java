package com.incipitweb.magiktap.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.Contact} entity.
 */
public class ContactDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 36)
    private String contactId;

    @NotNull
    @Size(max = 255)
    private String fn;

    @NotNull
    @Size(max = 255)
    private String n;

    @Size(max = 255)
    private String nickname;

    @NotNull
    private Instant bday;

    @Size(max = 50)
    private String mailer;

    private Integer tz;

    private Double geoLat;

    private Double geoLong;

    @Size(max = 50)
    private String title;

    @Size(max = 50)
    private String role;

    @Size(max = 255)
    private String prodId;

    @Size(max = 50)
    private String rev;

    @Size(max = 50)
    private String sortString;

    @Size(max = 255)
    private String uid;

    @Size(max = 255)
    private String url;

    @Size(max = 10)
    private String version;

    @Size(max = 50)
    private String className;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getFn() {
        return fn;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Instant getBday() {
        return bday;
    }

    public void setBday(Instant bday) {
        this.bday = bday;
    }

    public String getMailer() {
        return mailer;
    }

    public void setMailer(String mailer) {
        this.mailer = mailer;
    }

    public Integer getTz() {
        return tz;
    }

    public void setTz(Integer tz) {
        this.tz = tz;
    }

    public Double getGeoLat() {
        return geoLat;
    }

    public void setGeoLat(Double geoLat) {
        this.geoLat = geoLat;
    }

    public Double getGeoLong() {
        return geoLong;
    }

    public void setGeoLong(Double geoLong) {
        this.geoLong = geoLong;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(String rev) {
        this.rev = rev;
    }

    public String getSortString() {
        return sortString;
    }

    public void setSortString(String sortString) {
        this.sortString = sortString;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactDTO{" +
            "id=" + getId() +
            ", contactId='" + getContactId() + "'" +
            ", fn='" + getFn() + "'" +
            ", n='" + getN() + "'" +
            ", nickname='" + getNickname() + "'" +
            ", bday='" + getBday() + "'" +
            ", mailer='" + getMailer() + "'" +
            ", tz=" + getTz() +
            ", geoLat=" + getGeoLat() +
            ", geoLong=" + getGeoLong() +
            ", title='" + getTitle() + "'" +
            ", role='" + getRole() + "'" +
            ", prodId='" + getProdId() + "'" +
            ", rev='" + getRev() + "'" +
            ", sortString='" + getSortString() + "'" +
            ", uid='" + getUid() + "'" +
            ", url='" + getUrl() + "'" +
            ", version='" + getVersion() + "'" +
            ", className='" + getClassName() + "'" +
            "}";
    }
}

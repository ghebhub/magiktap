package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactPhoneNumber} entity.
 */
public class ContactPhoneNumberDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String localNumber;

    private String phoneNumberType;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalNumber() {
        return localNumber;
    }

    public void setLocalNumber(String localNumber) {
        this.localNumber = localNumber;
    }

    public String getPhoneNumberType() {
        return phoneNumberType;
    }

    public void setPhoneNumberType(String phoneNumberType) {
        this.phoneNumberType = phoneNumberType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactPhoneNumberDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactPhoneNumberDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactPhoneNumberDTO{" +
            "id=" + getId() +
            ", localNumber='" + getLocalNumber() + "'" +
            ", phoneNumberType='" + getPhoneNumberType() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

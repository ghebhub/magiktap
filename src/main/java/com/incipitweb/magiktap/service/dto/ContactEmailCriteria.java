package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactEmail} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactEmailResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-emails?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactEmailCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter emailAddress;

    private StringFilter emailType;

    private LongFilter contactId;

    public ContactEmailCriteria() {
    }

    public ContactEmailCriteria(ContactEmailCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.emailAddress = other.emailAddress == null ? null : other.emailAddress.copy();
        this.emailType = other.emailType == null ? null : other.emailType.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactEmailCriteria copy() {
        return new ContactEmailCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(StringFilter emailAddress) {
        this.emailAddress = emailAddress;
    }

    public StringFilter getEmailType() {
        return emailType;
    }

    public void setEmailType(StringFilter emailType) {
        this.emailType = emailType;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactEmailCriteria that = (ContactEmailCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(emailAddress, that.emailAddress) &&
            Objects.equals(emailType, that.emailType) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        emailAddress,
        emailType,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactEmailCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (emailAddress != null ? "emailAddress=" + emailAddress + ", " : "") +
                (emailType != null ? "emailType=" + emailType + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

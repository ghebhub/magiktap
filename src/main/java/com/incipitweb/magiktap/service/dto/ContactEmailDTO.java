package com.incipitweb.magiktap.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.incipitweb.magiktap.domain.ContactEmail} entity.
 */
public class ContactEmailDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 255)
    private String emailAddress;

    private String emailType;


    private Long contactId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ContactEmailDTO)) {
            return false;
        }

        return id != null && id.equals(((ContactEmailDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactEmailDTO{" +
            "id=" + getId() +
            ", emailAddress='" + getEmailAddress() + "'" +
            ", emailType='" + getEmailType() + "'" +
            ", contactId=" + getContactId() +
            "}";
    }
}

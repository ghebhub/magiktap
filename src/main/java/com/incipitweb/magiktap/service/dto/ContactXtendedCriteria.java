package com.incipitweb.magiktap.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.incipitweb.magiktap.domain.ContactXtended} entity. This class is used
 * in {@link com.incipitweb.magiktap.web.rest.ContactXtendedResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /contact-xtendeds?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactXtendedCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter xname;

    private StringFilter xvalue;

    private LongFilter contactId;

    public ContactXtendedCriteria() {
    }

    public ContactXtendedCriteria(ContactXtendedCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.xname = other.xname == null ? null : other.xname.copy();
        this.xvalue = other.xvalue == null ? null : other.xvalue.copy();
        this.contactId = other.contactId == null ? null : other.contactId.copy();
    }

    @Override
    public ContactXtendedCriteria copy() {
        return new ContactXtendedCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getXname() {
        return xname;
    }

    public void setXname(StringFilter xname) {
        this.xname = xname;
    }

    public StringFilter getXvalue() {
        return xvalue;
    }

    public void setXvalue(StringFilter xvalue) {
        this.xvalue = xvalue;
    }

    public LongFilter getContactId() {
        return contactId;
    }

    public void setContactId(LongFilter contactId) {
        this.contactId = contactId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ContactXtendedCriteria that = (ContactXtendedCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(xname, that.xname) &&
            Objects.equals(xvalue, that.xvalue) &&
            Objects.equals(contactId, that.contactId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        xname,
        xvalue,
        contactId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ContactXtendedCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (xname != null ? "xname=" + xname + ", " : "") +
                (xvalue != null ? "xvalue=" + xvalue + ", " : "") +
                (contactId != null ? "contactId=" + contactId + ", " : "") +
            "}";
    }

}

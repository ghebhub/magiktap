package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactAgent;
import com.incipitweb.magiktap.repository.ContactAgentRepository;
import com.incipitweb.magiktap.service.dto.ContactAgentDTO;
import com.incipitweb.magiktap.service.mapper.ContactAgentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactAgent}.
 */
@Service
@Transactional
public class ContactAgentService {

    private final Logger log = LoggerFactory.getLogger(ContactAgentService.class);

    private final ContactAgentRepository contactAgentRepository;

    private final ContactAgentMapper contactAgentMapper;

    public ContactAgentService(ContactAgentRepository contactAgentRepository, ContactAgentMapper contactAgentMapper) {
        this.contactAgentRepository = contactAgentRepository;
        this.contactAgentMapper = contactAgentMapper;
    }

    /**
     * Save a contactAgent.
     *
     * @param contactAgentDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactAgentDTO save(ContactAgentDTO contactAgentDTO) {
        log.debug("Request to save ContactAgent : {}", contactAgentDTO);
        ContactAgent contactAgent = contactAgentMapper.toEntity(contactAgentDTO);
        contactAgent = contactAgentRepository.save(contactAgent);
        return contactAgentMapper.toDto(contactAgent);
    }

    /**
     * Get all the contactAgents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactAgentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactAgents");
        return contactAgentRepository.findAll(pageable)
            .map(contactAgentMapper::toDto);
    }


    /**
     * Get one contactAgent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactAgentDTO> findOne(Long id) {
        log.debug("Request to get ContactAgent : {}", id);
        return contactAgentRepository.findById(id)
            .map(contactAgentMapper::toDto);
    }

    /**
     * Delete the contactAgent by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactAgent : {}", id);
        contactAgentRepository.deleteById(id);
    }
}

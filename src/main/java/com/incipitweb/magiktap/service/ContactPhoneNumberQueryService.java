package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactPhoneNumber;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactPhoneNumberRepository;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberCriteria;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberDTO;
import com.incipitweb.magiktap.service.mapper.ContactPhoneNumberMapper;

/**
 * Service for executing complex queries for {@link ContactPhoneNumber} entities in the database.
 * The main input is a {@link ContactPhoneNumberCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactPhoneNumberDTO} or a {@link Page} of {@link ContactPhoneNumberDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactPhoneNumberQueryService extends QueryService<ContactPhoneNumber> {

    private final Logger log = LoggerFactory.getLogger(ContactPhoneNumberQueryService.class);

    private final ContactPhoneNumberRepository contactPhoneNumberRepository;

    private final ContactPhoneNumberMapper contactPhoneNumberMapper;

    public ContactPhoneNumberQueryService(ContactPhoneNumberRepository contactPhoneNumberRepository, ContactPhoneNumberMapper contactPhoneNumberMapper) {
        this.contactPhoneNumberRepository = contactPhoneNumberRepository;
        this.contactPhoneNumberMapper = contactPhoneNumberMapper;
    }

    /**
     * Return a {@link List} of {@link ContactPhoneNumberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactPhoneNumberDTO> findByCriteria(ContactPhoneNumberCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactPhoneNumber> specification = createSpecification(criteria);
        return contactPhoneNumberMapper.toDto(contactPhoneNumberRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactPhoneNumberDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactPhoneNumberDTO> findByCriteria(ContactPhoneNumberCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactPhoneNumber> specification = createSpecification(criteria);
        return contactPhoneNumberRepository.findAll(specification, page)
            .map(contactPhoneNumberMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactPhoneNumberCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactPhoneNumber> specification = createSpecification(criteria);
        return contactPhoneNumberRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactPhoneNumberCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactPhoneNumber> createSpecification(ContactPhoneNumberCriteria criteria) {
        Specification<ContactPhoneNumber> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactPhoneNumber_.id));
            }
            if (criteria.getLocalNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLocalNumber(), ContactPhoneNumber_.localNumber));
            }
            if (criteria.getPhoneNumberType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhoneNumberType(), ContactPhoneNumber_.phoneNumberType));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactPhoneNumber_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

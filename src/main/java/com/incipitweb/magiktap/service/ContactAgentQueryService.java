package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactAgent;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactAgentRepository;
import com.incipitweb.magiktap.service.dto.ContactAgentCriteria;
import com.incipitweb.magiktap.service.dto.ContactAgentDTO;
import com.incipitweb.magiktap.service.mapper.ContactAgentMapper;

/**
 * Service for executing complex queries for {@link ContactAgent} entities in the database.
 * The main input is a {@link ContactAgentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactAgentDTO} or a {@link Page} of {@link ContactAgentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactAgentQueryService extends QueryService<ContactAgent> {

    private final Logger log = LoggerFactory.getLogger(ContactAgentQueryService.class);

    private final ContactAgentRepository contactAgentRepository;

    private final ContactAgentMapper contactAgentMapper;

    public ContactAgentQueryService(ContactAgentRepository contactAgentRepository, ContactAgentMapper contactAgentMapper) {
        this.contactAgentRepository = contactAgentRepository;
        this.contactAgentMapper = contactAgentMapper;
    }

    /**
     * Return a {@link List} of {@link ContactAgentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactAgentDTO> findByCriteria(ContactAgentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactAgent> specification = createSpecification(criteria);
        return contactAgentMapper.toDto(contactAgentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactAgentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactAgentDTO> findByCriteria(ContactAgentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactAgent> specification = createSpecification(criteria);
        return contactAgentRepository.findAll(specification, page)
            .map(contactAgentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactAgentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactAgent> specification = createSpecification(criteria);
        return contactAgentRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactAgentCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactAgent> createSpecification(ContactAgentCriteria criteria) {
        Specification<ContactAgent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactAgent_.id));
            }
            if (criteria.getUri() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUri(), ContactAgent_.uri));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactAgent_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

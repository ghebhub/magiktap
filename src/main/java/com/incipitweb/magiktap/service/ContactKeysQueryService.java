package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactKeys;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactKeysRepository;
import com.incipitweb.magiktap.service.dto.ContactKeysCriteria;
import com.incipitweb.magiktap.service.dto.ContactKeysDTO;
import com.incipitweb.magiktap.service.mapper.ContactKeysMapper;

/**
 * Service for executing complex queries for {@link ContactKeys} entities in the database.
 * The main input is a {@link ContactKeysCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactKeysDTO} or a {@link Page} of {@link ContactKeysDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactKeysQueryService extends QueryService<ContactKeys> {

    private final Logger log = LoggerFactory.getLogger(ContactKeysQueryService.class);

    private final ContactKeysRepository contactKeysRepository;

    private final ContactKeysMapper contactKeysMapper;

    public ContactKeysQueryService(ContactKeysRepository contactKeysRepository, ContactKeysMapper contactKeysMapper) {
        this.contactKeysRepository = contactKeysRepository;
        this.contactKeysMapper = contactKeysMapper;
    }

    /**
     * Return a {@link List} of {@link ContactKeysDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactKeysDTO> findByCriteria(ContactKeysCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactKeys> specification = createSpecification(criteria);
        return contactKeysMapper.toDto(contactKeysRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactKeysDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactKeysDTO> findByCriteria(ContactKeysCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactKeys> specification = createSpecification(criteria);
        return contactKeysRepository.findAll(specification, page)
            .map(contactKeysMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactKeysCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactKeys> specification = createSpecification(criteria);
        return contactKeysRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactKeysCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactKeys> createSpecification(ContactKeysCriteria criteria) {
        Specification<ContactKeys> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactKeys_.id));
            }
            if (criteria.getKeyData() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKeyData(), ContactKeys_.keyData));
            }
            if (criteria.getKeyType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getKeyType(), ContactKeys_.keyType));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactKeys_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

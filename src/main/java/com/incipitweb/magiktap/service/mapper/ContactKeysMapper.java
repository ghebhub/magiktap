package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactKeysDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactKeys} and its DTO {@link ContactKeysDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactKeysMapper extends EntityMapper<ContactKeysDTO, ContactKeys> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactKeysDTO toDto(ContactKeys contactKeys);

    @Mapping(source = "contactId", target = "contact")
    ContactKeys toEntity(ContactKeysDTO contactKeysDTO);

    default ContactKeys fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactKeys contactKeys = new ContactKeys();
        contactKeys.setId(id);
        return contactKeys;
    }
}

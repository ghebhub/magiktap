package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactData} and its DTO {@link ContactDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactDataMapper extends EntityMapper<ContactDataDTO, ContactData> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactDataDTO toDto(ContactData contactData);

    @Mapping(source = "contactId", target = "contact")
    ContactData toEntity(ContactDataDTO contactDataDTO);

    default ContactData fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactData contactData = new ContactData();
        contactData.setId(id);
        return contactData;
    }
}

package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactMailAddressDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactMailAddress} and its DTO {@link ContactMailAddressDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactMailAddressMapper extends EntityMapper<ContactMailAddressDTO, ContactMailAddress> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactMailAddressDTO toDto(ContactMailAddress contactMailAddress);

    @Mapping(source = "contactId", target = "contact")
    ContactMailAddress toEntity(ContactMailAddressDTO contactMailAddressDTO);

    default ContactMailAddress fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactMailAddress contactMailAddress = new ContactMailAddress();
        contactMailAddress.setId(id);
        return contactMailAddress;
    }
}

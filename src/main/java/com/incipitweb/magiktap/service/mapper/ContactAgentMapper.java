package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactAgentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactAgent} and its DTO {@link ContactAgentDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactAgentMapper extends EntityMapper<ContactAgentDTO, ContactAgent> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactAgentDTO toDto(ContactAgent contactAgent);

    @Mapping(source = "contactId", target = "contact")
    ContactAgent toEntity(ContactAgentDTO contactAgentDTO);

    default ContactAgent fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactAgent contactAgent = new ContactAgent();
        contactAgent.setId(id);
        return contactAgent;
    }
}

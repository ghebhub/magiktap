package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Contact} and its DTO {@link ContactDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContactMapper extends EntityMapper<ContactDTO, Contact> {


    @Mapping(target = "contactAgents", ignore = true)
    @Mapping(target = "removeContactAgent", ignore = true)
    @Mapping(target = "contactCategories", ignore = true)
    @Mapping(target = "removeContactCategories", ignore = true)
    @Mapping(target = "contactData", ignore = true)
    @Mapping(target = "removeContactData", ignore = true)
    @Mapping(target = "contactEmails", ignore = true)
    @Mapping(target = "removeContactEmail", ignore = true)
    @Mapping(target = "contactKeys", ignore = true)
    @Mapping(target = "removeContactKeys", ignore = true)
    @Mapping(target = "contactMailAddresses", ignore = true)
    @Mapping(target = "removeContactMailAddress", ignore = true)
    @Mapping(target = "contactNotes", ignore = true)
    @Mapping(target = "removeContactNote", ignore = true)
    @Mapping(target = "contactPhoneNumbers", ignore = true)
    @Mapping(target = "removeContactPhoneNumber", ignore = true)
    @Mapping(target = "contactXtendeds", ignore = true)
    @Mapping(target = "removeContactXtended", ignore = true)
    Contact toEntity(ContactDTO contactDTO);

    default Contact fromId(Long id) {
        if (id == null) {
            return null;
        }
        Contact contact = new Contact();
        contact.setId(id);
        return contact;
    }
}

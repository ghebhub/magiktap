package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactXtendedDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactXtended} and its DTO {@link ContactXtendedDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactXtendedMapper extends EntityMapper<ContactXtendedDTO, ContactXtended> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactXtendedDTO toDto(ContactXtended contactXtended);

    @Mapping(source = "contactId", target = "contact")
    ContactXtended toEntity(ContactXtendedDTO contactXtendedDTO);

    default ContactXtended fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactXtended contactXtended = new ContactXtended();
        contactXtended.setId(id);
        return contactXtended;
    }
}

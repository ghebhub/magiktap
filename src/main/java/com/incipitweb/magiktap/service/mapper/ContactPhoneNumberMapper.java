package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactPhoneNumber} and its DTO {@link ContactPhoneNumberDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactPhoneNumberMapper extends EntityMapper<ContactPhoneNumberDTO, ContactPhoneNumber> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactPhoneNumberDTO toDto(ContactPhoneNumber contactPhoneNumber);

    @Mapping(source = "contactId", target = "contact")
    ContactPhoneNumber toEntity(ContactPhoneNumberDTO contactPhoneNumberDTO);

    default ContactPhoneNumber fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactPhoneNumber contactPhoneNumber = new ContactPhoneNumber();
        contactPhoneNumber.setId(id);
        return contactPhoneNumber;
    }
}

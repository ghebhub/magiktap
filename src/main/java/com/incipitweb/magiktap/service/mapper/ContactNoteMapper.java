package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactNoteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactNote} and its DTO {@link ContactNoteDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactNoteMapper extends EntityMapper<ContactNoteDTO, ContactNote> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactNoteDTO toDto(ContactNote contactNote);

    @Mapping(source = "contactId", target = "contact")
    ContactNote toEntity(ContactNoteDTO contactNoteDTO);

    default ContactNote fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactNote contactNote = new ContactNote();
        contactNote.setId(id);
        return contactNote;
    }
}

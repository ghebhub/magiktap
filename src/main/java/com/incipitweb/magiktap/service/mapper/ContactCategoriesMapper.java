package com.incipitweb.magiktap.service.mapper;


import com.incipitweb.magiktap.domain.*;
import com.incipitweb.magiktap.service.dto.ContactCategoriesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ContactCategories} and its DTO {@link ContactCategoriesDTO}.
 */
@Mapper(componentModel = "spring", uses = {ContactMapper.class})
public interface ContactCategoriesMapper extends EntityMapper<ContactCategoriesDTO, ContactCategories> {

    @Mapping(source = "contact.id", target = "contactId")
    ContactCategoriesDTO toDto(ContactCategories contactCategories);

    @Mapping(source = "contactId", target = "contact")
    ContactCategories toEntity(ContactCategoriesDTO contactCategoriesDTO);

    default ContactCategories fromId(Long id) {
        if (id == null) {
            return null;
        }
        ContactCategories contactCategories = new ContactCategories();
        contactCategories.setId(id);
        return contactCategories;
    }
}

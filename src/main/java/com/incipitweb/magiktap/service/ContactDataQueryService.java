package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactData;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactDataRepository;
import com.incipitweb.magiktap.service.dto.ContactDataCriteria;
import com.incipitweb.magiktap.service.dto.ContactDataDTO;
import com.incipitweb.magiktap.service.mapper.ContactDataMapper;

/**
 * Service for executing complex queries for {@link ContactData} entities in the database.
 * The main input is a {@link ContactDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactDataDTO} or a {@link Page} of {@link ContactDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactDataQueryService extends QueryService<ContactData> {

    private final Logger log = LoggerFactory.getLogger(ContactDataQueryService.class);

    private final ContactDataRepository contactDataRepository;

    private final ContactDataMapper contactDataMapper;

    public ContactDataQueryService(ContactDataRepository contactDataRepository, ContactDataMapper contactDataMapper) {
        this.contactDataRepository = contactDataRepository;
        this.contactDataMapper = contactDataMapper;
    }

    /**
     * Return a {@link List} of {@link ContactDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactDataDTO> findByCriteria(ContactDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactData> specification = createSpecification(criteria);
        return contactDataMapper.toDto(contactDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactDataDTO> findByCriteria(ContactDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactData> specification = createSpecification(criteria);
        return contactDataRepository.findAll(specification, page)
            .map(contactDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactData> specification = createSpecification(criteria);
        return contactDataRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactDataCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactData> createSpecification(ContactDataCriteria criteria) {
        Specification<ContactData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactData_.id));
            }
            if (criteria.getDataName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataName(), ContactData_.dataName));
            }
            if (criteria.getUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUrl(), ContactData_.url));
            }
            if (criteria.getInline() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInline(), ContactData_.inline));
            }
            if (criteria.getContactTypes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactTypes(), ContactData_.contactTypes));
            }
            if (criteria.getContactEncodingTypes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactEncodingTypes(), ContactData_.contactEncodingTypes));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactData_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

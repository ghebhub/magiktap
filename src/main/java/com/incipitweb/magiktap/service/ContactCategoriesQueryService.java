package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactCategories;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactCategoriesRepository;
import com.incipitweb.magiktap.service.dto.ContactCategoriesCriteria;
import com.incipitweb.magiktap.service.dto.ContactCategoriesDTO;
import com.incipitweb.magiktap.service.mapper.ContactCategoriesMapper;

/**
 * Service for executing complex queries for {@link ContactCategories} entities in the database.
 * The main input is a {@link ContactCategoriesCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactCategoriesDTO} or a {@link Page} of {@link ContactCategoriesDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactCategoriesQueryService extends QueryService<ContactCategories> {

    private final Logger log = LoggerFactory.getLogger(ContactCategoriesQueryService.class);

    private final ContactCategoriesRepository contactCategoriesRepository;

    private final ContactCategoriesMapper contactCategoriesMapper;

    public ContactCategoriesQueryService(ContactCategoriesRepository contactCategoriesRepository, ContactCategoriesMapper contactCategoriesMapper) {
        this.contactCategoriesRepository = contactCategoriesRepository;
        this.contactCategoriesMapper = contactCategoriesMapper;
    }

    /**
     * Return a {@link List} of {@link ContactCategoriesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactCategoriesDTO> findByCriteria(ContactCategoriesCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactCategories> specification = createSpecification(criteria);
        return contactCategoriesMapper.toDto(contactCategoriesRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactCategoriesDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactCategoriesDTO> findByCriteria(ContactCategoriesCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactCategories> specification = createSpecification(criteria);
        return contactCategoriesRepository.findAll(specification, page)
            .map(contactCategoriesMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactCategoriesCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactCategories> specification = createSpecification(criteria);
        return contactCategoriesRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactCategoriesCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactCategories> createSpecification(ContactCategoriesCriteria criteria) {
        Specification<ContactCategories> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactCategories_.id));
            }
            if (criteria.getCategoryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryName(), ContactCategories_.categoryName));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactCategories_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

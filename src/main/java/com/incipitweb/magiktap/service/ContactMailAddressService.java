package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactMailAddress;
import com.incipitweb.magiktap.repository.ContactMailAddressRepository;
import com.incipitweb.magiktap.service.dto.ContactMailAddressDTO;
import com.incipitweb.magiktap.service.mapper.ContactMailAddressMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactMailAddress}.
 */
@Service
@Transactional
public class ContactMailAddressService {

    private final Logger log = LoggerFactory.getLogger(ContactMailAddressService.class);

    private final ContactMailAddressRepository contactMailAddressRepository;

    private final ContactMailAddressMapper contactMailAddressMapper;

    public ContactMailAddressService(ContactMailAddressRepository contactMailAddressRepository, ContactMailAddressMapper contactMailAddressMapper) {
        this.contactMailAddressRepository = contactMailAddressRepository;
        this.contactMailAddressMapper = contactMailAddressMapper;
    }

    /**
     * Save a contactMailAddress.
     *
     * @param contactMailAddressDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactMailAddressDTO save(ContactMailAddressDTO contactMailAddressDTO) {
        log.debug("Request to save ContactMailAddress : {}", contactMailAddressDTO);
        ContactMailAddress contactMailAddress = contactMailAddressMapper.toEntity(contactMailAddressDTO);
        contactMailAddress = contactMailAddressRepository.save(contactMailAddress);
        return contactMailAddressMapper.toDto(contactMailAddress);
    }

    /**
     * Get all the contactMailAddresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactMailAddressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactMailAddresses");
        return contactMailAddressRepository.findAll(pageable)
            .map(contactMailAddressMapper::toDto);
    }


    /**
     * Get one contactMailAddress by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactMailAddressDTO> findOne(Long id) {
        log.debug("Request to get ContactMailAddress : {}", id);
        return contactMailAddressRepository.findById(id)
            .map(contactMailAddressMapper::toDto);
    }

    /**
     * Delete the contactMailAddress by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactMailAddress : {}", id);
        contactMailAddressRepository.deleteById(id);
    }
}

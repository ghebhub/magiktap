package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactNote;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactNoteRepository;
import com.incipitweb.magiktap.service.dto.ContactNoteCriteria;
import com.incipitweb.magiktap.service.dto.ContactNoteDTO;
import com.incipitweb.magiktap.service.mapper.ContactNoteMapper;

/**
 * Service for executing complex queries for {@link ContactNote} entities in the database.
 * The main input is a {@link ContactNoteCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactNoteDTO} or a {@link Page} of {@link ContactNoteDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactNoteQueryService extends QueryService<ContactNote> {

    private final Logger log = LoggerFactory.getLogger(ContactNoteQueryService.class);

    private final ContactNoteRepository contactNoteRepository;

    private final ContactNoteMapper contactNoteMapper;

    public ContactNoteQueryService(ContactNoteRepository contactNoteRepository, ContactNoteMapper contactNoteMapper) {
        this.contactNoteRepository = contactNoteRepository;
        this.contactNoteMapper = contactNoteMapper;
    }

    /**
     * Return a {@link List} of {@link ContactNoteDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactNoteDTO> findByCriteria(ContactNoteCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactNote> specification = createSpecification(criteria);
        return contactNoteMapper.toDto(contactNoteRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactNoteDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactNoteDTO> findByCriteria(ContactNoteCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactNote> specification = createSpecification(criteria);
        return contactNoteRepository.findAll(specification, page)
            .map(contactNoteMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactNoteCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactNote> specification = createSpecification(criteria);
        return contactNoteRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactNoteCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactNote> createSpecification(ContactNoteCriteria criteria) {
        Specification<ContactNote> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactNote_.id));
            }
            if (criteria.getNote() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNote(), ContactNote_.note));
            }
            if (criteria.getContactNoteType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactNoteType(), ContactNote_.contactNoteType));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactNote_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

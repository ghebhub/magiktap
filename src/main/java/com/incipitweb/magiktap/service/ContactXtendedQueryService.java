package com.incipitweb.magiktap.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.incipitweb.magiktap.domain.ContactXtended;
import com.incipitweb.magiktap.domain.*; // for static metamodels
import com.incipitweb.magiktap.repository.ContactXtendedRepository;
import com.incipitweb.magiktap.service.dto.ContactXtendedCriteria;
import com.incipitweb.magiktap.service.dto.ContactXtendedDTO;
import com.incipitweb.magiktap.service.mapper.ContactXtendedMapper;

/**
 * Service for executing complex queries for {@link ContactXtended} entities in the database.
 * The main input is a {@link ContactXtendedCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ContactXtendedDTO} or a {@link Page} of {@link ContactXtendedDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactXtendedQueryService extends QueryService<ContactXtended> {

    private final Logger log = LoggerFactory.getLogger(ContactXtendedQueryService.class);

    private final ContactXtendedRepository contactXtendedRepository;

    private final ContactXtendedMapper contactXtendedMapper;

    public ContactXtendedQueryService(ContactXtendedRepository contactXtendedRepository, ContactXtendedMapper contactXtendedMapper) {
        this.contactXtendedRepository = contactXtendedRepository;
        this.contactXtendedMapper = contactXtendedMapper;
    }

    /**
     * Return a {@link List} of {@link ContactXtendedDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ContactXtendedDTO> findByCriteria(ContactXtendedCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ContactXtended> specification = createSpecification(criteria);
        return contactXtendedMapper.toDto(contactXtendedRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ContactXtendedDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactXtendedDTO> findByCriteria(ContactXtendedCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ContactXtended> specification = createSpecification(criteria);
        return contactXtendedRepository.findAll(specification, page)
            .map(contactXtendedMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ContactXtendedCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ContactXtended> specification = createSpecification(criteria);
        return contactXtendedRepository.count(specification);
    }

    /**
     * Function to convert {@link ContactXtendedCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ContactXtended> createSpecification(ContactXtendedCriteria criteria) {
        Specification<ContactXtended> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ContactXtended_.id));
            }
            if (criteria.getXname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getXname(), ContactXtended_.xname));
            }
            if (criteria.getXvalue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getXvalue(), ContactXtended_.xvalue));
            }
            if (criteria.getContactId() != null) {
                specification = specification.and(buildSpecification(criteria.getContactId(),
                    root -> root.join(ContactXtended_.contact, JoinType.LEFT).get(Contact_.id)));
            }
        }
        return specification;
    }
}

package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactData;
import com.incipitweb.magiktap.repository.ContactDataRepository;
import com.incipitweb.magiktap.service.dto.ContactDataDTO;
import com.incipitweb.magiktap.service.mapper.ContactDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactData}.
 */
@Service
@Transactional
public class ContactDataService {

    private final Logger log = LoggerFactory.getLogger(ContactDataService.class);

    private final ContactDataRepository contactDataRepository;

    private final ContactDataMapper contactDataMapper;

    public ContactDataService(ContactDataRepository contactDataRepository, ContactDataMapper contactDataMapper) {
        this.contactDataRepository = contactDataRepository;
        this.contactDataMapper = contactDataMapper;
    }

    /**
     * Save a contactData.
     *
     * @param contactDataDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactDataDTO save(ContactDataDTO contactDataDTO) {
        log.debug("Request to save ContactData : {}", contactDataDTO);
        ContactData contactData = contactDataMapper.toEntity(contactDataDTO);
        contactData = contactDataRepository.save(contactData);
        return contactDataMapper.toDto(contactData);
    }

    /**
     * Get all the contactData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactData");
        return contactDataRepository.findAll(pageable)
            .map(contactDataMapper::toDto);
    }


    /**
     * Get one contactData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactDataDTO> findOne(Long id) {
        log.debug("Request to get ContactData : {}", id);
        return contactDataRepository.findById(id)
            .map(contactDataMapper::toDto);
    }

    /**
     * Delete the contactData by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactData : {}", id);
        contactDataRepository.deleteById(id);
    }
}

package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactCategories;
import com.incipitweb.magiktap.repository.ContactCategoriesRepository;
import com.incipitweb.magiktap.service.dto.ContactCategoriesDTO;
import com.incipitweb.magiktap.service.mapper.ContactCategoriesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactCategories}.
 */
@Service
@Transactional
public class ContactCategoriesService {

    private final Logger log = LoggerFactory.getLogger(ContactCategoriesService.class);

    private final ContactCategoriesRepository contactCategoriesRepository;

    private final ContactCategoriesMapper contactCategoriesMapper;

    public ContactCategoriesService(ContactCategoriesRepository contactCategoriesRepository, ContactCategoriesMapper contactCategoriesMapper) {
        this.contactCategoriesRepository = contactCategoriesRepository;
        this.contactCategoriesMapper = contactCategoriesMapper;
    }

    /**
     * Save a contactCategories.
     *
     * @param contactCategoriesDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactCategoriesDTO save(ContactCategoriesDTO contactCategoriesDTO) {
        log.debug("Request to save ContactCategories : {}", contactCategoriesDTO);
        ContactCategories contactCategories = contactCategoriesMapper.toEntity(contactCategoriesDTO);
        contactCategories = contactCategoriesRepository.save(contactCategories);
        return contactCategoriesMapper.toDto(contactCategories);
    }

    /**
     * Get all the contactCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactCategoriesDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactCategories");
        return contactCategoriesRepository.findAll(pageable)
            .map(contactCategoriesMapper::toDto);
    }


    /**
     * Get one contactCategories by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactCategoriesDTO> findOne(Long id) {
        log.debug("Request to get ContactCategories : {}", id);
        return contactCategoriesRepository.findById(id)
            .map(contactCategoriesMapper::toDto);
    }

    /**
     * Delete the contactCategories by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactCategories : {}", id);
        contactCategoriesRepository.deleteById(id);
    }
}

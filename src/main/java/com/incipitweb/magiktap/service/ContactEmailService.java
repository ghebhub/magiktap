package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactEmail;
import com.incipitweb.magiktap.repository.ContactEmailRepository;
import com.incipitweb.magiktap.service.dto.ContactEmailDTO;
import com.incipitweb.magiktap.service.mapper.ContactEmailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactEmail}.
 */
@Service
@Transactional
public class ContactEmailService {

    private final Logger log = LoggerFactory.getLogger(ContactEmailService.class);

    private final ContactEmailRepository contactEmailRepository;

    private final ContactEmailMapper contactEmailMapper;

    public ContactEmailService(ContactEmailRepository contactEmailRepository, ContactEmailMapper contactEmailMapper) {
        this.contactEmailRepository = contactEmailRepository;
        this.contactEmailMapper = contactEmailMapper;
    }

    /**
     * Save a contactEmail.
     *
     * @param contactEmailDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactEmailDTO save(ContactEmailDTO contactEmailDTO) {
        log.debug("Request to save ContactEmail : {}", contactEmailDTO);
        ContactEmail contactEmail = contactEmailMapper.toEntity(contactEmailDTO);
        contactEmail = contactEmailRepository.save(contactEmail);
        return contactEmailMapper.toDto(contactEmail);
    }

    /**
     * Get all the contactEmails.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactEmailDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactEmails");
        return contactEmailRepository.findAll(pageable)
            .map(contactEmailMapper::toDto);
    }


    /**
     * Get one contactEmail by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactEmailDTO> findOne(Long id) {
        log.debug("Request to get ContactEmail : {}", id);
        return contactEmailRepository.findById(id)
            .map(contactEmailMapper::toDto);
    }

    /**
     * Delete the contactEmail by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactEmail : {}", id);
        contactEmailRepository.deleteById(id);
    }
}

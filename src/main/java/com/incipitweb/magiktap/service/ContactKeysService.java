package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactKeys;
import com.incipitweb.magiktap.repository.ContactKeysRepository;
import com.incipitweb.magiktap.service.dto.ContactKeysDTO;
import com.incipitweb.magiktap.service.mapper.ContactKeysMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactKeys}.
 */
@Service
@Transactional
public class ContactKeysService {

    private final Logger log = LoggerFactory.getLogger(ContactKeysService.class);

    private final ContactKeysRepository contactKeysRepository;

    private final ContactKeysMapper contactKeysMapper;

    public ContactKeysService(ContactKeysRepository contactKeysRepository, ContactKeysMapper contactKeysMapper) {
        this.contactKeysRepository = contactKeysRepository;
        this.contactKeysMapper = contactKeysMapper;
    }

    /**
     * Save a contactKeys.
     *
     * @param contactKeysDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactKeysDTO save(ContactKeysDTO contactKeysDTO) {
        log.debug("Request to save ContactKeys : {}", contactKeysDTO);
        ContactKeys contactKeys = contactKeysMapper.toEntity(contactKeysDTO);
        contactKeys = contactKeysRepository.save(contactKeys);
        return contactKeysMapper.toDto(contactKeys);
    }

    /**
     * Get all the contactKeys.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactKeysDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactKeys");
        return contactKeysRepository.findAll(pageable)
            .map(contactKeysMapper::toDto);
    }


    /**
     * Get one contactKeys by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactKeysDTO> findOne(Long id) {
        log.debug("Request to get ContactKeys : {}", id);
        return contactKeysRepository.findById(id)
            .map(contactKeysMapper::toDto);
    }

    /**
     * Delete the contactKeys by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactKeys : {}", id);
        contactKeysRepository.deleteById(id);
    }
}

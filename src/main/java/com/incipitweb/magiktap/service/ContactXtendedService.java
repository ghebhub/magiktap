package com.incipitweb.magiktap.service;

import com.incipitweb.magiktap.domain.ContactXtended;
import com.incipitweb.magiktap.repository.ContactXtendedRepository;
import com.incipitweb.magiktap.service.dto.ContactXtendedDTO;
import com.incipitweb.magiktap.service.mapper.ContactXtendedMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ContactXtended}.
 */
@Service
@Transactional
public class ContactXtendedService {

    private final Logger log = LoggerFactory.getLogger(ContactXtendedService.class);

    private final ContactXtendedRepository contactXtendedRepository;

    private final ContactXtendedMapper contactXtendedMapper;

    public ContactXtendedService(ContactXtendedRepository contactXtendedRepository, ContactXtendedMapper contactXtendedMapper) {
        this.contactXtendedRepository = contactXtendedRepository;
        this.contactXtendedMapper = contactXtendedMapper;
    }

    /**
     * Save a contactXtended.
     *
     * @param contactXtendedDTO the entity to save.
     * @return the persisted entity.
     */
    public ContactXtendedDTO save(ContactXtendedDTO contactXtendedDTO) {
        log.debug("Request to save ContactXtended : {}", contactXtendedDTO);
        ContactXtended contactXtended = contactXtendedMapper.toEntity(contactXtendedDTO);
        contactXtended = contactXtendedRepository.save(contactXtended);
        return contactXtendedMapper.toDto(contactXtended);
    }

    /**
     * Get all the contactXtendeds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ContactXtendedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ContactXtendeds");
        return contactXtendedRepository.findAll(pageable)
            .map(contactXtendedMapper::toDto);
    }


    /**
     * Get one contactXtended by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ContactXtendedDTO> findOne(Long id) {
        log.debug("Request to get ContactXtended : {}", id);
        return contactXtendedRepository.findById(id)
            .map(contactXtendedMapper::toDto);
    }

    /**
     * Delete the contactXtended by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ContactXtended : {}", id);
        contactXtendedRepository.deleteById(id);
    }
}

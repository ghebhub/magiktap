import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  <NavDropdown
    icon="th-list"
    name={translate('global.menu.entities.main')}
    id="entity-menu"
    style={{ maxHeight: '80vh', overflow: 'auto' }}
  >
    <MenuItem icon="asterisk" to="/contact">
      <Translate contentKey="global.menu.entities.contact" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-agent">
      <Translate contentKey="global.menu.entities.contactAgent" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-categories">
      <Translate contentKey="global.menu.entities.contactCategories" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-data">
      <Translate contentKey="global.menu.entities.contactData" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-email">
      <Translate contentKey="global.menu.entities.contactEmail" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-keys">
      <Translate contentKey="global.menu.entities.contactKeys" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-mail-address">
      <Translate contentKey="global.menu.entities.contactMailAddress" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-note">
      <Translate contentKey="global.menu.entities.contactNote" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-phone-number">
      <Translate contentKey="global.menu.entities.contactPhoneNumber" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/contact-xtended">
      <Translate contentKey="global.menu.entities.contactXtended" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);

import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import contact, {
  ContactState
} from 'app/entities/contact/contact.reducer';
// prettier-ignore
import contactAgent, {
  ContactAgentState
} from 'app/entities/contact-agent/contact-agent.reducer';
// prettier-ignore
import contactCategories, {
  ContactCategoriesState
} from 'app/entities/contact-categories/contact-categories.reducer';
// prettier-ignore
import contactData, {
  ContactDataState
} from 'app/entities/contact-data/contact-data.reducer';
// prettier-ignore
import contactEmail, {
  ContactEmailState
} from 'app/entities/contact-email/contact-email.reducer';
// prettier-ignore
import contactKeys, {
  ContactKeysState
} from 'app/entities/contact-keys/contact-keys.reducer';
// prettier-ignore
import contactMailAddress, {
  ContactMailAddressState
} from 'app/entities/contact-mail-address/contact-mail-address.reducer';
// prettier-ignore
import contactNote, {
  ContactNoteState
} from 'app/entities/contact-note/contact-note.reducer';
// prettier-ignore
import contactPhoneNumber, {
  ContactPhoneNumberState
} from 'app/entities/contact-phone-number/contact-phone-number.reducer';
// prettier-ignore
import contactXtended, {
  ContactXtendedState
} from 'app/entities/contact-xtended/contact-xtended.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly contact: ContactState;
  readonly contactAgent: ContactAgentState;
  readonly contactCategories: ContactCategoriesState;
  readonly contactData: ContactDataState;
  readonly contactEmail: ContactEmailState;
  readonly contactKeys: ContactKeysState;
  readonly contactMailAddress: ContactMailAddressState;
  readonly contactNote: ContactNoteState;
  readonly contactPhoneNumber: ContactPhoneNumberState;
  readonly contactXtended: ContactXtendedState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  contact,
  contactAgent,
  contactCategories,
  contactData,
  contactEmail,
  contactKeys,
  contactMailAddress,
  contactNote,
  contactPhoneNumber,
  contactXtended,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar,
});

export default rootReducer;

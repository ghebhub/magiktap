export interface IContactKeys {
  id?: number;
  keyData?: string;
  keyType?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactKeys> = {};

export interface IContactXtended {
  id?: number;
  xname?: string;
  xvalue?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactXtended> = {};

export interface IContactAgent {
  id?: number;
  uri?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactAgent> = {};

export interface IContactEmail {
  id?: number;
  emailAddress?: string;
  emailType?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactEmail> = {};

export interface IContactData {
  id?: number;
  dataName?: string;
  url?: string;
  inline?: string;
  contactTypes?: string;
  contactEncodingTypes?: string;
  contactDataContentType?: string;
  contactData?: any;
  contactId?: number;
}

export const defaultValue: Readonly<IContactData> = {};

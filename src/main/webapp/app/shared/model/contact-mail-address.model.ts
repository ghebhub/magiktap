export interface IContactMailAddress {
  id?: number;
  pobox?: string;
  extendedAddress?: string;
  street?: string;
  locality?: string;
  region?: string;
  postalCode?: string;
  country?: string;
  mailAddressType?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactMailAddress> = {};

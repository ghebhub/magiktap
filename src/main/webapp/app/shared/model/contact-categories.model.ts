export interface IContactCategories {
  id?: number;
  categoryName?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactCategories> = {};

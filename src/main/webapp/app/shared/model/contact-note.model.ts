export interface IContactNote {
  id?: number;
  note?: string;
  contactNoteType?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactNote> = {};

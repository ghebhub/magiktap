export interface IContactPhoneNumber {
  id?: number;
  localNumber?: string;
  phoneNumberType?: string;
  contactId?: number;
}

export const defaultValue: Readonly<IContactPhoneNumber> = {};

import { Moment } from 'moment';
import { IContactAgent } from 'app/shared/model/contact-agent.model';
import { IContactCategories } from 'app/shared/model/contact-categories.model';
import { IContactData } from 'app/shared/model/contact-data.model';
import { IContactEmail } from 'app/shared/model/contact-email.model';
import { IContactKeys } from 'app/shared/model/contact-keys.model';
import { IContactMailAddress } from 'app/shared/model/contact-mail-address.model';
import { IContactNote } from 'app/shared/model/contact-note.model';
import { IContactPhoneNumber } from 'app/shared/model/contact-phone-number.model';
import { IContactXtended } from 'app/shared/model/contact-xtended.model';

export interface IContact {
  id?: number;
  contactId?: string;
  fn?: string;
  n?: string;
  nickname?: string;
  bday?: string;
  mailer?: string;
  tz?: number;
  geoLat?: number;
  geoLong?: number;
  title?: string;
  role?: string;
  prodId?: string;
  rev?: string;
  sortString?: string;
  uid?: string;
  url?: string;
  version?: string;
  className?: string;
  contactAgents?: IContactAgent[];
  contactCategories?: IContactCategories[];
  contactData?: IContactData[];
  contactEmails?: IContactEmail[];
  contactKeys?: IContactKeys[];
  contactMailAddresses?: IContactMailAddress[];
  contactNotes?: IContactNote[];
  contactPhoneNumbers?: IContactPhoneNumber[];
  contactXtendeds?: IContactXtended[];
}

export const defaultValue: Readonly<IContact> = {};

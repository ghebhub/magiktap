import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-note.reducer';
import { IContactNote } from 'app/shared/model/contact-note.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactNoteDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactNoteDetail = (props: IContactNoteDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactNoteEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactNote.detail.title">ContactNote</Translate> [<b>{contactNoteEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="note">
              <Translate contentKey="magiktapApp.contactNote.note">Note</Translate>
            </span>
          </dt>
          <dd>{contactNoteEntity.note}</dd>
          <dt>
            <span id="contactNoteType">
              <Translate contentKey="magiktapApp.contactNote.contactNoteType">Contact Note Type</Translate>
            </span>
          </dt>
          <dd>{contactNoteEntity.contactNoteType}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactNote.contact">Contact</Translate>
          </dt>
          <dd>{contactNoteEntity.contactId ? contactNoteEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-note" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-note/${contactNoteEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactNote }: IRootState) => ({
  contactNoteEntity: contactNote.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactNoteDetail);

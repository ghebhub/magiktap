import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContactNote, defaultValue } from 'app/shared/model/contact-note.model';

export const ACTION_TYPES = {
  FETCH_CONTACTNOTE_LIST: 'contactNote/FETCH_CONTACTNOTE_LIST',
  FETCH_CONTACTNOTE: 'contactNote/FETCH_CONTACTNOTE',
  CREATE_CONTACTNOTE: 'contactNote/CREATE_CONTACTNOTE',
  UPDATE_CONTACTNOTE: 'contactNote/UPDATE_CONTACTNOTE',
  DELETE_CONTACTNOTE: 'contactNote/DELETE_CONTACTNOTE',
  RESET: 'contactNote/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContactNote>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ContactNoteState = Readonly<typeof initialState>;

// Reducer

export default (state: ContactNoteState = initialState, action): ContactNoteState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTACTNOTE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTACTNOTE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTACTNOTE):
    case REQUEST(ACTION_TYPES.UPDATE_CONTACTNOTE):
    case REQUEST(ACTION_TYPES.DELETE_CONTACTNOTE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTACTNOTE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTACTNOTE):
    case FAILURE(ACTION_TYPES.CREATE_CONTACTNOTE):
    case FAILURE(ACTION_TYPES.UPDATE_CONTACTNOTE):
    case FAILURE(ACTION_TYPES.DELETE_CONTACTNOTE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTNOTE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTNOTE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTACTNOTE):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTACTNOTE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTACTNOTE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contact-notes';

// Actions

export const getEntities: ICrudGetAllAction<IContactNote> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTNOTE_LIST,
    payload: axios.get<IContactNote>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IContactNote> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTNOTE,
    payload: axios.get<IContactNote>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContactNote> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTACTNOTE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContactNote> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTACTNOTE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContactNote> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTACTNOTE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

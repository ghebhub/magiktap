import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactNote from './contact-note';
import ContactNoteDetail from './contact-note-detail';
import ContactNoteUpdate from './contact-note-update';
import ContactNoteDeleteDialog from './contact-note-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactNoteUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactNoteUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactNoteDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactNote} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactNoteDeleteDialog} />
  </>
);

export default Routes;

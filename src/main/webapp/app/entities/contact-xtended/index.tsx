import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactXtended from './contact-xtended';
import ContactXtendedDetail from './contact-xtended-detail';
import ContactXtendedUpdate from './contact-xtended-update';
import ContactXtendedDeleteDialog from './contact-xtended-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactXtendedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactXtendedUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactXtendedDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactXtended} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactXtendedDeleteDialog} />
  </>
);

export default Routes;

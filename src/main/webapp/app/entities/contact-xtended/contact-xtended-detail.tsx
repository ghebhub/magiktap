import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-xtended.reducer';
import { IContactXtended } from 'app/shared/model/contact-xtended.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactXtendedDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactXtendedDetail = (props: IContactXtendedDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactXtendedEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactXtended.detail.title">ContactXtended</Translate> [<b>{contactXtendedEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="xname">
              <Translate contentKey="magiktapApp.contactXtended.xname">Xname</Translate>
            </span>
          </dt>
          <dd>{contactXtendedEntity.xname}</dd>
          <dt>
            <span id="xvalue">
              <Translate contentKey="magiktapApp.contactXtended.xvalue">Xvalue</Translate>
            </span>
          </dt>
          <dd>{contactXtendedEntity.xvalue}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactXtended.contact">Contact</Translate>
          </dt>
          <dd>{contactXtendedEntity.contactId ? contactXtendedEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-xtended" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-xtended/${contactXtendedEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactXtended }: IRootState) => ({
  contactXtendedEntity: contactXtended.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactXtendedDetail);

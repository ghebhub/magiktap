import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-email.reducer';
import { IContactEmail } from 'app/shared/model/contact-email.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactEmailDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactEmailDetail = (props: IContactEmailDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactEmailEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactEmail.detail.title">ContactEmail</Translate> [<b>{contactEmailEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="emailAddress">
              <Translate contentKey="magiktapApp.contactEmail.emailAddress">Email Address</Translate>
            </span>
          </dt>
          <dd>{contactEmailEntity.emailAddress}</dd>
          <dt>
            <span id="emailType">
              <Translate contentKey="magiktapApp.contactEmail.emailType">Email Type</Translate>
            </span>
          </dt>
          <dd>{contactEmailEntity.emailType}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactEmail.contact">Contact</Translate>
          </dt>
          <dd>{contactEmailEntity.contactId ? contactEmailEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-email" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-email/${contactEmailEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactEmail }: IRootState) => ({
  contactEmailEntity: contactEmail.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactEmailDetail);

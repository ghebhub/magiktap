import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactEmail from './contact-email';
import ContactEmailDetail from './contact-email-detail';
import ContactEmailUpdate from './contact-email-update';
import ContactEmailDeleteDialog from './contact-email-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactEmailUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactEmailUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactEmailDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactEmail} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactEmailDeleteDialog} />
  </>
);

export default Routes;

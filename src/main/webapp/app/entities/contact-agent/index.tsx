import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactAgent from './contact-agent';
import ContactAgentDetail from './contact-agent-detail';
import ContactAgentUpdate from './contact-agent-update';
import ContactAgentDeleteDialog from './contact-agent-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactAgentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactAgentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactAgentDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactAgent} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactAgentDeleteDialog} />
  </>
);

export default Routes;

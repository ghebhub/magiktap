import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContactAgent, defaultValue } from 'app/shared/model/contact-agent.model';

export const ACTION_TYPES = {
  FETCH_CONTACTAGENT_LIST: 'contactAgent/FETCH_CONTACTAGENT_LIST',
  FETCH_CONTACTAGENT: 'contactAgent/FETCH_CONTACTAGENT',
  CREATE_CONTACTAGENT: 'contactAgent/CREATE_CONTACTAGENT',
  UPDATE_CONTACTAGENT: 'contactAgent/UPDATE_CONTACTAGENT',
  DELETE_CONTACTAGENT: 'contactAgent/DELETE_CONTACTAGENT',
  RESET: 'contactAgent/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContactAgent>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ContactAgentState = Readonly<typeof initialState>;

// Reducer

export default (state: ContactAgentState = initialState, action): ContactAgentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTACTAGENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTACTAGENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTACTAGENT):
    case REQUEST(ACTION_TYPES.UPDATE_CONTACTAGENT):
    case REQUEST(ACTION_TYPES.DELETE_CONTACTAGENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTACTAGENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTACTAGENT):
    case FAILURE(ACTION_TYPES.CREATE_CONTACTAGENT):
    case FAILURE(ACTION_TYPES.UPDATE_CONTACTAGENT):
    case FAILURE(ACTION_TYPES.DELETE_CONTACTAGENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTAGENT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTAGENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTACTAGENT):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTACTAGENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTACTAGENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contact-agents';

// Actions

export const getEntities: ICrudGetAllAction<IContactAgent> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTAGENT_LIST,
    payload: axios.get<IContactAgent>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IContactAgent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTAGENT,
    payload: axios.get<IContactAgent>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContactAgent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTACTAGENT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContactAgent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTACTAGENT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContactAgent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTACTAGENT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

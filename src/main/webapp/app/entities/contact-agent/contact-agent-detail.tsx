import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-agent.reducer';
import { IContactAgent } from 'app/shared/model/contact-agent.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactAgentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactAgentDetail = (props: IContactAgentDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactAgentEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactAgent.detail.title">ContactAgent</Translate> [<b>{contactAgentEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="uri">
              <Translate contentKey="magiktapApp.contactAgent.uri">Uri</Translate>
            </span>
          </dt>
          <dd>{contactAgentEntity.uri}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactAgent.contact">Contact</Translate>
          </dt>
          <dd>{contactAgentEntity.contactId ? contactAgentEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-agent" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-agent/${contactAgentEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactAgent }: IRootState) => ({
  contactAgentEntity: contactAgent.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactAgentDetail);

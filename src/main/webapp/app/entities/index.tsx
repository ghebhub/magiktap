import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Contact from './contact';
import ContactAgent from './contact-agent';
import ContactCategories from './contact-categories';
import ContactData from './contact-data';
import ContactEmail from './contact-email';
import ContactKeys from './contact-keys';
import ContactMailAddress from './contact-mail-address';
import ContactNote from './contact-note';
import ContactPhoneNumber from './contact-phone-number';
import ContactXtended from './contact-xtended';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}contact`} component={Contact} />
      <ErrorBoundaryRoute path={`${match.url}contact-agent`} component={ContactAgent} />
      <ErrorBoundaryRoute path={`${match.url}contact-categories`} component={ContactCategories} />
      <ErrorBoundaryRoute path={`${match.url}contact-data`} component={ContactData} />
      <ErrorBoundaryRoute path={`${match.url}contact-email`} component={ContactEmail} />
      <ErrorBoundaryRoute path={`${match.url}contact-keys`} component={ContactKeys} />
      <ErrorBoundaryRoute path={`${match.url}contact-mail-address`} component={ContactMailAddress} />
      <ErrorBoundaryRoute path={`${match.url}contact-note`} component={ContactNote} />
      <ErrorBoundaryRoute path={`${match.url}contact-phone-number`} component={ContactPhoneNumber} />
      <ErrorBoundaryRoute path={`${match.url}contact-xtended`} component={ContactXtended} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;

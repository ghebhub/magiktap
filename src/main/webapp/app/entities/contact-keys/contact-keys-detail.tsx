import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-keys.reducer';
import { IContactKeys } from 'app/shared/model/contact-keys.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactKeysDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactKeysDetail = (props: IContactKeysDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactKeysEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactKeys.detail.title">ContactKeys</Translate> [<b>{contactKeysEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="keyData">
              <Translate contentKey="magiktapApp.contactKeys.keyData">Key Data</Translate>
            </span>
          </dt>
          <dd>{contactKeysEntity.keyData}</dd>
          <dt>
            <span id="keyType">
              <Translate contentKey="magiktapApp.contactKeys.keyType">Key Type</Translate>
            </span>
          </dt>
          <dd>{contactKeysEntity.keyType}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactKeys.contact">Contact</Translate>
          </dt>
          <dd>{contactKeysEntity.contactId ? contactKeysEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-keys" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-keys/${contactKeysEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactKeys }: IRootState) => ({
  contactKeysEntity: contactKeys.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactKeysDetail);

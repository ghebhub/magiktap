import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactKeys from './contact-keys';
import ContactKeysDetail from './contact-keys-detail';
import ContactKeysUpdate from './contact-keys-update';
import ContactKeysDeleteDialog from './contact-keys-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactKeysUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactKeysUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactKeysDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactKeys} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactKeysDeleteDialog} />
  </>
);

export default Routes;

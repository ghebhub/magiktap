import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './contact.reducer';
import { IContact } from 'app/shared/model/contact.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IContactUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactUpdate = (props: IContactUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { contactEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/contact' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    values.bday = convertDateTimeToServer(values.bday);

    if (errors.length === 0) {
      const entity = {
        ...contactEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="magiktapApp.contact.home.createOrEditLabel">
            <Translate contentKey="magiktapApp.contact.home.createOrEditLabel">Create or edit a Contact</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : contactEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="contact-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="contact-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="contactIdLabel" for="contact-contactId">
                  <Translate contentKey="magiktapApp.contact.contactId">Contact Id</Translate>
                </Label>
                <AvField
                  id="contact-contactId"
                  type="text"
                  name="contactId"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 36, errorMessage: translate('entity.validation.maxlength', { max: 36 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="fnLabel" for="contact-fn">
                  <Translate contentKey="magiktapApp.contact.fn">Fn</Translate>
                </Label>
                <AvField
                  id="contact-fn"
                  type="text"
                  name="fn"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nLabel" for="contact-n">
                  <Translate contentKey="magiktapApp.contact.n">N</Translate>
                </Label>
                <AvField
                  id="contact-n"
                  type="text"
                  name="n"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="nicknameLabel" for="contact-nickname">
                  <Translate contentKey="magiktapApp.contact.nickname">Nickname</Translate>
                </Label>
                <AvField
                  id="contact-nickname"
                  type="text"
                  name="nickname"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="bdayLabel" for="contact-bday">
                  <Translate contentKey="magiktapApp.contact.bday">Bday</Translate>
                </Label>
                <AvInput
                  id="contact-bday"
                  type="datetime-local"
                  className="form-control"
                  name="bday"
                  placeholder={'YYYY-MM-DD HH:mm'}
                  value={isNew ? displayDefaultDateTime() : convertDateTimeFromServer(props.contactEntity.bday)}
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mailerLabel" for="contact-mailer">
                  <Translate contentKey="magiktapApp.contact.mailer">Mailer</Translate>
                </Label>
                <AvField
                  id="contact-mailer"
                  type="text"
                  name="mailer"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="tzLabel" for="contact-tz">
                  <Translate contentKey="magiktapApp.contact.tz">Tz</Translate>
                </Label>
                <AvField id="contact-tz" type="string" className="form-control" name="tz" />
              </AvGroup>
              <AvGroup>
                <Label id="geoLatLabel" for="contact-geoLat">
                  <Translate contentKey="magiktapApp.contact.geoLat">Geo Lat</Translate>
                </Label>
                <AvField id="contact-geoLat" type="string" className="form-control" name="geoLat" />
              </AvGroup>
              <AvGroup>
                <Label id="geoLongLabel" for="contact-geoLong">
                  <Translate contentKey="magiktapApp.contact.geoLong">Geo Long</Translate>
                </Label>
                <AvField id="contact-geoLong" type="string" className="form-control" name="geoLong" />
              </AvGroup>
              <AvGroup>
                <Label id="titleLabel" for="contact-title">
                  <Translate contentKey="magiktapApp.contact.title">Title</Translate>
                </Label>
                <AvField
                  id="contact-title"
                  type="text"
                  name="title"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="roleLabel" for="contact-role">
                  <Translate contentKey="magiktapApp.contact.role">Role</Translate>
                </Label>
                <AvField
                  id="contact-role"
                  type="text"
                  name="role"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="prodIdLabel" for="contact-prodId">
                  <Translate contentKey="magiktapApp.contact.prodId">Prod Id</Translate>
                </Label>
                <AvField
                  id="contact-prodId"
                  type="text"
                  name="prodId"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="revLabel" for="contact-rev">
                  <Translate contentKey="magiktapApp.contact.rev">Rev</Translate>
                </Label>
                <AvField
                  id="contact-rev"
                  type="text"
                  name="rev"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="sortStringLabel" for="contact-sortString">
                  <Translate contentKey="magiktapApp.contact.sortString">Sort String</Translate>
                </Label>
                <AvField
                  id="contact-sortString"
                  type="text"
                  name="sortString"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="uidLabel" for="contact-uid">
                  <Translate contentKey="magiktapApp.contact.uid">Uid</Translate>
                </Label>
                <AvField
                  id="contact-uid"
                  type="text"
                  name="uid"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="urlLabel" for="contact-url">
                  <Translate contentKey="magiktapApp.contact.url">Url</Translate>
                </Label>
                <AvField
                  id="contact-url"
                  type="text"
                  name="url"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="versionLabel" for="contact-version">
                  <Translate contentKey="magiktapApp.contact.version">Version</Translate>
                </Label>
                <AvField
                  id="contact-version"
                  type="text"
                  name="version"
                  validate={{
                    maxLength: { value: 10, errorMessage: translate('entity.validation.maxlength', { max: 10 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="classNameLabel" for="contact-className">
                  <Translate contentKey="magiktapApp.contact.className">Class Name</Translate>
                </Label>
                <AvField
                  id="contact-className"
                  type="text"
                  name="className"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/contact" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  contactEntity: storeState.contact.entity,
  loading: storeState.contact.loading,
  updating: storeState.contact.updating,
  updateSuccess: storeState.contact.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactUpdate);

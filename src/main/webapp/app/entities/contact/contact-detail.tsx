import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact.reducer';
import { IContact } from 'app/shared/model/contact.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactDetail = (props: IContactDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contact.detail.title">Contact</Translate> [<b>{contactEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="contactId">
              <Translate contentKey="magiktapApp.contact.contactId">Contact Id</Translate>
            </span>
          </dt>
          <dd>{contactEntity.contactId}</dd>
          <dt>
            <span id="fn">
              <Translate contentKey="magiktapApp.contact.fn">Fn</Translate>
            </span>
          </dt>
          <dd>{contactEntity.fn}</dd>
          <dt>
            <span id="n">
              <Translate contentKey="magiktapApp.contact.n">N</Translate>
            </span>
          </dt>
          <dd>{contactEntity.n}</dd>
          <dt>
            <span id="nickname">
              <Translate contentKey="magiktapApp.contact.nickname">Nickname</Translate>
            </span>
          </dt>
          <dd>{contactEntity.nickname}</dd>
          <dt>
            <span id="bday">
              <Translate contentKey="magiktapApp.contact.bday">Bday</Translate>
            </span>
          </dt>
          <dd>{contactEntity.bday ? <TextFormat value={contactEntity.bday} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="mailer">
              <Translate contentKey="magiktapApp.contact.mailer">Mailer</Translate>
            </span>
          </dt>
          <dd>{contactEntity.mailer}</dd>
          <dt>
            <span id="tz">
              <Translate contentKey="magiktapApp.contact.tz">Tz</Translate>
            </span>
          </dt>
          <dd>{contactEntity.tz}</dd>
          <dt>
            <span id="geoLat">
              <Translate contentKey="magiktapApp.contact.geoLat">Geo Lat</Translate>
            </span>
          </dt>
          <dd>{contactEntity.geoLat}</dd>
          <dt>
            <span id="geoLong">
              <Translate contentKey="magiktapApp.contact.geoLong">Geo Long</Translate>
            </span>
          </dt>
          <dd>{contactEntity.geoLong}</dd>
          <dt>
            <span id="title">
              <Translate contentKey="magiktapApp.contact.title">Title</Translate>
            </span>
          </dt>
          <dd>{contactEntity.title}</dd>
          <dt>
            <span id="role">
              <Translate contentKey="magiktapApp.contact.role">Role</Translate>
            </span>
          </dt>
          <dd>{contactEntity.role}</dd>
          <dt>
            <span id="prodId">
              <Translate contentKey="magiktapApp.contact.prodId">Prod Id</Translate>
            </span>
          </dt>
          <dd>{contactEntity.prodId}</dd>
          <dt>
            <span id="rev">
              <Translate contentKey="magiktapApp.contact.rev">Rev</Translate>
            </span>
          </dt>
          <dd>{contactEntity.rev}</dd>
          <dt>
            <span id="sortString">
              <Translate contentKey="magiktapApp.contact.sortString">Sort String</Translate>
            </span>
          </dt>
          <dd>{contactEntity.sortString}</dd>
          <dt>
            <span id="uid">
              <Translate contentKey="magiktapApp.contact.uid">Uid</Translate>
            </span>
          </dt>
          <dd>{contactEntity.uid}</dd>
          <dt>
            <span id="url">
              <Translate contentKey="magiktapApp.contact.url">Url</Translate>
            </span>
          </dt>
          <dd>{contactEntity.url}</dd>
          <dt>
            <span id="version">
              <Translate contentKey="magiktapApp.contact.version">Version</Translate>
            </span>
          </dt>
          <dd>{contactEntity.version}</dd>
          <dt>
            <span id="className">
              <Translate contentKey="magiktapApp.contact.className">Class Name</Translate>
            </span>
          </dt>
          <dd>{contactEntity.className}</dd>
        </dl>
        <Button tag={Link} to="/contact" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact/${contactEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contact }: IRootState) => ({
  contactEntity: contact.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetail);

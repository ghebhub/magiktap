import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-phone-number.reducer';
import { IContactPhoneNumber } from 'app/shared/model/contact-phone-number.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactPhoneNumberDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactPhoneNumberDetail = (props: IContactPhoneNumberDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactPhoneNumberEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactPhoneNumber.detail.title">ContactPhoneNumber</Translate> [
          <b>{contactPhoneNumberEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="localNumber">
              <Translate contentKey="magiktapApp.contactPhoneNumber.localNumber">Local Number</Translate>
            </span>
          </dt>
          <dd>{contactPhoneNumberEntity.localNumber}</dd>
          <dt>
            <span id="phoneNumberType">
              <Translate contentKey="magiktapApp.contactPhoneNumber.phoneNumberType">Phone Number Type</Translate>
            </span>
          </dt>
          <dd>{contactPhoneNumberEntity.phoneNumberType}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactPhoneNumber.contact">Contact</Translate>
          </dt>
          <dd>{contactPhoneNumberEntity.contactId ? contactPhoneNumberEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-phone-number" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-phone-number/${contactPhoneNumberEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactPhoneNumber }: IRootState) => ({
  contactPhoneNumberEntity: contactPhoneNumber.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactPhoneNumberDetail);

import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactPhoneNumber from './contact-phone-number';
import ContactPhoneNumberDetail from './contact-phone-number-detail';
import ContactPhoneNumberUpdate from './contact-phone-number-update';
import ContactPhoneNumberDeleteDialog from './contact-phone-number-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactPhoneNumberUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactPhoneNumberUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactPhoneNumberDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactPhoneNumber} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactPhoneNumberDeleteDialog} />
  </>
);

export default Routes;

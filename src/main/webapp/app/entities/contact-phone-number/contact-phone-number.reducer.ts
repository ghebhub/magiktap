import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContactPhoneNumber, defaultValue } from 'app/shared/model/contact-phone-number.model';

export const ACTION_TYPES = {
  FETCH_CONTACTPHONENUMBER_LIST: 'contactPhoneNumber/FETCH_CONTACTPHONENUMBER_LIST',
  FETCH_CONTACTPHONENUMBER: 'contactPhoneNumber/FETCH_CONTACTPHONENUMBER',
  CREATE_CONTACTPHONENUMBER: 'contactPhoneNumber/CREATE_CONTACTPHONENUMBER',
  UPDATE_CONTACTPHONENUMBER: 'contactPhoneNumber/UPDATE_CONTACTPHONENUMBER',
  DELETE_CONTACTPHONENUMBER: 'contactPhoneNumber/DELETE_CONTACTPHONENUMBER',
  RESET: 'contactPhoneNumber/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContactPhoneNumber>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ContactPhoneNumberState = Readonly<typeof initialState>;

// Reducer

export default (state: ContactPhoneNumberState = initialState, action): ContactPhoneNumberState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTACTPHONENUMBER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTACTPHONENUMBER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTACTPHONENUMBER):
    case REQUEST(ACTION_TYPES.UPDATE_CONTACTPHONENUMBER):
    case REQUEST(ACTION_TYPES.DELETE_CONTACTPHONENUMBER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTACTPHONENUMBER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTACTPHONENUMBER):
    case FAILURE(ACTION_TYPES.CREATE_CONTACTPHONENUMBER):
    case FAILURE(ACTION_TYPES.UPDATE_CONTACTPHONENUMBER):
    case FAILURE(ACTION_TYPES.DELETE_CONTACTPHONENUMBER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTPHONENUMBER_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTPHONENUMBER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTACTPHONENUMBER):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTACTPHONENUMBER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTACTPHONENUMBER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contact-phone-numbers';

// Actions

export const getEntities: ICrudGetAllAction<IContactPhoneNumber> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTPHONENUMBER_LIST,
    payload: axios.get<IContactPhoneNumber>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IContactPhoneNumber> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTPHONENUMBER,
    payload: axios.get<IContactPhoneNumber>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContactPhoneNumber> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTACTPHONENUMBER,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContactPhoneNumber> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTACTPHONENUMBER,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContactPhoneNumber> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTACTPHONENUMBER,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

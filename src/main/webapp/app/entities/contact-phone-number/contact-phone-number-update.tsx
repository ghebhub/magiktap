import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IContact } from 'app/shared/model/contact.model';
import { getEntities as getContacts } from 'app/entities/contact/contact.reducer';
import { getEntity, updateEntity, createEntity, reset } from './contact-phone-number.reducer';
import { IContactPhoneNumber } from 'app/shared/model/contact-phone-number.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IContactPhoneNumberUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactPhoneNumberUpdate = (props: IContactPhoneNumberUpdateProps) => {
  const [contactId, setContactId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { contactPhoneNumberEntity, contacts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/contact-phone-number' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getContacts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...contactPhoneNumberEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="magiktapApp.contactPhoneNumber.home.createOrEditLabel">
            <Translate contentKey="magiktapApp.contactPhoneNumber.home.createOrEditLabel">Create or edit a ContactPhoneNumber</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : contactPhoneNumberEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="contact-phone-number-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="contact-phone-number-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="localNumberLabel" for="contact-phone-number-localNumber">
                  <Translate contentKey="magiktapApp.contactPhoneNumber.localNumber">Local Number</Translate>
                </Label>
                <AvField
                  id="contact-phone-number-localNumber"
                  type="text"
                  name="localNumber"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="phoneNumberTypeLabel" for="contact-phone-number-phoneNumberType">
                  <Translate contentKey="magiktapApp.contactPhoneNumber.phoneNumberType">Phone Number Type</Translate>
                </Label>
                <AvField id="contact-phone-number-phoneNumberType" type="text" name="phoneNumberType" />
              </AvGroup>
              <AvGroup>
                <Label for="contact-phone-number-contact">
                  <Translate contentKey="magiktapApp.contactPhoneNumber.contact">Contact</Translate>
                </Label>
                <AvInput id="contact-phone-number-contact" type="select" className="form-control" name="contactId">
                  <option value="" key="0" />
                  {contacts
                    ? contacts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/contact-phone-number" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  contacts: storeState.contact.entities,
  contactPhoneNumberEntity: storeState.contactPhoneNumber.entity,
  loading: storeState.contactPhoneNumber.loading,
  updating: storeState.contactPhoneNumber.updating,
  updateSuccess: storeState.contactPhoneNumber.updateSuccess,
});

const mapDispatchToProps = {
  getContacts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactPhoneNumberUpdate);

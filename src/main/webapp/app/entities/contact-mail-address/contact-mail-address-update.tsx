import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IContact } from 'app/shared/model/contact.model';
import { getEntities as getContacts } from 'app/entities/contact/contact.reducer';
import { getEntity, updateEntity, createEntity, reset } from './contact-mail-address.reducer';
import { IContactMailAddress } from 'app/shared/model/contact-mail-address.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IContactMailAddressUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactMailAddressUpdate = (props: IContactMailAddressUpdateProps) => {
  const [contactId, setContactId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { contactMailAddressEntity, contacts, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/contact-mail-address' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getContacts();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...contactMailAddressEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="magiktapApp.contactMailAddress.home.createOrEditLabel">
            <Translate contentKey="magiktapApp.contactMailAddress.home.createOrEditLabel">Create or edit a ContactMailAddress</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : contactMailAddressEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="contact-mail-address-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="contact-mail-address-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="poboxLabel" for="contact-mail-address-pobox">
                  <Translate contentKey="magiktapApp.contactMailAddress.pobox">Pobox</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-pobox"
                  type="text"
                  name="pobox"
                  validate={{
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="extendedAddressLabel" for="contact-mail-address-extendedAddress">
                  <Translate contentKey="magiktapApp.contactMailAddress.extendedAddress">Extended Address</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-extendedAddress"
                  type="text"
                  name="extendedAddress"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="streetLabel" for="contact-mail-address-street">
                  <Translate contentKey="magiktapApp.contactMailAddress.street">Street</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-street"
                  type="text"
                  name="street"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="localityLabel" for="contact-mail-address-locality">
                  <Translate contentKey="magiktapApp.contactMailAddress.locality">Locality</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-locality"
                  type="text"
                  name="locality"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="regionLabel" for="contact-mail-address-region">
                  <Translate contentKey="magiktapApp.contactMailAddress.region">Region</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-region"
                  type="text"
                  name="region"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="postalCodeLabel" for="contact-mail-address-postalCode">
                  <Translate contentKey="magiktapApp.contactMailAddress.postalCode">Postal Code</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-postalCode"
                  type="text"
                  name="postalCode"
                  validate={{
                    maxLength: { value: 30, errorMessage: translate('entity.validation.maxlength', { max: 30 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="countryLabel" for="contact-mail-address-country">
                  <Translate contentKey="magiktapApp.contactMailAddress.country">Country</Translate>
                </Label>
                <AvField
                  id="contact-mail-address-country"
                  type="text"
                  name="country"
                  validate={{
                    maxLength: { value: 50, errorMessage: translate('entity.validation.maxlength', { max: 50 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="mailAddressTypeLabel" for="contact-mail-address-mailAddressType">
                  <Translate contentKey="magiktapApp.contactMailAddress.mailAddressType">Mail Address Type</Translate>
                </Label>
                <AvField id="contact-mail-address-mailAddressType" type="text" name="mailAddressType" />
              </AvGroup>
              <AvGroup>
                <Label for="contact-mail-address-contact">
                  <Translate contentKey="magiktapApp.contactMailAddress.contact">Contact</Translate>
                </Label>
                <AvInput id="contact-mail-address-contact" type="select" className="form-control" name="contactId">
                  <option value="" key="0" />
                  {contacts
                    ? contacts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/contact-mail-address" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  contacts: storeState.contact.entities,
  contactMailAddressEntity: storeState.contactMailAddress.entity,
  loading: storeState.contactMailAddress.loading,
  updating: storeState.contactMailAddress.updating,
  updateSuccess: storeState.contactMailAddress.updateSuccess,
});

const mapDispatchToProps = {
  getContacts,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactMailAddressUpdate);

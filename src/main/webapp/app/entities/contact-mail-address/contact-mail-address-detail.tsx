import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-mail-address.reducer';
import { IContactMailAddress } from 'app/shared/model/contact-mail-address.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactMailAddressDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactMailAddressDetail = (props: IContactMailAddressDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactMailAddressEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactMailAddress.detail.title">ContactMailAddress</Translate> [
          <b>{contactMailAddressEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="pobox">
              <Translate contentKey="magiktapApp.contactMailAddress.pobox">Pobox</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.pobox}</dd>
          <dt>
            <span id="extendedAddress">
              <Translate contentKey="magiktapApp.contactMailAddress.extendedAddress">Extended Address</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.extendedAddress}</dd>
          <dt>
            <span id="street">
              <Translate contentKey="magiktapApp.contactMailAddress.street">Street</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.street}</dd>
          <dt>
            <span id="locality">
              <Translate contentKey="magiktapApp.contactMailAddress.locality">Locality</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.locality}</dd>
          <dt>
            <span id="region">
              <Translate contentKey="magiktapApp.contactMailAddress.region">Region</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.region}</dd>
          <dt>
            <span id="postalCode">
              <Translate contentKey="magiktapApp.contactMailAddress.postalCode">Postal Code</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.postalCode}</dd>
          <dt>
            <span id="country">
              <Translate contentKey="magiktapApp.contactMailAddress.country">Country</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.country}</dd>
          <dt>
            <span id="mailAddressType">
              <Translate contentKey="magiktapApp.contactMailAddress.mailAddressType">Mail Address Type</Translate>
            </span>
          </dt>
          <dd>{contactMailAddressEntity.mailAddressType}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactMailAddress.contact">Contact</Translate>
          </dt>
          <dd>{contactMailAddressEntity.contactId ? contactMailAddressEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-mail-address" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-mail-address/${contactMailAddressEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactMailAddress }: IRootState) => ({
  contactMailAddressEntity: contactMailAddress.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactMailAddressDetail);

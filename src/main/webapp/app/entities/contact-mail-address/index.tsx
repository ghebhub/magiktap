import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactMailAddress from './contact-mail-address';
import ContactMailAddressDetail from './contact-mail-address-detail';
import ContactMailAddressUpdate from './contact-mail-address-update';
import ContactMailAddressDeleteDialog from './contact-mail-address-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactMailAddressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactMailAddressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactMailAddressDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactMailAddress} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactMailAddressDeleteDialog} />
  </>
);

export default Routes;

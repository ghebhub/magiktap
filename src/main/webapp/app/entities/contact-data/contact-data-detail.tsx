import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction, openFile, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-data.reducer';
import { IContactData } from 'app/shared/model/contact-data.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactDataDetail = (props: IContactDataDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactDataEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactData.detail.title">ContactData</Translate> [<b>{contactDataEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="dataName">
              <Translate contentKey="magiktapApp.contactData.dataName">Data Name</Translate>
            </span>
          </dt>
          <dd>{contactDataEntity.dataName}</dd>
          <dt>
            <span id="url">
              <Translate contentKey="magiktapApp.contactData.url">Url</Translate>
            </span>
          </dt>
          <dd>{contactDataEntity.url}</dd>
          <dt>
            <span id="inline">
              <Translate contentKey="magiktapApp.contactData.inline">Inline</Translate>
            </span>
          </dt>
          <dd>{contactDataEntity.inline}</dd>
          <dt>
            <span id="contactTypes">
              <Translate contentKey="magiktapApp.contactData.contactTypes">Contact Types</Translate>
            </span>
          </dt>
          <dd>{contactDataEntity.contactTypes}</dd>
          <dt>
            <span id="contactEncodingTypes">
              <Translate contentKey="magiktapApp.contactData.contactEncodingTypes">Contact Encoding Types</Translate>
            </span>
          </dt>
          <dd>{contactDataEntity.contactEncodingTypes}</dd>
          <dt>
            <span id="contactData">
              <Translate contentKey="magiktapApp.contactData.contactData">Contact Data</Translate>
            </span>
          </dt>
          <dd>
            {contactDataEntity.contactData ? (
              <div>
                {contactDataEntity.contactDataContentType ? (
                  <a onClick={openFile(contactDataEntity.contactDataContentType, contactDataEntity.contactData)}>
                    <Translate contentKey="entity.action.open">Open</Translate>&nbsp;
                  </a>
                ) : null}
                <span>
                  {contactDataEntity.contactDataContentType}, {byteSize(contactDataEntity.contactData)}
                </span>
              </div>
            ) : null}
          </dd>
          <dt>
            <Translate contentKey="magiktapApp.contactData.contact">Contact</Translate>
          </dt>
          <dd>{contactDataEntity.contactId ? contactDataEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-data" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-data/${contactDataEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactData }: IRootState) => ({
  contactDataEntity: contactData.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactDataDetail);

import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactData from './contact-data';
import ContactDataDetail from './contact-data-detail';
import ContactDataUpdate from './contact-data-update';
import ContactDataDeleteDialog from './contact-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactData} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactDataDeleteDialog} />
  </>
);

export default Routes;

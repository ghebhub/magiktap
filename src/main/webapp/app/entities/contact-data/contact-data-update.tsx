import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, setFileData, openFile, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IContact } from 'app/shared/model/contact.model';
import { getEntities as getContacts } from 'app/entities/contact/contact.reducer';
import { getEntity, updateEntity, createEntity, setBlob, reset } from './contact-data.reducer';
import { IContactData } from 'app/shared/model/contact-data.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IContactDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactDataUpdate = (props: IContactDataUpdateProps) => {
  const [contactId, setContactId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { contactDataEntity, contacts, loading, updating } = props;

  const { contactData, contactDataContentType } = contactDataEntity;

  const handleClose = () => {
    props.history.push('/contact-data' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getContacts();
  }, []);

  const onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => props.setBlob(name, data, contentType), isAnImage);
  };

  const clearBlob = name => () => {
    props.setBlob(name, undefined, undefined);
  };

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...contactDataEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="magiktapApp.contactData.home.createOrEditLabel">
            <Translate contentKey="magiktapApp.contactData.home.createOrEditLabel">Create or edit a ContactData</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : contactDataEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="contact-data-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="contact-data-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="dataNameLabel" for="contact-data-dataName">
                  <Translate contentKey="magiktapApp.contactData.dataName">Data Name</Translate>
                </Label>
                <AvField
                  id="contact-data-dataName"
                  type="text"
                  name="dataName"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 10, errorMessage: translate('entity.validation.maxlength', { max: 10 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="urlLabel" for="contact-data-url">
                  <Translate contentKey="magiktapApp.contactData.url">Url</Translate>
                </Label>
                <AvField
                  id="contact-data-url"
                  type="text"
                  name="url"
                  validate={{
                    maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="inlineLabel" for="contact-data-inline">
                  <Translate contentKey="magiktapApp.contactData.inline">Inline</Translate>
                </Label>
                <AvField
                  id="contact-data-inline"
                  type="text"
                  name="inline"
                  validate={{
                    maxLength: { value: 1, errorMessage: translate('entity.validation.maxlength', { max: 1 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="contactTypesLabel" for="contact-data-contactTypes">
                  <Translate contentKey="magiktapApp.contactData.contactTypes">Contact Types</Translate>
                </Label>
                <AvField
                  id="contact-data-contactTypes"
                  type="text"
                  name="contactTypes"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 10, errorMessage: translate('entity.validation.maxlength', { max: 10 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <Label id="contactEncodingTypesLabel" for="contact-data-contactEncodingTypes">
                  <Translate contentKey="magiktapApp.contactData.contactEncodingTypes">Contact Encoding Types</Translate>
                </Label>
                <AvField
                  id="contact-data-contactEncodingTypes"
                  type="text"
                  name="contactEncodingTypes"
                  validate={{
                    required: { value: true, errorMessage: translate('entity.validation.required') },
                    maxLength: { value: 10, errorMessage: translate('entity.validation.maxlength', { max: 10 }) },
                  }}
                />
              </AvGroup>
              <AvGroup>
                <AvGroup>
                  <Label id="contactDataLabel" for="contactData">
                    <Translate contentKey="magiktapApp.contactData.contactData">Contact Data</Translate>
                  </Label>
                  <br />
                  {contactData ? (
                    <div>
                      {contactDataContentType ? (
                        <a onClick={openFile(contactDataContentType, contactData)}>
                          <Translate contentKey="entity.action.open">Open</Translate>
                        </a>
                      ) : null}
                      <br />
                      <Row>
                        <Col md="11">
                          <span>
                            {contactDataContentType}, {byteSize(contactData)}
                          </span>
                        </Col>
                        <Col md="1">
                          <Button color="danger" onClick={clearBlob('contactData')}>
                            <FontAwesomeIcon icon="times-circle" />
                          </Button>
                        </Col>
                      </Row>
                    </div>
                  ) : null}
                  <input id="file_contactData" type="file" onChange={onBlobChange(false, 'contactData')} />
                  <AvInput type="hidden" name="contactData" value={contactData} />
                </AvGroup>
              </AvGroup>
              <AvGroup>
                <Label for="contact-data-contact">
                  <Translate contentKey="magiktapApp.contactData.contact">Contact</Translate>
                </Label>
                <AvInput id="contact-data-contact" type="select" className="form-control" name="contactId">
                  <option value="" key="0" />
                  {contacts
                    ? contacts.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.id}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/contact-data" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  contacts: storeState.contact.entities,
  contactDataEntity: storeState.contactData.entity,
  loading: storeState.contactData.loading,
  updating: storeState.contactData.updating,
  updateSuccess: storeState.contactData.updateSuccess,
});

const mapDispatchToProps = {
  getContacts,
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactDataUpdate);

import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContactData, defaultValue } from 'app/shared/model/contact-data.model';

export const ACTION_TYPES = {
  FETCH_CONTACTDATA_LIST: 'contactData/FETCH_CONTACTDATA_LIST',
  FETCH_CONTACTDATA: 'contactData/FETCH_CONTACTDATA',
  CREATE_CONTACTDATA: 'contactData/CREATE_CONTACTDATA',
  UPDATE_CONTACTDATA: 'contactData/UPDATE_CONTACTDATA',
  DELETE_CONTACTDATA: 'contactData/DELETE_CONTACTDATA',
  SET_BLOB: 'contactData/SET_BLOB',
  RESET: 'contactData/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContactData>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ContactDataState = Readonly<typeof initialState>;

// Reducer

export default (state: ContactDataState = initialState, action): ContactDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTACTDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTACTDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTACTDATA):
    case REQUEST(ACTION_TYPES.UPDATE_CONTACTDATA):
    case REQUEST(ACTION_TYPES.DELETE_CONTACTDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTACTDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTACTDATA):
    case FAILURE(ACTION_TYPES.CREATE_CONTACTDATA):
    case FAILURE(ACTION_TYPES.UPDATE_CONTACTDATA):
    case FAILURE(ACTION_TYPES.DELETE_CONTACTDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTDATA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTACTDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTACTDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTACTDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.SET_BLOB: {
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType,
        },
      };
    }
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contact-data';

// Actions

export const getEntities: ICrudGetAllAction<IContactData> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTDATA_LIST,
    payload: axios.get<IContactData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IContactData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTDATA,
    payload: axios.get<IContactData>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContactData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTACTDATA,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContactData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTACTDATA,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContactData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTACTDATA,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType,
  },
});

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IContactCategories, defaultValue } from 'app/shared/model/contact-categories.model';

export const ACTION_TYPES = {
  FETCH_CONTACTCATEGORIES_LIST: 'contactCategories/FETCH_CONTACTCATEGORIES_LIST',
  FETCH_CONTACTCATEGORIES: 'contactCategories/FETCH_CONTACTCATEGORIES',
  CREATE_CONTACTCATEGORIES: 'contactCategories/CREATE_CONTACTCATEGORIES',
  UPDATE_CONTACTCATEGORIES: 'contactCategories/UPDATE_CONTACTCATEGORIES',
  DELETE_CONTACTCATEGORIES: 'contactCategories/DELETE_CONTACTCATEGORIES',
  RESET: 'contactCategories/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IContactCategories>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ContactCategoriesState = Readonly<typeof initialState>;

// Reducer

export default (state: ContactCategoriesState = initialState, action): ContactCategoriesState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CONTACTCATEGORIES_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CONTACTCATEGORIES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_CONTACTCATEGORIES):
    case REQUEST(ACTION_TYPES.UPDATE_CONTACTCATEGORIES):
    case REQUEST(ACTION_TYPES.DELETE_CONTACTCATEGORIES):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_CONTACTCATEGORIES_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CONTACTCATEGORIES):
    case FAILURE(ACTION_TYPES.CREATE_CONTACTCATEGORIES):
    case FAILURE(ACTION_TYPES.UPDATE_CONTACTCATEGORIES):
    case FAILURE(ACTION_TYPES.DELETE_CONTACTCATEGORIES):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTCATEGORIES_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_CONTACTCATEGORIES):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_CONTACTCATEGORIES):
    case SUCCESS(ACTION_TYPES.UPDATE_CONTACTCATEGORIES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_CONTACTCATEGORIES):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/contact-categories';

// Actions

export const getEntities: ICrudGetAllAction<IContactCategories> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTCATEGORIES_LIST,
    payload: axios.get<IContactCategories>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IContactCategories> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CONTACTCATEGORIES,
    payload: axios.get<IContactCategories>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IContactCategories> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CONTACTCATEGORIES,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IContactCategories> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CONTACTCATEGORIES,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IContactCategories> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CONTACTCATEGORIES,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});

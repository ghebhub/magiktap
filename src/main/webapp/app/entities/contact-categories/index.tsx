import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ContactCategories from './contact-categories';
import ContactCategoriesDetail from './contact-categories-detail';
import ContactCategoriesUpdate from './contact-categories-update';
import ContactCategoriesDeleteDialog from './contact-categories-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ContactCategoriesUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ContactCategoriesUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ContactCategoriesDetail} />
      <ErrorBoundaryRoute path={match.url} component={ContactCategories} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={ContactCategoriesDeleteDialog} />
  </>
);

export default Routes;

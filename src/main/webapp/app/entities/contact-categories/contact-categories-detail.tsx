import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contact-categories.reducer';
import { IContactCategories } from 'app/shared/model/contact-categories.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContactCategoriesDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ContactCategoriesDetail = (props: IContactCategoriesDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { contactCategoriesEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="magiktapApp.contactCategories.detail.title">ContactCategories</Translate> [
          <b>{contactCategoriesEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="categoryName">
              <Translate contentKey="magiktapApp.contactCategories.categoryName">Category Name</Translate>
            </span>
          </dt>
          <dd>{contactCategoriesEntity.categoryName}</dd>
          <dt>
            <Translate contentKey="magiktapApp.contactCategories.contact">Contact</Translate>
          </dt>
          <dd>{contactCategoriesEntity.contactId ? contactCategoriesEntity.contactId : ''}</dd>
        </dl>
        <Button tag={Link} to="/contact-categories" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/contact-categories/${contactCategoriesEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ contactCategories }: IRootState) => ({
  contactCategoriesEntity: contactCategories.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ContactCategoriesDetail);

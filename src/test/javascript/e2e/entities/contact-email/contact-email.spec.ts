import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactEmailComponentsPage from './contact-email.page-object';
import ContactEmailUpdatePage from './contact-email-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactEmail e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactEmailComponentsPage: ContactEmailComponentsPage;
  let contactEmailUpdatePage: ContactEmailUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactEmailComponentsPage = new ContactEmailComponentsPage();
    contactEmailComponentsPage = await contactEmailComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactEmails', async () => {
    expect(await contactEmailComponentsPage.title.getText()).to.match(/Contact Emails/);
    expect(await contactEmailComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactEmails', async () => {
    const beforeRecordsCount = (await isVisible(contactEmailComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactEmailComponentsPage.table);
    contactEmailUpdatePage = await contactEmailComponentsPage.goToCreateContactEmail();
    await contactEmailUpdatePage.enterData();

    expect(await contactEmailComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactEmailComponentsPage.table);
    await waitUntilCount(contactEmailComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactEmailComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactEmailComponentsPage.deleteContactEmail();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactEmailComponentsPage.records, beforeRecordsCount);
      expect(await contactEmailComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactEmailComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

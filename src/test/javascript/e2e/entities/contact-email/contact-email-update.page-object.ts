import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactEmailUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactEmail.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  emailAddressInput: ElementFinder = element(by.css('input#contact-email-emailAddress'));
  emailTypeInput: ElementFinder = element(by.css('input#contact-email-emailType'));
  contactSelect: ElementFinder = element(by.css('select#contact-email-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setEmailAddressInput(emailAddress) {
    await this.emailAddressInput.sendKeys(emailAddress);
  }

  async getEmailAddressInput() {
    return this.emailAddressInput.getAttribute('value');
  }

  async setEmailTypeInput(emailType) {
    await this.emailTypeInput.sendKeys(emailType);
  }

  async getEmailTypeInput() {
    return this.emailTypeInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setEmailAddressInput('emailAddress');
    expect(await this.getEmailAddressInput()).to.match(/emailAddress/);
    await waitUntilDisplayed(this.saveButton);
    await this.setEmailTypeInput('emailType');
    expect(await this.getEmailTypeInput()).to.match(/emailType/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

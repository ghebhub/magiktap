import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactPhoneNumberUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactPhoneNumber.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  localNumberInput: ElementFinder = element(by.css('input#contact-phone-number-localNumber'));
  phoneNumberTypeInput: ElementFinder = element(by.css('input#contact-phone-number-phoneNumberType'));
  contactSelect: ElementFinder = element(by.css('select#contact-phone-number-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setLocalNumberInput(localNumber) {
    await this.localNumberInput.sendKeys(localNumber);
  }

  async getLocalNumberInput() {
    return this.localNumberInput.getAttribute('value');
  }

  async setPhoneNumberTypeInput(phoneNumberType) {
    await this.phoneNumberTypeInput.sendKeys(phoneNumberType);
  }

  async getPhoneNumberTypeInput() {
    return this.phoneNumberTypeInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setLocalNumberInput('localNumber');
    expect(await this.getLocalNumberInput()).to.match(/localNumber/);
    await waitUntilDisplayed(this.saveButton);
    await this.setPhoneNumberTypeInput('phoneNumberType');
    expect(await this.getPhoneNumberTypeInput()).to.match(/phoneNumberType/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactPhoneNumberComponentsPage from './contact-phone-number.page-object';
import ContactPhoneNumberUpdatePage from './contact-phone-number-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactPhoneNumber e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactPhoneNumberComponentsPage: ContactPhoneNumberComponentsPage;
  let contactPhoneNumberUpdatePage: ContactPhoneNumberUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactPhoneNumberComponentsPage = new ContactPhoneNumberComponentsPage();
    contactPhoneNumberComponentsPage = await contactPhoneNumberComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactPhoneNumbers', async () => {
    expect(await contactPhoneNumberComponentsPage.title.getText()).to.match(/Contact Phone Numbers/);
    expect(await contactPhoneNumberComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactPhoneNumbers', async () => {
    const beforeRecordsCount = (await isVisible(contactPhoneNumberComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactPhoneNumberComponentsPage.table);
    contactPhoneNumberUpdatePage = await contactPhoneNumberComponentsPage.goToCreateContactPhoneNumber();
    await contactPhoneNumberUpdatePage.enterData();

    expect(await contactPhoneNumberComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactPhoneNumberComponentsPage.table);
    await waitUntilCount(contactPhoneNumberComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactPhoneNumberComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactPhoneNumberComponentsPage.deleteContactPhoneNumber();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactPhoneNumberComponentsPage.records, beforeRecordsCount);
      expect(await contactPhoneNumberComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactPhoneNumberComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

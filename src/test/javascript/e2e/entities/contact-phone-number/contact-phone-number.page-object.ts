import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactPhoneNumberUpdatePage from './contact-phone-number-update.page-object';

const expect = chai.expect;
export class ContactPhoneNumberDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactPhoneNumber.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactPhoneNumber'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactPhoneNumberComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-phone-number-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-phone-number');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactPhoneNumber() {
    await this.createButton.click();
    return new ContactPhoneNumberUpdatePage();
  }

  async deleteContactPhoneNumber() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactPhoneNumberDeleteDialog = new ContactPhoneNumberDeleteDialog();
    await waitUntilDisplayed(contactPhoneNumberDeleteDialog.deleteModal);
    expect(await contactPhoneNumberDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /magiktapApp.contactPhoneNumber.delete.question/
    );
    await contactPhoneNumberDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactPhoneNumberDeleteDialog.deleteModal);

    expect(await isVisible(contactPhoneNumberDeleteDialog.deleteModal)).to.be.false;
  }
}

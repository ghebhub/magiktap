import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactKeysComponentsPage from './contact-keys.page-object';
import ContactKeysUpdatePage from './contact-keys-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactKeys e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactKeysComponentsPage: ContactKeysComponentsPage;
  let contactKeysUpdatePage: ContactKeysUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactKeysComponentsPage = new ContactKeysComponentsPage();
    contactKeysComponentsPage = await contactKeysComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactKeys', async () => {
    expect(await contactKeysComponentsPage.title.getText()).to.match(/Contact Keys/);
    expect(await contactKeysComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactKeys', async () => {
    const beforeRecordsCount = (await isVisible(contactKeysComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactKeysComponentsPage.table);
    contactKeysUpdatePage = await contactKeysComponentsPage.goToCreateContactKeys();
    await contactKeysUpdatePage.enterData();

    expect(await contactKeysComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactKeysComponentsPage.table);
    await waitUntilCount(contactKeysComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactKeysComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactKeysComponentsPage.deleteContactKeys();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactKeysComponentsPage.records, beforeRecordsCount);
      expect(await contactKeysComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactKeysComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

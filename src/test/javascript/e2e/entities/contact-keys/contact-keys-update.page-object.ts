import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactKeysUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactKeys.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  keyDataInput: ElementFinder = element(by.css('input#contact-keys-keyData'));
  keyTypeInput: ElementFinder = element(by.css('input#contact-keys-keyType'));
  contactSelect: ElementFinder = element(by.css('select#contact-keys-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setKeyDataInput(keyData) {
    await this.keyDataInput.sendKeys(keyData);
  }

  async getKeyDataInput() {
    return this.keyDataInput.getAttribute('value');
  }

  async setKeyTypeInput(keyType) {
    await this.keyTypeInput.sendKeys(keyType);
  }

  async getKeyTypeInput() {
    return this.keyTypeInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setKeyDataInput('keyData');
    expect(await this.getKeyDataInput()).to.match(/keyData/);
    await waitUntilDisplayed(this.saveButton);
    await this.setKeyTypeInput('keyType');
    expect(await this.getKeyTypeInput()).to.match(/keyType/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactKeysUpdatePage from './contact-keys-update.page-object';

const expect = chai.expect;
export class ContactKeysDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactKeys.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactKeys'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactKeysComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-keys-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-keys');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactKeys() {
    await this.createButton.click();
    return new ContactKeysUpdatePage();
  }

  async deleteContactKeys() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactKeysDeleteDialog = new ContactKeysDeleteDialog();
    await waitUntilDisplayed(contactKeysDeleteDialog.deleteModal);
    expect(await contactKeysDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/magiktapApp.contactKeys.delete.question/);
    await contactKeysDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactKeysDeleteDialog.deleteModal);

    expect(await isVisible(contactKeysDeleteDialog.deleteModal)).to.be.false;
  }
}

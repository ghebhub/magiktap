import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactAgentUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactAgent.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  uriInput: ElementFinder = element(by.css('input#contact-agent-uri'));
  contactSelect: ElementFinder = element(by.css('select#contact-agent-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setUriInput(uri) {
    await this.uriInput.sendKeys(uri);
  }

  async getUriInput() {
    return this.uriInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setUriInput('uri');
    expect(await this.getUriInput()).to.match(/uri/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

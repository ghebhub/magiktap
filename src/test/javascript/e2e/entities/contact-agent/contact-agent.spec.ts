import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactAgentComponentsPage from './contact-agent.page-object';
import ContactAgentUpdatePage from './contact-agent-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactAgent e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactAgentComponentsPage: ContactAgentComponentsPage;
  let contactAgentUpdatePage: ContactAgentUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactAgentComponentsPage = new ContactAgentComponentsPage();
    contactAgentComponentsPage = await contactAgentComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactAgents', async () => {
    expect(await contactAgentComponentsPage.title.getText()).to.match(/Contact Agents/);
    expect(await contactAgentComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactAgents', async () => {
    const beforeRecordsCount = (await isVisible(contactAgentComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactAgentComponentsPage.table);
    contactAgentUpdatePage = await contactAgentComponentsPage.goToCreateContactAgent();
    await contactAgentUpdatePage.enterData();

    expect(await contactAgentComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactAgentComponentsPage.table);
    await waitUntilCount(contactAgentComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactAgentComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactAgentComponentsPage.deleteContactAgent();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactAgentComponentsPage.records, beforeRecordsCount);
      expect(await contactAgentComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactAgentComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

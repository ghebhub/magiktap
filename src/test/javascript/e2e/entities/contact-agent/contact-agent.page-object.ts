import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactAgentUpdatePage from './contact-agent-update.page-object';

const expect = chai.expect;
export class ContactAgentDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactAgent.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactAgent'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactAgentComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-agent-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-agent');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactAgent() {
    await this.createButton.click();
    return new ContactAgentUpdatePage();
  }

  async deleteContactAgent() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactAgentDeleteDialog = new ContactAgentDeleteDialog();
    await waitUntilDisplayed(contactAgentDeleteDialog.deleteModal);
    expect(await contactAgentDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/magiktapApp.contactAgent.delete.question/);
    await contactAgentDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactAgentDeleteDialog.deleteModal);

    expect(await isVisible(contactAgentDeleteDialog.deleteModal)).to.be.false;
  }
}

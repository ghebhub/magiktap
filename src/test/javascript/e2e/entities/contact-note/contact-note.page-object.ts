import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactNoteUpdatePage from './contact-note-update.page-object';

const expect = chai.expect;
export class ContactNoteDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactNote.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactNote'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactNoteComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-note-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-note');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactNote() {
    await this.createButton.click();
    return new ContactNoteUpdatePage();
  }

  async deleteContactNote() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactNoteDeleteDialog = new ContactNoteDeleteDialog();
    await waitUntilDisplayed(contactNoteDeleteDialog.deleteModal);
    expect(await contactNoteDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/magiktapApp.contactNote.delete.question/);
    await contactNoteDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactNoteDeleteDialog.deleteModal);

    expect(await isVisible(contactNoteDeleteDialog.deleteModal)).to.be.false;
  }
}

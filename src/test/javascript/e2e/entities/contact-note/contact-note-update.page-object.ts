import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactNoteUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactNote.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  noteInput: ElementFinder = element(by.css('input#contact-note-note'));
  contactNoteTypeInput: ElementFinder = element(by.css('input#contact-note-contactNoteType'));
  contactSelect: ElementFinder = element(by.css('select#contact-note-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setNoteInput(note) {
    await this.noteInput.sendKeys(note);
  }

  async getNoteInput() {
    return this.noteInput.getAttribute('value');
  }

  async setContactNoteTypeInput(contactNoteType) {
    await this.contactNoteTypeInput.sendKeys(contactNoteType);
  }

  async getContactNoteTypeInput() {
    return this.contactNoteTypeInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setNoteInput('note');
    expect(await this.getNoteInput()).to.match(/note/);
    await waitUntilDisplayed(this.saveButton);
    await this.setContactNoteTypeInput('contactNoteType');
    expect(await this.getContactNoteTypeInput()).to.match(/contactNoteType/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

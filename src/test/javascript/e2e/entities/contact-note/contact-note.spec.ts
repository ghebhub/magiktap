import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactNoteComponentsPage from './contact-note.page-object';
import ContactNoteUpdatePage from './contact-note-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactNote e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactNoteComponentsPage: ContactNoteComponentsPage;
  let contactNoteUpdatePage: ContactNoteUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactNoteComponentsPage = new ContactNoteComponentsPage();
    contactNoteComponentsPage = await contactNoteComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactNotes', async () => {
    expect(await contactNoteComponentsPage.title.getText()).to.match(/Contact Notes/);
    expect(await contactNoteComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactNotes', async () => {
    const beforeRecordsCount = (await isVisible(contactNoteComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactNoteComponentsPage.table);
    contactNoteUpdatePage = await contactNoteComponentsPage.goToCreateContactNote();
    await contactNoteUpdatePage.enterData();

    expect(await contactNoteComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactNoteComponentsPage.table);
    await waitUntilCount(contactNoteComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactNoteComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactNoteComponentsPage.deleteContactNote();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactNoteComponentsPage.records, beforeRecordsCount);
      expect(await contactNoteComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactNoteComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

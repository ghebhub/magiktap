import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactXtendedUpdatePage from './contact-xtended-update.page-object';

const expect = chai.expect;
export class ContactXtendedDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactXtended.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactXtended'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactXtendedComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-xtended-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-xtended');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactXtended() {
    await this.createButton.click();
    return new ContactXtendedUpdatePage();
  }

  async deleteContactXtended() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactXtendedDeleteDialog = new ContactXtendedDeleteDialog();
    await waitUntilDisplayed(contactXtendedDeleteDialog.deleteModal);
    expect(await contactXtendedDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/magiktapApp.contactXtended.delete.question/);
    await contactXtendedDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactXtendedDeleteDialog.deleteModal);

    expect(await isVisible(contactXtendedDeleteDialog.deleteModal)).to.be.false;
  }
}

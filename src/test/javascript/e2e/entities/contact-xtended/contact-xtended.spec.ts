import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactXtendedComponentsPage from './contact-xtended.page-object';
import ContactXtendedUpdatePage from './contact-xtended-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactXtended e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactXtendedComponentsPage: ContactXtendedComponentsPage;
  let contactXtendedUpdatePage: ContactXtendedUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactXtendedComponentsPage = new ContactXtendedComponentsPage();
    contactXtendedComponentsPage = await contactXtendedComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactXtendeds', async () => {
    expect(await contactXtendedComponentsPage.title.getText()).to.match(/Contact Xtendeds/);
    expect(await contactXtendedComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactXtendeds', async () => {
    const beforeRecordsCount = (await isVisible(contactXtendedComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactXtendedComponentsPage.table);
    contactXtendedUpdatePage = await contactXtendedComponentsPage.goToCreateContactXtended();
    await contactXtendedUpdatePage.enterData();

    expect(await contactXtendedComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactXtendedComponentsPage.table);
    await waitUntilCount(contactXtendedComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactXtendedComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactXtendedComponentsPage.deleteContactXtended();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactXtendedComponentsPage.records, beforeRecordsCount);
      expect(await contactXtendedComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactXtendedComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

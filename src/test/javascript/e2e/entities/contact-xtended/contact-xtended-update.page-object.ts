import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactXtendedUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactXtended.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  xnameInput: ElementFinder = element(by.css('input#contact-xtended-xname'));
  xvalueInput: ElementFinder = element(by.css('input#contact-xtended-xvalue'));
  contactSelect: ElementFinder = element(by.css('select#contact-xtended-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setXnameInput(xname) {
    await this.xnameInput.sendKeys(xname);
  }

  async getXnameInput() {
    return this.xnameInput.getAttribute('value');
  }

  async setXvalueInput(xvalue) {
    await this.xvalueInput.sendKeys(xvalue);
  }

  async getXvalueInput() {
    return this.xvalueInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setXnameInput('xname');
    expect(await this.getXnameInput()).to.match(/xname/);
    await waitUntilDisplayed(this.saveButton);
    await this.setXvalueInput('xvalue');
    expect(await this.getXvalueInput()).to.match(/xvalue/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

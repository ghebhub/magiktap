import { element, by, ElementFinder, protractor } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contact.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  contactIdInput: ElementFinder = element(by.css('input#contact-contactId'));
  fnInput: ElementFinder = element(by.css('input#contact-fn'));
  nInput: ElementFinder = element(by.css('input#contact-n'));
  nicknameInput: ElementFinder = element(by.css('input#contact-nickname'));
  bdayInput: ElementFinder = element(by.css('input#contact-bday'));
  mailerInput: ElementFinder = element(by.css('input#contact-mailer'));
  tzInput: ElementFinder = element(by.css('input#contact-tz'));
  geoLatInput: ElementFinder = element(by.css('input#contact-geoLat'));
  geoLongInput: ElementFinder = element(by.css('input#contact-geoLong'));
  titleInput: ElementFinder = element(by.css('input#contact-title'));
  roleInput: ElementFinder = element(by.css('input#contact-role'));
  prodIdInput: ElementFinder = element(by.css('input#contact-prodId'));
  revInput: ElementFinder = element(by.css('input#contact-rev'));
  sortStringInput: ElementFinder = element(by.css('input#contact-sortString'));
  uidInput: ElementFinder = element(by.css('input#contact-uid'));
  urlInput: ElementFinder = element(by.css('input#contact-url'));
  versionInput: ElementFinder = element(by.css('input#contact-version'));
  classNameInput: ElementFinder = element(by.css('input#contact-className'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setContactIdInput(contactId) {
    await this.contactIdInput.sendKeys(contactId);
  }

  async getContactIdInput() {
    return this.contactIdInput.getAttribute('value');
  }

  async setFnInput(fn) {
    await this.fnInput.sendKeys(fn);
  }

  async getFnInput() {
    return this.fnInput.getAttribute('value');
  }

  async setNInput(n) {
    await this.nInput.sendKeys(n);
  }

  async getNInput() {
    return this.nInput.getAttribute('value');
  }

  async setNicknameInput(nickname) {
    await this.nicknameInput.sendKeys(nickname);
  }

  async getNicknameInput() {
    return this.nicknameInput.getAttribute('value');
  }

  async setBdayInput(bday) {
    await this.bdayInput.sendKeys(bday);
  }

  async getBdayInput() {
    return this.bdayInput.getAttribute('value');
  }

  async setMailerInput(mailer) {
    await this.mailerInput.sendKeys(mailer);
  }

  async getMailerInput() {
    return this.mailerInput.getAttribute('value');
  }

  async setTzInput(tz) {
    await this.tzInput.sendKeys(tz);
  }

  async getTzInput() {
    return this.tzInput.getAttribute('value');
  }

  async setGeoLatInput(geoLat) {
    await this.geoLatInput.sendKeys(geoLat);
  }

  async getGeoLatInput() {
    return this.geoLatInput.getAttribute('value');
  }

  async setGeoLongInput(geoLong) {
    await this.geoLongInput.sendKeys(geoLong);
  }

  async getGeoLongInput() {
    return this.geoLongInput.getAttribute('value');
  }

  async setTitleInput(title) {
    await this.titleInput.sendKeys(title);
  }

  async getTitleInput() {
    return this.titleInput.getAttribute('value');
  }

  async setRoleInput(role) {
    await this.roleInput.sendKeys(role);
  }

  async getRoleInput() {
    return this.roleInput.getAttribute('value');
  }

  async setProdIdInput(prodId) {
    await this.prodIdInput.sendKeys(prodId);
  }

  async getProdIdInput() {
    return this.prodIdInput.getAttribute('value');
  }

  async setRevInput(rev) {
    await this.revInput.sendKeys(rev);
  }

  async getRevInput() {
    return this.revInput.getAttribute('value');
  }

  async setSortStringInput(sortString) {
    await this.sortStringInput.sendKeys(sortString);
  }

  async getSortStringInput() {
    return this.sortStringInput.getAttribute('value');
  }

  async setUidInput(uid) {
    await this.uidInput.sendKeys(uid);
  }

  async getUidInput() {
    return this.uidInput.getAttribute('value');
  }

  async setUrlInput(url) {
    await this.urlInput.sendKeys(url);
  }

  async getUrlInput() {
    return this.urlInput.getAttribute('value');
  }

  async setVersionInput(version) {
    await this.versionInput.sendKeys(version);
  }

  async getVersionInput() {
    return this.versionInput.getAttribute('value');
  }

  async setClassNameInput(className) {
    await this.classNameInput.sendKeys(className);
  }

  async getClassNameInput() {
    return this.classNameInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setContactIdInput('contactId');
    expect(await this.getContactIdInput()).to.match(/contactId/);
    await waitUntilDisplayed(this.saveButton);
    await this.setFnInput('fn');
    expect(await this.getFnInput()).to.match(/fn/);
    await waitUntilDisplayed(this.saveButton);
    await this.setNInput('n');
    expect(await this.getNInput()).to.match(/n/);
    await waitUntilDisplayed(this.saveButton);
    await this.setNicknameInput('nickname');
    expect(await this.getNicknameInput()).to.match(/nickname/);
    await waitUntilDisplayed(this.saveButton);
    await this.setBdayInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
    expect(await this.getBdayInput()).to.contain('2001-01-01T02:30');
    await waitUntilDisplayed(this.saveButton);
    await this.setMailerInput('mailer');
    expect(await this.getMailerInput()).to.match(/mailer/);
    await waitUntilDisplayed(this.saveButton);
    await this.setTzInput('5');
    expect(await this.getTzInput()).to.eq('5');
    await waitUntilDisplayed(this.saveButton);
    await this.setGeoLatInput('5');
    expect(await this.getGeoLatInput()).to.eq('5');
    await waitUntilDisplayed(this.saveButton);
    await this.setGeoLongInput('5');
    expect(await this.getGeoLongInput()).to.eq('5');
    await waitUntilDisplayed(this.saveButton);
    await this.setTitleInput('title');
    expect(await this.getTitleInput()).to.match(/title/);
    await waitUntilDisplayed(this.saveButton);
    await this.setRoleInput('role');
    expect(await this.getRoleInput()).to.match(/role/);
    await waitUntilDisplayed(this.saveButton);
    await this.setProdIdInput('prodId');
    expect(await this.getProdIdInput()).to.match(/prodId/);
    await waitUntilDisplayed(this.saveButton);
    await this.setRevInput('rev');
    expect(await this.getRevInput()).to.match(/rev/);
    await waitUntilDisplayed(this.saveButton);
    await this.setSortStringInput('sortString');
    expect(await this.getSortStringInput()).to.match(/sortString/);
    await waitUntilDisplayed(this.saveButton);
    await this.setUidInput('uid');
    expect(await this.getUidInput()).to.match(/uid/);
    await waitUntilDisplayed(this.saveButton);
    await this.setUrlInput('url');
    expect(await this.getUrlInput()).to.match(/url/);
    await waitUntilDisplayed(this.saveButton);
    await this.setVersionInput('version');
    expect(await this.getVersionInput()).to.match(/version/);
    await waitUntilDisplayed(this.saveButton);
    await this.setClassNameInput('className');
    expect(await this.getClassNameInput()).to.match(/className/);
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

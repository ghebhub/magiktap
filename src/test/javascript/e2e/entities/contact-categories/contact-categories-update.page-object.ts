import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactCategoriesUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactCategories.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  categoryNameInput: ElementFinder = element(by.css('input#contact-categories-categoryName'));
  contactSelect: ElementFinder = element(by.css('select#contact-categories-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setCategoryNameInput(categoryName) {
    await this.categoryNameInput.sendKeys(categoryName);
  }

  async getCategoryNameInput() {
    return this.categoryNameInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setCategoryNameInput('categoryName');
    expect(await this.getCategoryNameInput()).to.match(/categoryName/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

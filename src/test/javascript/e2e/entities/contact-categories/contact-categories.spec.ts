import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactCategoriesComponentsPage from './contact-categories.page-object';
import ContactCategoriesUpdatePage from './contact-categories-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactCategories e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactCategoriesComponentsPage: ContactCategoriesComponentsPage;
  let contactCategoriesUpdatePage: ContactCategoriesUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactCategoriesComponentsPage = new ContactCategoriesComponentsPage();
    contactCategoriesComponentsPage = await contactCategoriesComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactCategories', async () => {
    expect(await contactCategoriesComponentsPage.title.getText()).to.match(/Contact Categories/);
    expect(await contactCategoriesComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactCategories', async () => {
    const beforeRecordsCount = (await isVisible(contactCategoriesComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactCategoriesComponentsPage.table);
    contactCategoriesUpdatePage = await contactCategoriesComponentsPage.goToCreateContactCategories();
    await contactCategoriesUpdatePage.enterData();

    expect(await contactCategoriesComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactCategoriesComponentsPage.table);
    await waitUntilCount(contactCategoriesComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactCategoriesComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactCategoriesComponentsPage.deleteContactCategories();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactCategoriesComponentsPage.records, beforeRecordsCount);
      expect(await contactCategoriesComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactCategoriesComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

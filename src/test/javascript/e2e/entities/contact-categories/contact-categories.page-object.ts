import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactCategoriesUpdatePage from './contact-categories-update.page-object';

const expect = chai.expect;
export class ContactCategoriesDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactCategories.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactCategories'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactCategoriesComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-categories-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-categories');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactCategories() {
    await this.createButton.click();
    return new ContactCategoriesUpdatePage();
  }

  async deleteContactCategories() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactCategoriesDeleteDialog = new ContactCategoriesDeleteDialog();
    await waitUntilDisplayed(contactCategoriesDeleteDialog.deleteModal);
    expect(await contactCategoriesDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /magiktapApp.contactCategories.delete.question/
    );
    await contactCategoriesDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactCategoriesDeleteDialog.deleteModal);

    expect(await isVisible(contactCategoriesDeleteDialog.deleteModal)).to.be.false;
  }
}

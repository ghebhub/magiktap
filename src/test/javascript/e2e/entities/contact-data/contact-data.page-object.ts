import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactDataUpdatePage from './contact-data-update.page-object';

const expect = chai.expect;
export class ContactDataDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactData.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactData'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactDataComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-data-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-data');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactData() {
    await this.createButton.click();
    return new ContactDataUpdatePage();
  }

  async deleteContactData() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactDataDeleteDialog = new ContactDataDeleteDialog();
    await waitUntilDisplayed(contactDataDeleteDialog.deleteModal);
    expect(await contactDataDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/magiktapApp.contactData.delete.question/);
    await contactDataDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactDataDeleteDialog.deleteModal);

    expect(await isVisible(contactDataDeleteDialog.deleteModal)).to.be.false;
  }
}

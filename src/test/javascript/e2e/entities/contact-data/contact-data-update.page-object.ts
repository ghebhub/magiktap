import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

import path from 'path';

const expect = chai.expect;

const fileToUpload = '../../../../../../src/main/webapp/content/images/logo-jhipster.png';
const absolutePath = path.resolve(__dirname, fileToUpload);
export default class ContactDataUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactData.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  dataNameInput: ElementFinder = element(by.css('input#contact-data-dataName'));
  urlInput: ElementFinder = element(by.css('input#contact-data-url'));
  inlineInput: ElementFinder = element(by.css('input#contact-data-inline'));
  contactTypesInput: ElementFinder = element(by.css('input#contact-data-contactTypes'));
  contactEncodingTypesInput: ElementFinder = element(by.css('input#contact-data-contactEncodingTypes'));
  contactDataInput: ElementFinder = element(by.css('input#file_contactData'));
  contactSelect: ElementFinder = element(by.css('select#contact-data-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setDataNameInput(dataName) {
    await this.dataNameInput.sendKeys(dataName);
  }

  async getDataNameInput() {
    return this.dataNameInput.getAttribute('value');
  }

  async setUrlInput(url) {
    await this.urlInput.sendKeys(url);
  }

  async getUrlInput() {
    return this.urlInput.getAttribute('value');
  }

  async setInlineInput(inline) {
    await this.inlineInput.sendKeys(inline);
  }

  async getInlineInput() {
    return this.inlineInput.getAttribute('value');
  }

  async setContactTypesInput(contactTypes) {
    await this.contactTypesInput.sendKeys(contactTypes);
  }

  async getContactTypesInput() {
    return this.contactTypesInput.getAttribute('value');
  }

  async setContactEncodingTypesInput(contactEncodingTypes) {
    await this.contactEncodingTypesInput.sendKeys(contactEncodingTypes);
  }

  async getContactEncodingTypesInput() {
    return this.contactEncodingTypesInput.getAttribute('value');
  }

  async setContactDataInput(contactData) {
    await this.contactDataInput.sendKeys(contactData);
  }

  async getContactDataInput() {
    return this.contactDataInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setDataNameInput('dataName');
    expect(await this.getDataNameInput()).to.match(/dataName/);
    await waitUntilDisplayed(this.saveButton);
    await this.setUrlInput('url');
    expect(await this.getUrlInput()).to.match(/url/);
    await waitUntilDisplayed(this.saveButton);
    await this.setInlineInput('inline');
    expect(await this.getInlineInput()).to.match(/inline/);
    await waitUntilDisplayed(this.saveButton);
    await this.setContactTypesInput('contactTypes');
    expect(await this.getContactTypesInput()).to.match(/contactTypes/);
    await waitUntilDisplayed(this.saveButton);
    await this.setContactEncodingTypesInput('contactEncodingTypes');
    expect(await this.getContactEncodingTypesInput()).to.match(/contactEncodingTypes/);
    await waitUntilDisplayed(this.saveButton);
    await this.setContactDataInput(absolutePath);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

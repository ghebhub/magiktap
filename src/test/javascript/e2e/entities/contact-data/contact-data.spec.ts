import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactDataComponentsPage from './contact-data.page-object';
import ContactDataUpdatePage from './contact-data-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';
import path from 'path';

const expect = chai.expect;

describe('ContactData e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactDataComponentsPage: ContactDataComponentsPage;
  let contactDataUpdatePage: ContactDataUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactDataComponentsPage = new ContactDataComponentsPage();
    contactDataComponentsPage = await contactDataComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactData', async () => {
    expect(await contactDataComponentsPage.title.getText()).to.match(/Contact Data/);
    expect(await contactDataComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactData', async () => {
    const beforeRecordsCount = (await isVisible(contactDataComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactDataComponentsPage.table);
    contactDataUpdatePage = await contactDataComponentsPage.goToCreateContactData();
    await contactDataUpdatePage.enterData();

    expect(await contactDataComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactDataComponentsPage.table);
    await waitUntilCount(contactDataComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactDataComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactDataComponentsPage.deleteContactData();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactDataComponentsPage.records, beforeRecordsCount);
      expect(await contactDataComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactDataComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import ContactMailAddressUpdatePage from './contact-mail-address-update.page-object';

const expect = chai.expect;
export class ContactMailAddressDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('magiktapApp.contactMailAddress.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-contactMailAddress'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class ContactMailAddressComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('contact-mail-address-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('contact-mail-address');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateContactMailAddress() {
    await this.createButton.click();
    return new ContactMailAddressUpdatePage();
  }

  async deleteContactMailAddress() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const contactMailAddressDeleteDialog = new ContactMailAddressDeleteDialog();
    await waitUntilDisplayed(contactMailAddressDeleteDialog.deleteModal);
    expect(await contactMailAddressDeleteDialog.getDialogTitle().getAttribute('id')).to.match(
      /magiktapApp.contactMailAddress.delete.question/
    );
    await contactMailAddressDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(contactMailAddressDeleteDialog.deleteModal);

    expect(await isVisible(contactMailAddressDeleteDialog.deleteModal)).to.be.false;
  }
}

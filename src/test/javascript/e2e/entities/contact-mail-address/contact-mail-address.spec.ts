import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import ContactMailAddressComponentsPage from './contact-mail-address.page-object';
import ContactMailAddressUpdatePage from './contact-mail-address-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('ContactMailAddress e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let contactMailAddressComponentsPage: ContactMailAddressComponentsPage;
  let contactMailAddressUpdatePage: ContactMailAddressUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    contactMailAddressComponentsPage = new ContactMailAddressComponentsPage();
    contactMailAddressComponentsPage = await contactMailAddressComponentsPage.goToPage(navBarPage);
  });

  it('should load ContactMailAddresses', async () => {
    expect(await contactMailAddressComponentsPage.title.getText()).to.match(/Contact Mail Addresses/);
    expect(await contactMailAddressComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete ContactMailAddresses', async () => {
    const beforeRecordsCount = (await isVisible(contactMailAddressComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(contactMailAddressComponentsPage.table);
    contactMailAddressUpdatePage = await contactMailAddressComponentsPage.goToCreateContactMailAddress();
    await contactMailAddressUpdatePage.enterData();

    expect(await contactMailAddressComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(contactMailAddressComponentsPage.table);
    await waitUntilCount(contactMailAddressComponentsPage.records, beforeRecordsCount + 1);
    expect(await contactMailAddressComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await contactMailAddressComponentsPage.deleteContactMailAddress();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(contactMailAddressComponentsPage.records, beforeRecordsCount);
      expect(await contactMailAddressComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(contactMailAddressComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

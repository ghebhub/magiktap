import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class ContactMailAddressUpdatePage {
  pageTitle: ElementFinder = element(by.id('magiktapApp.contactMailAddress.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  poboxInput: ElementFinder = element(by.css('input#contact-mail-address-pobox'));
  extendedAddressInput: ElementFinder = element(by.css('input#contact-mail-address-extendedAddress'));
  streetInput: ElementFinder = element(by.css('input#contact-mail-address-street'));
  localityInput: ElementFinder = element(by.css('input#contact-mail-address-locality'));
  regionInput: ElementFinder = element(by.css('input#contact-mail-address-region'));
  postalCodeInput: ElementFinder = element(by.css('input#contact-mail-address-postalCode'));
  countryInput: ElementFinder = element(by.css('input#contact-mail-address-country'));
  mailAddressTypeInput: ElementFinder = element(by.css('input#contact-mail-address-mailAddressType'));
  contactSelect: ElementFinder = element(by.css('select#contact-mail-address-contact'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setPoboxInput(pobox) {
    await this.poboxInput.sendKeys(pobox);
  }

  async getPoboxInput() {
    return this.poboxInput.getAttribute('value');
  }

  async setExtendedAddressInput(extendedAddress) {
    await this.extendedAddressInput.sendKeys(extendedAddress);
  }

  async getExtendedAddressInput() {
    return this.extendedAddressInput.getAttribute('value');
  }

  async setStreetInput(street) {
    await this.streetInput.sendKeys(street);
  }

  async getStreetInput() {
    return this.streetInput.getAttribute('value');
  }

  async setLocalityInput(locality) {
    await this.localityInput.sendKeys(locality);
  }

  async getLocalityInput() {
    return this.localityInput.getAttribute('value');
  }

  async setRegionInput(region) {
    await this.regionInput.sendKeys(region);
  }

  async getRegionInput() {
    return this.regionInput.getAttribute('value');
  }

  async setPostalCodeInput(postalCode) {
    await this.postalCodeInput.sendKeys(postalCode);
  }

  async getPostalCodeInput() {
    return this.postalCodeInput.getAttribute('value');
  }

  async setCountryInput(country) {
    await this.countryInput.sendKeys(country);
  }

  async getCountryInput() {
    return this.countryInput.getAttribute('value');
  }

  async setMailAddressTypeInput(mailAddressType) {
    await this.mailAddressTypeInput.sendKeys(mailAddressType);
  }

  async getMailAddressTypeInput() {
    return this.mailAddressTypeInput.getAttribute('value');
  }

  async contactSelectLastOption() {
    await this.contactSelect.all(by.tagName('option')).last().click();
  }

  async contactSelectOption(option) {
    await this.contactSelect.sendKeys(option);
  }

  getContactSelect() {
    return this.contactSelect;
  }

  async getContactSelectedOption() {
    return this.contactSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setPoboxInput('pobox');
    expect(await this.getPoboxInput()).to.match(/pobox/);
    await waitUntilDisplayed(this.saveButton);
    await this.setExtendedAddressInput('extendedAddress');
    expect(await this.getExtendedAddressInput()).to.match(/extendedAddress/);
    await waitUntilDisplayed(this.saveButton);
    await this.setStreetInput('street');
    expect(await this.getStreetInput()).to.match(/street/);
    await waitUntilDisplayed(this.saveButton);
    await this.setLocalityInput('locality');
    expect(await this.getLocalityInput()).to.match(/locality/);
    await waitUntilDisplayed(this.saveButton);
    await this.setRegionInput('region');
    expect(await this.getRegionInput()).to.match(/region/);
    await waitUntilDisplayed(this.saveButton);
    await this.setPostalCodeInput('postalCode');
    expect(await this.getPostalCodeInput()).to.match(/postalCode/);
    await waitUntilDisplayed(this.saveButton);
    await this.setCountryInput('country');
    expect(await this.getCountryInput()).to.match(/country/);
    await waitUntilDisplayed(this.saveButton);
    await this.setMailAddressTypeInput('mailAddressType');
    expect(await this.getMailAddressTypeInput()).to.match(/mailAddressType/);
    await this.contactSelectLastOption();
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}

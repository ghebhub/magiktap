package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactKeys;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactKeysRepository;
import com.incipitweb.magiktap.service.ContactKeysService;
import com.incipitweb.magiktap.service.dto.ContactKeysDTO;
import com.incipitweb.magiktap.service.mapper.ContactKeysMapper;
import com.incipitweb.magiktap.service.dto.ContactKeysCriteria;
import com.incipitweb.magiktap.service.ContactKeysQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactKeysResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactKeysResourceIT {

    private static final String DEFAULT_KEY_DATA = "AAAAAAAAAA";
    private static final String UPDATED_KEY_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_KEY_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_KEY_TYPE = "BBBBBBBBBB";

    @Autowired
    private ContactKeysRepository contactKeysRepository;

    @Autowired
    private ContactKeysMapper contactKeysMapper;

    @Autowired
    private ContactKeysService contactKeysService;

    @Autowired
    private ContactKeysQueryService contactKeysQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactKeysMockMvc;

    private ContactKeys contactKeys;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactKeys createEntity(EntityManager em) {
        ContactKeys contactKeys = new ContactKeys()
            .keyData(DEFAULT_KEY_DATA)
            .keyType(DEFAULT_KEY_TYPE);
        return contactKeys;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactKeys createUpdatedEntity(EntityManager em) {
        ContactKeys contactKeys = new ContactKeys()
            .keyData(UPDATED_KEY_DATA)
            .keyType(UPDATED_KEY_TYPE);
        return contactKeys;
    }

    @BeforeEach
    public void initTest() {
        contactKeys = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactKeys() throws Exception {
        int databaseSizeBeforeCreate = contactKeysRepository.findAll().size();
        // Create the ContactKeys
        ContactKeysDTO contactKeysDTO = contactKeysMapper.toDto(contactKeys);
        restContactKeysMockMvc.perform(post("/api/contact-keys")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactKeysDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactKeys in the database
        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeCreate + 1);
        ContactKeys testContactKeys = contactKeysList.get(contactKeysList.size() - 1);
        assertThat(testContactKeys.getKeyData()).isEqualTo(DEFAULT_KEY_DATA);
        assertThat(testContactKeys.getKeyType()).isEqualTo(DEFAULT_KEY_TYPE);
    }

    @Test
    @Transactional
    public void createContactKeysWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactKeysRepository.findAll().size();

        // Create the ContactKeys with an existing ID
        contactKeys.setId(1L);
        ContactKeysDTO contactKeysDTO = contactKeysMapper.toDto(contactKeys);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactKeysMockMvc.perform(post("/api/contact-keys")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactKeysDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactKeys in the database
        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkKeyDataIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactKeysRepository.findAll().size();
        // set the field null
        contactKeys.setKeyData(null);

        // Create the ContactKeys, which fails.
        ContactKeysDTO contactKeysDTO = contactKeysMapper.toDto(contactKeys);


        restContactKeysMockMvc.perform(post("/api/contact-keys")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactKeysDTO)))
            .andExpect(status().isBadRequest());

        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactKeys() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList
        restContactKeysMockMvc.perform(get("/api/contact-keys?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactKeys.getId().intValue())))
            .andExpect(jsonPath("$.[*].keyData").value(hasItem(DEFAULT_KEY_DATA)))
            .andExpect(jsonPath("$.[*].keyType").value(hasItem(DEFAULT_KEY_TYPE)));
    }
    
    @Test
    @Transactional
    public void getContactKeys() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get the contactKeys
        restContactKeysMockMvc.perform(get("/api/contact-keys/{id}", contactKeys.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactKeys.getId().intValue()))
            .andExpect(jsonPath("$.keyData").value(DEFAULT_KEY_DATA))
            .andExpect(jsonPath("$.keyType").value(DEFAULT_KEY_TYPE));
    }


    @Test
    @Transactional
    public void getContactKeysByIdFiltering() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        Long id = contactKeys.getId();

        defaultContactKeysShouldBeFound("id.equals=" + id);
        defaultContactKeysShouldNotBeFound("id.notEquals=" + id);

        defaultContactKeysShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactKeysShouldNotBeFound("id.greaterThan=" + id);

        defaultContactKeysShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactKeysShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactKeysByKeyDataIsEqualToSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData equals to DEFAULT_KEY_DATA
        defaultContactKeysShouldBeFound("keyData.equals=" + DEFAULT_KEY_DATA);

        // Get all the contactKeysList where keyData equals to UPDATED_KEY_DATA
        defaultContactKeysShouldNotBeFound("keyData.equals=" + UPDATED_KEY_DATA);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyDataIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData not equals to DEFAULT_KEY_DATA
        defaultContactKeysShouldNotBeFound("keyData.notEquals=" + DEFAULT_KEY_DATA);

        // Get all the contactKeysList where keyData not equals to UPDATED_KEY_DATA
        defaultContactKeysShouldBeFound("keyData.notEquals=" + UPDATED_KEY_DATA);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyDataIsInShouldWork() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData in DEFAULT_KEY_DATA or UPDATED_KEY_DATA
        defaultContactKeysShouldBeFound("keyData.in=" + DEFAULT_KEY_DATA + "," + UPDATED_KEY_DATA);

        // Get all the contactKeysList where keyData equals to UPDATED_KEY_DATA
        defaultContactKeysShouldNotBeFound("keyData.in=" + UPDATED_KEY_DATA);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyDataIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData is not null
        defaultContactKeysShouldBeFound("keyData.specified=true");

        // Get all the contactKeysList where keyData is null
        defaultContactKeysShouldNotBeFound("keyData.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactKeysByKeyDataContainsSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData contains DEFAULT_KEY_DATA
        defaultContactKeysShouldBeFound("keyData.contains=" + DEFAULT_KEY_DATA);

        // Get all the contactKeysList where keyData contains UPDATED_KEY_DATA
        defaultContactKeysShouldNotBeFound("keyData.contains=" + UPDATED_KEY_DATA);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyDataNotContainsSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyData does not contain DEFAULT_KEY_DATA
        defaultContactKeysShouldNotBeFound("keyData.doesNotContain=" + DEFAULT_KEY_DATA);

        // Get all the contactKeysList where keyData does not contain UPDATED_KEY_DATA
        defaultContactKeysShouldBeFound("keyData.doesNotContain=" + UPDATED_KEY_DATA);
    }


    @Test
    @Transactional
    public void getAllContactKeysByKeyTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType equals to DEFAULT_KEY_TYPE
        defaultContactKeysShouldBeFound("keyType.equals=" + DEFAULT_KEY_TYPE);

        // Get all the contactKeysList where keyType equals to UPDATED_KEY_TYPE
        defaultContactKeysShouldNotBeFound("keyType.equals=" + UPDATED_KEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType not equals to DEFAULT_KEY_TYPE
        defaultContactKeysShouldNotBeFound("keyType.notEquals=" + DEFAULT_KEY_TYPE);

        // Get all the contactKeysList where keyType not equals to UPDATED_KEY_TYPE
        defaultContactKeysShouldBeFound("keyType.notEquals=" + UPDATED_KEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType in DEFAULT_KEY_TYPE or UPDATED_KEY_TYPE
        defaultContactKeysShouldBeFound("keyType.in=" + DEFAULT_KEY_TYPE + "," + UPDATED_KEY_TYPE);

        // Get all the contactKeysList where keyType equals to UPDATED_KEY_TYPE
        defaultContactKeysShouldNotBeFound("keyType.in=" + UPDATED_KEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType is not null
        defaultContactKeysShouldBeFound("keyType.specified=true");

        // Get all the contactKeysList where keyType is null
        defaultContactKeysShouldNotBeFound("keyType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactKeysByKeyTypeContainsSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType contains DEFAULT_KEY_TYPE
        defaultContactKeysShouldBeFound("keyType.contains=" + DEFAULT_KEY_TYPE);

        // Get all the contactKeysList where keyType contains UPDATED_KEY_TYPE
        defaultContactKeysShouldNotBeFound("keyType.contains=" + UPDATED_KEY_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactKeysByKeyTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        // Get all the contactKeysList where keyType does not contain DEFAULT_KEY_TYPE
        defaultContactKeysShouldNotBeFound("keyType.doesNotContain=" + DEFAULT_KEY_TYPE);

        // Get all the contactKeysList where keyType does not contain UPDATED_KEY_TYPE
        defaultContactKeysShouldBeFound("keyType.doesNotContain=" + UPDATED_KEY_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactKeysByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactKeys.setContact(contact);
        contactKeysRepository.saveAndFlush(contactKeys);
        Long contactId = contact.getId();

        // Get all the contactKeysList where contact equals to contactId
        defaultContactKeysShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactKeysList where contact equals to contactId + 1
        defaultContactKeysShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactKeysShouldBeFound(String filter) throws Exception {
        restContactKeysMockMvc.perform(get("/api/contact-keys?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactKeys.getId().intValue())))
            .andExpect(jsonPath("$.[*].keyData").value(hasItem(DEFAULT_KEY_DATA)))
            .andExpect(jsonPath("$.[*].keyType").value(hasItem(DEFAULT_KEY_TYPE)));

        // Check, that the count call also returns 1
        restContactKeysMockMvc.perform(get("/api/contact-keys/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactKeysShouldNotBeFound(String filter) throws Exception {
        restContactKeysMockMvc.perform(get("/api/contact-keys?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactKeysMockMvc.perform(get("/api/contact-keys/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactKeys() throws Exception {
        // Get the contactKeys
        restContactKeysMockMvc.perform(get("/api/contact-keys/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactKeys() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        int databaseSizeBeforeUpdate = contactKeysRepository.findAll().size();

        // Update the contactKeys
        ContactKeys updatedContactKeys = contactKeysRepository.findById(contactKeys.getId()).get();
        // Disconnect from session so that the updates on updatedContactKeys are not directly saved in db
        em.detach(updatedContactKeys);
        updatedContactKeys
            .keyData(UPDATED_KEY_DATA)
            .keyType(UPDATED_KEY_TYPE);
        ContactKeysDTO contactKeysDTO = contactKeysMapper.toDto(updatedContactKeys);

        restContactKeysMockMvc.perform(put("/api/contact-keys")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactKeysDTO)))
            .andExpect(status().isOk());

        // Validate the ContactKeys in the database
        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeUpdate);
        ContactKeys testContactKeys = contactKeysList.get(contactKeysList.size() - 1);
        assertThat(testContactKeys.getKeyData()).isEqualTo(UPDATED_KEY_DATA);
        assertThat(testContactKeys.getKeyType()).isEqualTo(UPDATED_KEY_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactKeys() throws Exception {
        int databaseSizeBeforeUpdate = contactKeysRepository.findAll().size();

        // Create the ContactKeys
        ContactKeysDTO contactKeysDTO = contactKeysMapper.toDto(contactKeys);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactKeysMockMvc.perform(put("/api/contact-keys")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactKeysDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactKeys in the database
        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactKeys() throws Exception {
        // Initialize the database
        contactKeysRepository.saveAndFlush(contactKeys);

        int databaseSizeBeforeDelete = contactKeysRepository.findAll().size();

        // Delete the contactKeys
        restContactKeysMockMvc.perform(delete("/api/contact-keys/{id}", contactKeys.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactKeys> contactKeysList = contactKeysRepository.findAll();
        assertThat(contactKeysList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

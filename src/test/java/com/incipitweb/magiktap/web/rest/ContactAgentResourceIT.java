package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactAgent;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactAgentRepository;
import com.incipitweb.magiktap.service.ContactAgentService;
import com.incipitweb.magiktap.service.dto.ContactAgentDTO;
import com.incipitweb.magiktap.service.mapper.ContactAgentMapper;
import com.incipitweb.magiktap.service.dto.ContactAgentCriteria;
import com.incipitweb.magiktap.service.ContactAgentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactAgentResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactAgentResourceIT {

    private static final String DEFAULT_URI = "AAAAAAAAAA";
    private static final String UPDATED_URI = "BBBBBBBBBB";

    @Autowired
    private ContactAgentRepository contactAgentRepository;

    @Autowired
    private ContactAgentMapper contactAgentMapper;

    @Autowired
    private ContactAgentService contactAgentService;

    @Autowired
    private ContactAgentQueryService contactAgentQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactAgentMockMvc;

    private ContactAgent contactAgent;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactAgent createEntity(EntityManager em) {
        ContactAgent contactAgent = new ContactAgent()
            .uri(DEFAULT_URI);
        return contactAgent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactAgent createUpdatedEntity(EntityManager em) {
        ContactAgent contactAgent = new ContactAgent()
            .uri(UPDATED_URI);
        return contactAgent;
    }

    @BeforeEach
    public void initTest() {
        contactAgent = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactAgent() throws Exception {
        int databaseSizeBeforeCreate = contactAgentRepository.findAll().size();
        // Create the ContactAgent
        ContactAgentDTO contactAgentDTO = contactAgentMapper.toDto(contactAgent);
        restContactAgentMockMvc.perform(post("/api/contact-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactAgentDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactAgent in the database
        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeCreate + 1);
        ContactAgent testContactAgent = contactAgentList.get(contactAgentList.size() - 1);
        assertThat(testContactAgent.getUri()).isEqualTo(DEFAULT_URI);
    }

    @Test
    @Transactional
    public void createContactAgentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactAgentRepository.findAll().size();

        // Create the ContactAgent with an existing ID
        contactAgent.setId(1L);
        ContactAgentDTO contactAgentDTO = contactAgentMapper.toDto(contactAgent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactAgentMockMvc.perform(post("/api/contact-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactAgentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactAgent in the database
        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUriIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactAgentRepository.findAll().size();
        // set the field null
        contactAgent.setUri(null);

        // Create the ContactAgent, which fails.
        ContactAgentDTO contactAgentDTO = contactAgentMapper.toDto(contactAgent);


        restContactAgentMockMvc.perform(post("/api/contact-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactAgentDTO)))
            .andExpect(status().isBadRequest());

        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactAgents() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList
        restContactAgentMockMvc.perform(get("/api/contact-agents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactAgent.getId().intValue())))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)));
    }
    
    @Test
    @Transactional
    public void getContactAgent() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get the contactAgent
        restContactAgentMockMvc.perform(get("/api/contact-agents/{id}", contactAgent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactAgent.getId().intValue()))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI));
    }


    @Test
    @Transactional
    public void getContactAgentsByIdFiltering() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        Long id = contactAgent.getId();

        defaultContactAgentShouldBeFound("id.equals=" + id);
        defaultContactAgentShouldNotBeFound("id.notEquals=" + id);

        defaultContactAgentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactAgentShouldNotBeFound("id.greaterThan=" + id);

        defaultContactAgentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactAgentShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactAgentsByUriIsEqualToSomething() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri equals to DEFAULT_URI
        defaultContactAgentShouldBeFound("uri.equals=" + DEFAULT_URI);

        // Get all the contactAgentList where uri equals to UPDATED_URI
        defaultContactAgentShouldNotBeFound("uri.equals=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllContactAgentsByUriIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri not equals to DEFAULT_URI
        defaultContactAgentShouldNotBeFound("uri.notEquals=" + DEFAULT_URI);

        // Get all the contactAgentList where uri not equals to UPDATED_URI
        defaultContactAgentShouldBeFound("uri.notEquals=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllContactAgentsByUriIsInShouldWork() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri in DEFAULT_URI or UPDATED_URI
        defaultContactAgentShouldBeFound("uri.in=" + DEFAULT_URI + "," + UPDATED_URI);

        // Get all the contactAgentList where uri equals to UPDATED_URI
        defaultContactAgentShouldNotBeFound("uri.in=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllContactAgentsByUriIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri is not null
        defaultContactAgentShouldBeFound("uri.specified=true");

        // Get all the contactAgentList where uri is null
        defaultContactAgentShouldNotBeFound("uri.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactAgentsByUriContainsSomething() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri contains DEFAULT_URI
        defaultContactAgentShouldBeFound("uri.contains=" + DEFAULT_URI);

        // Get all the contactAgentList where uri contains UPDATED_URI
        defaultContactAgentShouldNotBeFound("uri.contains=" + UPDATED_URI);
    }

    @Test
    @Transactional
    public void getAllContactAgentsByUriNotContainsSomething() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        // Get all the contactAgentList where uri does not contain DEFAULT_URI
        defaultContactAgentShouldNotBeFound("uri.doesNotContain=" + DEFAULT_URI);

        // Get all the contactAgentList where uri does not contain UPDATED_URI
        defaultContactAgentShouldBeFound("uri.doesNotContain=" + UPDATED_URI);
    }


    @Test
    @Transactional
    public void getAllContactAgentsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactAgent.setContact(contact);
        contactAgentRepository.saveAndFlush(contactAgent);
        Long contactId = contact.getId();

        // Get all the contactAgentList where contact equals to contactId
        defaultContactAgentShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactAgentList where contact equals to contactId + 1
        defaultContactAgentShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactAgentShouldBeFound(String filter) throws Exception {
        restContactAgentMockMvc.perform(get("/api/contact-agents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactAgent.getId().intValue())))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI)));

        // Check, that the count call also returns 1
        restContactAgentMockMvc.perform(get("/api/contact-agents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactAgentShouldNotBeFound(String filter) throws Exception {
        restContactAgentMockMvc.perform(get("/api/contact-agents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactAgentMockMvc.perform(get("/api/contact-agents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactAgent() throws Exception {
        // Get the contactAgent
        restContactAgentMockMvc.perform(get("/api/contact-agents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactAgent() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        int databaseSizeBeforeUpdate = contactAgentRepository.findAll().size();

        // Update the contactAgent
        ContactAgent updatedContactAgent = contactAgentRepository.findById(contactAgent.getId()).get();
        // Disconnect from session so that the updates on updatedContactAgent are not directly saved in db
        em.detach(updatedContactAgent);
        updatedContactAgent
            .uri(UPDATED_URI);
        ContactAgentDTO contactAgentDTO = contactAgentMapper.toDto(updatedContactAgent);

        restContactAgentMockMvc.perform(put("/api/contact-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactAgentDTO)))
            .andExpect(status().isOk());

        // Validate the ContactAgent in the database
        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeUpdate);
        ContactAgent testContactAgent = contactAgentList.get(contactAgentList.size() - 1);
        assertThat(testContactAgent.getUri()).isEqualTo(UPDATED_URI);
    }

    @Test
    @Transactional
    public void updateNonExistingContactAgent() throws Exception {
        int databaseSizeBeforeUpdate = contactAgentRepository.findAll().size();

        // Create the ContactAgent
        ContactAgentDTO contactAgentDTO = contactAgentMapper.toDto(contactAgent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactAgentMockMvc.perform(put("/api/contact-agents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactAgentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactAgent in the database
        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactAgent() throws Exception {
        // Initialize the database
        contactAgentRepository.saveAndFlush(contactAgent);

        int databaseSizeBeforeDelete = contactAgentRepository.findAll().size();

        // Delete the contactAgent
        restContactAgentMockMvc.perform(delete("/api/contact-agents/{id}", contactAgent.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactAgent> contactAgentList = contactAgentRepository.findAll();
        assertThat(contactAgentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

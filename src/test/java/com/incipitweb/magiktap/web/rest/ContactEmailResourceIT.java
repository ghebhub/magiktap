package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactEmail;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactEmailRepository;
import com.incipitweb.magiktap.service.ContactEmailService;
import com.incipitweb.magiktap.service.dto.ContactEmailDTO;
import com.incipitweb.magiktap.service.mapper.ContactEmailMapper;
import com.incipitweb.magiktap.service.dto.ContactEmailCriteria;
import com.incipitweb.magiktap.service.ContactEmailQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactEmailResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactEmailResourceIT {

    private static final String DEFAULT_EMAIL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL_TYPE = "BBBBBBBBBB";

    @Autowired
    private ContactEmailRepository contactEmailRepository;

    @Autowired
    private ContactEmailMapper contactEmailMapper;

    @Autowired
    private ContactEmailService contactEmailService;

    @Autowired
    private ContactEmailQueryService contactEmailQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactEmailMockMvc;

    private ContactEmail contactEmail;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactEmail createEntity(EntityManager em) {
        ContactEmail contactEmail = new ContactEmail()
            .emailAddress(DEFAULT_EMAIL_ADDRESS)
            .emailType(DEFAULT_EMAIL_TYPE);
        return contactEmail;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactEmail createUpdatedEntity(EntityManager em) {
        ContactEmail contactEmail = new ContactEmail()
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .emailType(UPDATED_EMAIL_TYPE);
        return contactEmail;
    }

    @BeforeEach
    public void initTest() {
        contactEmail = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactEmail() throws Exception {
        int databaseSizeBeforeCreate = contactEmailRepository.findAll().size();
        // Create the ContactEmail
        ContactEmailDTO contactEmailDTO = contactEmailMapper.toDto(contactEmail);
        restContactEmailMockMvc.perform(post("/api/contact-emails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactEmailDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactEmail in the database
        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeCreate + 1);
        ContactEmail testContactEmail = contactEmailList.get(contactEmailList.size() - 1);
        assertThat(testContactEmail.getEmailAddress()).isEqualTo(DEFAULT_EMAIL_ADDRESS);
        assertThat(testContactEmail.getEmailType()).isEqualTo(DEFAULT_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void createContactEmailWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactEmailRepository.findAll().size();

        // Create the ContactEmail with an existing ID
        contactEmail.setId(1L);
        ContactEmailDTO contactEmailDTO = contactEmailMapper.toDto(contactEmail);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactEmailMockMvc.perform(post("/api/contact-emails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactEmailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactEmail in the database
        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEmailAddressIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactEmailRepository.findAll().size();
        // set the field null
        contactEmail.setEmailAddress(null);

        // Create the ContactEmail, which fails.
        ContactEmailDTO contactEmailDTO = contactEmailMapper.toDto(contactEmail);


        restContactEmailMockMvc.perform(post("/api/contact-emails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactEmailDTO)))
            .andExpect(status().isBadRequest());

        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactEmails() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList
        restContactEmailMockMvc.perform(get("/api/contact-emails?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactEmail.getId().intValue())))
            .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS)))
            .andExpect(jsonPath("$.[*].emailType").value(hasItem(DEFAULT_EMAIL_TYPE)));
    }
    
    @Test
    @Transactional
    public void getContactEmail() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get the contactEmail
        restContactEmailMockMvc.perform(get("/api/contact-emails/{id}", contactEmail.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactEmail.getId().intValue()))
            .andExpect(jsonPath("$.emailAddress").value(DEFAULT_EMAIL_ADDRESS))
            .andExpect(jsonPath("$.emailType").value(DEFAULT_EMAIL_TYPE));
    }


    @Test
    @Transactional
    public void getContactEmailsByIdFiltering() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        Long id = contactEmail.getId();

        defaultContactEmailShouldBeFound("id.equals=" + id);
        defaultContactEmailShouldNotBeFound("id.notEquals=" + id);

        defaultContactEmailShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactEmailShouldNotBeFound("id.greaterThan=" + id);

        defaultContactEmailShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactEmailShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress equals to DEFAULT_EMAIL_ADDRESS
        defaultContactEmailShouldBeFound("emailAddress.equals=" + DEFAULT_EMAIL_ADDRESS);

        // Get all the contactEmailList where emailAddress equals to UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldNotBeFound("emailAddress.equals=" + UPDATED_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress not equals to DEFAULT_EMAIL_ADDRESS
        defaultContactEmailShouldNotBeFound("emailAddress.notEquals=" + DEFAULT_EMAIL_ADDRESS);

        // Get all the contactEmailList where emailAddress not equals to UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldBeFound("emailAddress.notEquals=" + UPDATED_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressIsInShouldWork() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress in DEFAULT_EMAIL_ADDRESS or UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldBeFound("emailAddress.in=" + DEFAULT_EMAIL_ADDRESS + "," + UPDATED_EMAIL_ADDRESS);

        // Get all the contactEmailList where emailAddress equals to UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldNotBeFound("emailAddress.in=" + UPDATED_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress is not null
        defaultContactEmailShouldBeFound("emailAddress.specified=true");

        // Get all the contactEmailList where emailAddress is null
        defaultContactEmailShouldNotBeFound("emailAddress.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressContainsSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress contains DEFAULT_EMAIL_ADDRESS
        defaultContactEmailShouldBeFound("emailAddress.contains=" + DEFAULT_EMAIL_ADDRESS);

        // Get all the contactEmailList where emailAddress contains UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldNotBeFound("emailAddress.contains=" + UPDATED_EMAIL_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailAddressNotContainsSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailAddress does not contain DEFAULT_EMAIL_ADDRESS
        defaultContactEmailShouldNotBeFound("emailAddress.doesNotContain=" + DEFAULT_EMAIL_ADDRESS);

        // Get all the contactEmailList where emailAddress does not contain UPDATED_EMAIL_ADDRESS
        defaultContactEmailShouldBeFound("emailAddress.doesNotContain=" + UPDATED_EMAIL_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType equals to DEFAULT_EMAIL_TYPE
        defaultContactEmailShouldBeFound("emailType.equals=" + DEFAULT_EMAIL_TYPE);

        // Get all the contactEmailList where emailType equals to UPDATED_EMAIL_TYPE
        defaultContactEmailShouldNotBeFound("emailType.equals=" + UPDATED_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType not equals to DEFAULT_EMAIL_TYPE
        defaultContactEmailShouldNotBeFound("emailType.notEquals=" + DEFAULT_EMAIL_TYPE);

        // Get all the contactEmailList where emailType not equals to UPDATED_EMAIL_TYPE
        defaultContactEmailShouldBeFound("emailType.notEquals=" + UPDATED_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType in DEFAULT_EMAIL_TYPE or UPDATED_EMAIL_TYPE
        defaultContactEmailShouldBeFound("emailType.in=" + DEFAULT_EMAIL_TYPE + "," + UPDATED_EMAIL_TYPE);

        // Get all the contactEmailList where emailType equals to UPDATED_EMAIL_TYPE
        defaultContactEmailShouldNotBeFound("emailType.in=" + UPDATED_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType is not null
        defaultContactEmailShouldBeFound("emailType.specified=true");

        // Get all the contactEmailList where emailType is null
        defaultContactEmailShouldNotBeFound("emailType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeContainsSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType contains DEFAULT_EMAIL_TYPE
        defaultContactEmailShouldBeFound("emailType.contains=" + DEFAULT_EMAIL_TYPE);

        // Get all the contactEmailList where emailType contains UPDATED_EMAIL_TYPE
        defaultContactEmailShouldNotBeFound("emailType.contains=" + UPDATED_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactEmailsByEmailTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        // Get all the contactEmailList where emailType does not contain DEFAULT_EMAIL_TYPE
        defaultContactEmailShouldNotBeFound("emailType.doesNotContain=" + DEFAULT_EMAIL_TYPE);

        // Get all the contactEmailList where emailType does not contain UPDATED_EMAIL_TYPE
        defaultContactEmailShouldBeFound("emailType.doesNotContain=" + UPDATED_EMAIL_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactEmailsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactEmail.setContact(contact);
        contactEmailRepository.saveAndFlush(contactEmail);
        Long contactId = contact.getId();

        // Get all the contactEmailList where contact equals to contactId
        defaultContactEmailShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactEmailList where contact equals to contactId + 1
        defaultContactEmailShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactEmailShouldBeFound(String filter) throws Exception {
        restContactEmailMockMvc.perform(get("/api/contact-emails?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactEmail.getId().intValue())))
            .andExpect(jsonPath("$.[*].emailAddress").value(hasItem(DEFAULT_EMAIL_ADDRESS)))
            .andExpect(jsonPath("$.[*].emailType").value(hasItem(DEFAULT_EMAIL_TYPE)));

        // Check, that the count call also returns 1
        restContactEmailMockMvc.perform(get("/api/contact-emails/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactEmailShouldNotBeFound(String filter) throws Exception {
        restContactEmailMockMvc.perform(get("/api/contact-emails?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactEmailMockMvc.perform(get("/api/contact-emails/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactEmail() throws Exception {
        // Get the contactEmail
        restContactEmailMockMvc.perform(get("/api/contact-emails/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactEmail() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        int databaseSizeBeforeUpdate = contactEmailRepository.findAll().size();

        // Update the contactEmail
        ContactEmail updatedContactEmail = contactEmailRepository.findById(contactEmail.getId()).get();
        // Disconnect from session so that the updates on updatedContactEmail are not directly saved in db
        em.detach(updatedContactEmail);
        updatedContactEmail
            .emailAddress(UPDATED_EMAIL_ADDRESS)
            .emailType(UPDATED_EMAIL_TYPE);
        ContactEmailDTO contactEmailDTO = contactEmailMapper.toDto(updatedContactEmail);

        restContactEmailMockMvc.perform(put("/api/contact-emails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactEmailDTO)))
            .andExpect(status().isOk());

        // Validate the ContactEmail in the database
        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeUpdate);
        ContactEmail testContactEmail = contactEmailList.get(contactEmailList.size() - 1);
        assertThat(testContactEmail.getEmailAddress()).isEqualTo(UPDATED_EMAIL_ADDRESS);
        assertThat(testContactEmail.getEmailType()).isEqualTo(UPDATED_EMAIL_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactEmail() throws Exception {
        int databaseSizeBeforeUpdate = contactEmailRepository.findAll().size();

        // Create the ContactEmail
        ContactEmailDTO contactEmailDTO = contactEmailMapper.toDto(contactEmail);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactEmailMockMvc.perform(put("/api/contact-emails")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactEmailDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactEmail in the database
        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactEmail() throws Exception {
        // Initialize the database
        contactEmailRepository.saveAndFlush(contactEmail);

        int databaseSizeBeforeDelete = contactEmailRepository.findAll().size();

        // Delete the contactEmail
        restContactEmailMockMvc.perform(delete("/api/contact-emails/{id}", contactEmail.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactEmail> contactEmailList = contactEmailRepository.findAll();
        assertThat(contactEmailList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

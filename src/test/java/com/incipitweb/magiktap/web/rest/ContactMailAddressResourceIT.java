package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactMailAddress;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactMailAddressRepository;
import com.incipitweb.magiktap.service.ContactMailAddressService;
import com.incipitweb.magiktap.service.dto.ContactMailAddressDTO;
import com.incipitweb.magiktap.service.mapper.ContactMailAddressMapper;
import com.incipitweb.magiktap.service.dto.ContactMailAddressCriteria;
import com.incipitweb.magiktap.service.ContactMailAddressQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactMailAddressResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactMailAddressResourceIT {

    private static final String DEFAULT_POBOX = "AAAAAAAAAA";
    private static final String UPDATED_POBOX = "BBBBBBBBBB";

    private static final String DEFAULT_EXTENDED_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_EXTENDED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_STREET = "AAAAAAAAAA";
    private static final String UPDATED_STREET = "BBBBBBBBBB";

    private static final String DEFAULT_LOCALITY = "AAAAAAAAAA";
    private static final String UPDATED_LOCALITY = "BBBBBBBBBB";

    private static final String DEFAULT_REGION = "AAAAAAAAAA";
    private static final String UPDATED_REGION = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_MAIL_ADDRESS_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MAIL_ADDRESS_TYPE = "BBBBBBBBBB";

    @Autowired
    private ContactMailAddressRepository contactMailAddressRepository;

    @Autowired
    private ContactMailAddressMapper contactMailAddressMapper;

    @Autowired
    private ContactMailAddressService contactMailAddressService;

    @Autowired
    private ContactMailAddressQueryService contactMailAddressQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactMailAddressMockMvc;

    private ContactMailAddress contactMailAddress;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMailAddress createEntity(EntityManager em) {
        ContactMailAddress contactMailAddress = new ContactMailAddress()
            .pobox(DEFAULT_POBOX)
            .extendedAddress(DEFAULT_EXTENDED_ADDRESS)
            .street(DEFAULT_STREET)
            .locality(DEFAULT_LOCALITY)
            .region(DEFAULT_REGION)
            .postalCode(DEFAULT_POSTAL_CODE)
            .country(DEFAULT_COUNTRY)
            .mailAddressType(DEFAULT_MAIL_ADDRESS_TYPE);
        return contactMailAddress;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactMailAddress createUpdatedEntity(EntityManager em) {
        ContactMailAddress contactMailAddress = new ContactMailAddress()
            .pobox(UPDATED_POBOX)
            .extendedAddress(UPDATED_EXTENDED_ADDRESS)
            .street(UPDATED_STREET)
            .locality(UPDATED_LOCALITY)
            .region(UPDATED_REGION)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .mailAddressType(UPDATED_MAIL_ADDRESS_TYPE);
        return contactMailAddress;
    }

    @BeforeEach
    public void initTest() {
        contactMailAddress = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactMailAddress() throws Exception {
        int databaseSizeBeforeCreate = contactMailAddressRepository.findAll().size();
        // Create the ContactMailAddress
        ContactMailAddressDTO contactMailAddressDTO = contactMailAddressMapper.toDto(contactMailAddress);
        restContactMailAddressMockMvc.perform(post("/api/contact-mail-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMailAddressDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactMailAddress in the database
        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeCreate + 1);
        ContactMailAddress testContactMailAddress = contactMailAddressList.get(contactMailAddressList.size() - 1);
        assertThat(testContactMailAddress.getPobox()).isEqualTo(DEFAULT_POBOX);
        assertThat(testContactMailAddress.getExtendedAddress()).isEqualTo(DEFAULT_EXTENDED_ADDRESS);
        assertThat(testContactMailAddress.getStreet()).isEqualTo(DEFAULT_STREET);
        assertThat(testContactMailAddress.getLocality()).isEqualTo(DEFAULT_LOCALITY);
        assertThat(testContactMailAddress.getRegion()).isEqualTo(DEFAULT_REGION);
        assertThat(testContactMailAddress.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testContactMailAddress.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testContactMailAddress.getMailAddressType()).isEqualTo(DEFAULT_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void createContactMailAddressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactMailAddressRepository.findAll().size();

        // Create the ContactMailAddress with an existing ID
        contactMailAddress.setId(1L);
        ContactMailAddressDTO contactMailAddressDTO = contactMailAddressMapper.toDto(contactMailAddress);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMailAddressMockMvc.perform(post("/api/contact-mail-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMailAddressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMailAddress in the database
        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStreetIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactMailAddressRepository.findAll().size();
        // set the field null
        contactMailAddress.setStreet(null);

        // Create the ContactMailAddress, which fails.
        ContactMailAddressDTO contactMailAddressDTO = contactMailAddressMapper.toDto(contactMailAddress);


        restContactMailAddressMockMvc.perform(post("/api/contact-mail-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMailAddressDTO)))
            .andExpect(status().isBadRequest());

        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactMailAddresses() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMailAddress.getId().intValue())))
            .andExpect(jsonPath("$.[*].pobox").value(hasItem(DEFAULT_POBOX)))
            .andExpect(jsonPath("$.[*].extendedAddress").value(hasItem(DEFAULT_EXTENDED_ADDRESS)))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_STREET)))
            .andExpect(jsonPath("$.[*].locality").value(hasItem(DEFAULT_LOCALITY)))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].mailAddressType").value(hasItem(DEFAULT_MAIL_ADDRESS_TYPE)));
    }
    
    @Test
    @Transactional
    public void getContactMailAddress() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get the contactMailAddress
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses/{id}", contactMailAddress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactMailAddress.getId().intValue()))
            .andExpect(jsonPath("$.pobox").value(DEFAULT_POBOX))
            .andExpect(jsonPath("$.extendedAddress").value(DEFAULT_EXTENDED_ADDRESS))
            .andExpect(jsonPath("$.street").value(DEFAULT_STREET))
            .andExpect(jsonPath("$.locality").value(DEFAULT_LOCALITY))
            .andExpect(jsonPath("$.region").value(DEFAULT_REGION))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.mailAddressType").value(DEFAULT_MAIL_ADDRESS_TYPE));
    }


    @Test
    @Transactional
    public void getContactMailAddressesByIdFiltering() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        Long id = contactMailAddress.getId();

        defaultContactMailAddressShouldBeFound("id.equals=" + id);
        defaultContactMailAddressShouldNotBeFound("id.notEquals=" + id);

        defaultContactMailAddressShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactMailAddressShouldNotBeFound("id.greaterThan=" + id);

        defaultContactMailAddressShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactMailAddressShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox equals to DEFAULT_POBOX
        defaultContactMailAddressShouldBeFound("pobox.equals=" + DEFAULT_POBOX);

        // Get all the contactMailAddressList where pobox equals to UPDATED_POBOX
        defaultContactMailAddressShouldNotBeFound("pobox.equals=" + UPDATED_POBOX);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox not equals to DEFAULT_POBOX
        defaultContactMailAddressShouldNotBeFound("pobox.notEquals=" + DEFAULT_POBOX);

        // Get all the contactMailAddressList where pobox not equals to UPDATED_POBOX
        defaultContactMailAddressShouldBeFound("pobox.notEquals=" + UPDATED_POBOX);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox in DEFAULT_POBOX or UPDATED_POBOX
        defaultContactMailAddressShouldBeFound("pobox.in=" + DEFAULT_POBOX + "," + UPDATED_POBOX);

        // Get all the contactMailAddressList where pobox equals to UPDATED_POBOX
        defaultContactMailAddressShouldNotBeFound("pobox.in=" + UPDATED_POBOX);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox is not null
        defaultContactMailAddressShouldBeFound("pobox.specified=true");

        // Get all the contactMailAddressList where pobox is null
        defaultContactMailAddressShouldNotBeFound("pobox.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox contains DEFAULT_POBOX
        defaultContactMailAddressShouldBeFound("pobox.contains=" + DEFAULT_POBOX);

        // Get all the contactMailAddressList where pobox contains UPDATED_POBOX
        defaultContactMailAddressShouldNotBeFound("pobox.contains=" + UPDATED_POBOX);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPoboxNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where pobox does not contain DEFAULT_POBOX
        defaultContactMailAddressShouldNotBeFound("pobox.doesNotContain=" + DEFAULT_POBOX);

        // Get all the contactMailAddressList where pobox does not contain UPDATED_POBOX
        defaultContactMailAddressShouldBeFound("pobox.doesNotContain=" + UPDATED_POBOX);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress equals to DEFAULT_EXTENDED_ADDRESS
        defaultContactMailAddressShouldBeFound("extendedAddress.equals=" + DEFAULT_EXTENDED_ADDRESS);

        // Get all the contactMailAddressList where extendedAddress equals to UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldNotBeFound("extendedAddress.equals=" + UPDATED_EXTENDED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress not equals to DEFAULT_EXTENDED_ADDRESS
        defaultContactMailAddressShouldNotBeFound("extendedAddress.notEquals=" + DEFAULT_EXTENDED_ADDRESS);

        // Get all the contactMailAddressList where extendedAddress not equals to UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldBeFound("extendedAddress.notEquals=" + UPDATED_EXTENDED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress in DEFAULT_EXTENDED_ADDRESS or UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldBeFound("extendedAddress.in=" + DEFAULT_EXTENDED_ADDRESS + "," + UPDATED_EXTENDED_ADDRESS);

        // Get all the contactMailAddressList where extendedAddress equals to UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldNotBeFound("extendedAddress.in=" + UPDATED_EXTENDED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress is not null
        defaultContactMailAddressShouldBeFound("extendedAddress.specified=true");

        // Get all the contactMailAddressList where extendedAddress is null
        defaultContactMailAddressShouldNotBeFound("extendedAddress.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress contains DEFAULT_EXTENDED_ADDRESS
        defaultContactMailAddressShouldBeFound("extendedAddress.contains=" + DEFAULT_EXTENDED_ADDRESS);

        // Get all the contactMailAddressList where extendedAddress contains UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldNotBeFound("extendedAddress.contains=" + UPDATED_EXTENDED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByExtendedAddressNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where extendedAddress does not contain DEFAULT_EXTENDED_ADDRESS
        defaultContactMailAddressShouldNotBeFound("extendedAddress.doesNotContain=" + DEFAULT_EXTENDED_ADDRESS);

        // Get all the contactMailAddressList where extendedAddress does not contain UPDATED_EXTENDED_ADDRESS
        defaultContactMailAddressShouldBeFound("extendedAddress.doesNotContain=" + UPDATED_EXTENDED_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByStreetIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street equals to DEFAULT_STREET
        defaultContactMailAddressShouldBeFound("street.equals=" + DEFAULT_STREET);

        // Get all the contactMailAddressList where street equals to UPDATED_STREET
        defaultContactMailAddressShouldNotBeFound("street.equals=" + UPDATED_STREET);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByStreetIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street not equals to DEFAULT_STREET
        defaultContactMailAddressShouldNotBeFound("street.notEquals=" + DEFAULT_STREET);

        // Get all the contactMailAddressList where street not equals to UPDATED_STREET
        defaultContactMailAddressShouldBeFound("street.notEquals=" + UPDATED_STREET);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByStreetIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street in DEFAULT_STREET or UPDATED_STREET
        defaultContactMailAddressShouldBeFound("street.in=" + DEFAULT_STREET + "," + UPDATED_STREET);

        // Get all the contactMailAddressList where street equals to UPDATED_STREET
        defaultContactMailAddressShouldNotBeFound("street.in=" + UPDATED_STREET);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByStreetIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street is not null
        defaultContactMailAddressShouldBeFound("street.specified=true");

        // Get all the contactMailAddressList where street is null
        defaultContactMailAddressShouldNotBeFound("street.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByStreetContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street contains DEFAULT_STREET
        defaultContactMailAddressShouldBeFound("street.contains=" + DEFAULT_STREET);

        // Get all the contactMailAddressList where street contains UPDATED_STREET
        defaultContactMailAddressShouldNotBeFound("street.contains=" + UPDATED_STREET);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByStreetNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where street does not contain DEFAULT_STREET
        defaultContactMailAddressShouldNotBeFound("street.doesNotContain=" + DEFAULT_STREET);

        // Get all the contactMailAddressList where street does not contain UPDATED_STREET
        defaultContactMailAddressShouldBeFound("street.doesNotContain=" + UPDATED_STREET);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality equals to DEFAULT_LOCALITY
        defaultContactMailAddressShouldBeFound("locality.equals=" + DEFAULT_LOCALITY);

        // Get all the contactMailAddressList where locality equals to UPDATED_LOCALITY
        defaultContactMailAddressShouldNotBeFound("locality.equals=" + UPDATED_LOCALITY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality not equals to DEFAULT_LOCALITY
        defaultContactMailAddressShouldNotBeFound("locality.notEquals=" + DEFAULT_LOCALITY);

        // Get all the contactMailAddressList where locality not equals to UPDATED_LOCALITY
        defaultContactMailAddressShouldBeFound("locality.notEquals=" + UPDATED_LOCALITY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality in DEFAULT_LOCALITY or UPDATED_LOCALITY
        defaultContactMailAddressShouldBeFound("locality.in=" + DEFAULT_LOCALITY + "," + UPDATED_LOCALITY);

        // Get all the contactMailAddressList where locality equals to UPDATED_LOCALITY
        defaultContactMailAddressShouldNotBeFound("locality.in=" + UPDATED_LOCALITY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality is not null
        defaultContactMailAddressShouldBeFound("locality.specified=true");

        // Get all the contactMailAddressList where locality is null
        defaultContactMailAddressShouldNotBeFound("locality.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality contains DEFAULT_LOCALITY
        defaultContactMailAddressShouldBeFound("locality.contains=" + DEFAULT_LOCALITY);

        // Get all the contactMailAddressList where locality contains UPDATED_LOCALITY
        defaultContactMailAddressShouldNotBeFound("locality.contains=" + UPDATED_LOCALITY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByLocalityNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where locality does not contain DEFAULT_LOCALITY
        defaultContactMailAddressShouldNotBeFound("locality.doesNotContain=" + DEFAULT_LOCALITY);

        // Get all the contactMailAddressList where locality does not contain UPDATED_LOCALITY
        defaultContactMailAddressShouldBeFound("locality.doesNotContain=" + UPDATED_LOCALITY);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByRegionIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region equals to DEFAULT_REGION
        defaultContactMailAddressShouldBeFound("region.equals=" + DEFAULT_REGION);

        // Get all the contactMailAddressList where region equals to UPDATED_REGION
        defaultContactMailAddressShouldNotBeFound("region.equals=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByRegionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region not equals to DEFAULT_REGION
        defaultContactMailAddressShouldNotBeFound("region.notEquals=" + DEFAULT_REGION);

        // Get all the contactMailAddressList where region not equals to UPDATED_REGION
        defaultContactMailAddressShouldBeFound("region.notEquals=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByRegionIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region in DEFAULT_REGION or UPDATED_REGION
        defaultContactMailAddressShouldBeFound("region.in=" + DEFAULT_REGION + "," + UPDATED_REGION);

        // Get all the contactMailAddressList where region equals to UPDATED_REGION
        defaultContactMailAddressShouldNotBeFound("region.in=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByRegionIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region is not null
        defaultContactMailAddressShouldBeFound("region.specified=true");

        // Get all the contactMailAddressList where region is null
        defaultContactMailAddressShouldNotBeFound("region.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByRegionContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region contains DEFAULT_REGION
        defaultContactMailAddressShouldBeFound("region.contains=" + DEFAULT_REGION);

        // Get all the contactMailAddressList where region contains UPDATED_REGION
        defaultContactMailAddressShouldNotBeFound("region.contains=" + UPDATED_REGION);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByRegionNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where region does not contain DEFAULT_REGION
        defaultContactMailAddressShouldNotBeFound("region.doesNotContain=" + DEFAULT_REGION);

        // Get all the contactMailAddressList where region does not contain UPDATED_REGION
        defaultContactMailAddressShouldBeFound("region.doesNotContain=" + UPDATED_REGION);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode equals to DEFAULT_POSTAL_CODE
        defaultContactMailAddressShouldBeFound("postalCode.equals=" + DEFAULT_POSTAL_CODE);

        // Get all the contactMailAddressList where postalCode equals to UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldNotBeFound("postalCode.equals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode not equals to DEFAULT_POSTAL_CODE
        defaultContactMailAddressShouldNotBeFound("postalCode.notEquals=" + DEFAULT_POSTAL_CODE);

        // Get all the contactMailAddressList where postalCode not equals to UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldBeFound("postalCode.notEquals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode in DEFAULT_POSTAL_CODE or UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldBeFound("postalCode.in=" + DEFAULT_POSTAL_CODE + "," + UPDATED_POSTAL_CODE);

        // Get all the contactMailAddressList where postalCode equals to UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldNotBeFound("postalCode.in=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode is not null
        defaultContactMailAddressShouldBeFound("postalCode.specified=true");

        // Get all the contactMailAddressList where postalCode is null
        defaultContactMailAddressShouldNotBeFound("postalCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode contains DEFAULT_POSTAL_CODE
        defaultContactMailAddressShouldBeFound("postalCode.contains=" + DEFAULT_POSTAL_CODE);

        // Get all the contactMailAddressList where postalCode contains UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldNotBeFound("postalCode.contains=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByPostalCodeNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where postalCode does not contain DEFAULT_POSTAL_CODE
        defaultContactMailAddressShouldNotBeFound("postalCode.doesNotContain=" + DEFAULT_POSTAL_CODE);

        // Get all the contactMailAddressList where postalCode does not contain UPDATED_POSTAL_CODE
        defaultContactMailAddressShouldBeFound("postalCode.doesNotContain=" + UPDATED_POSTAL_CODE);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country equals to DEFAULT_COUNTRY
        defaultContactMailAddressShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the contactMailAddressList where country equals to UPDATED_COUNTRY
        defaultContactMailAddressShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByCountryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country not equals to DEFAULT_COUNTRY
        defaultContactMailAddressShouldNotBeFound("country.notEquals=" + DEFAULT_COUNTRY);

        // Get all the contactMailAddressList where country not equals to UPDATED_COUNTRY
        defaultContactMailAddressShouldBeFound("country.notEquals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultContactMailAddressShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the contactMailAddressList where country equals to UPDATED_COUNTRY
        defaultContactMailAddressShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country is not null
        defaultContactMailAddressShouldBeFound("country.specified=true");

        // Get all the contactMailAddressList where country is null
        defaultContactMailAddressShouldNotBeFound("country.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByCountryContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country contains DEFAULT_COUNTRY
        defaultContactMailAddressShouldBeFound("country.contains=" + DEFAULT_COUNTRY);

        // Get all the contactMailAddressList where country contains UPDATED_COUNTRY
        defaultContactMailAddressShouldNotBeFound("country.contains=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByCountryNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where country does not contain DEFAULT_COUNTRY
        defaultContactMailAddressShouldNotBeFound("country.doesNotContain=" + DEFAULT_COUNTRY);

        // Get all the contactMailAddressList where country does not contain UPDATED_COUNTRY
        defaultContactMailAddressShouldBeFound("country.doesNotContain=" + UPDATED_COUNTRY);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType equals to DEFAULT_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldBeFound("mailAddressType.equals=" + DEFAULT_MAIL_ADDRESS_TYPE);

        // Get all the contactMailAddressList where mailAddressType equals to UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldNotBeFound("mailAddressType.equals=" + UPDATED_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType not equals to DEFAULT_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldNotBeFound("mailAddressType.notEquals=" + DEFAULT_MAIL_ADDRESS_TYPE);

        // Get all the contactMailAddressList where mailAddressType not equals to UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldBeFound("mailAddressType.notEquals=" + UPDATED_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType in DEFAULT_MAIL_ADDRESS_TYPE or UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldBeFound("mailAddressType.in=" + DEFAULT_MAIL_ADDRESS_TYPE + "," + UPDATED_MAIL_ADDRESS_TYPE);

        // Get all the contactMailAddressList where mailAddressType equals to UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldNotBeFound("mailAddressType.in=" + UPDATED_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType is not null
        defaultContactMailAddressShouldBeFound("mailAddressType.specified=true");

        // Get all the contactMailAddressList where mailAddressType is null
        defaultContactMailAddressShouldNotBeFound("mailAddressType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType contains DEFAULT_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldBeFound("mailAddressType.contains=" + DEFAULT_MAIL_ADDRESS_TYPE);

        // Get all the contactMailAddressList where mailAddressType contains UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldNotBeFound("mailAddressType.contains=" + UPDATED_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactMailAddressesByMailAddressTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        // Get all the contactMailAddressList where mailAddressType does not contain DEFAULT_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldNotBeFound("mailAddressType.doesNotContain=" + DEFAULT_MAIL_ADDRESS_TYPE);

        // Get all the contactMailAddressList where mailAddressType does not contain UPDATED_MAIL_ADDRESS_TYPE
        defaultContactMailAddressShouldBeFound("mailAddressType.doesNotContain=" + UPDATED_MAIL_ADDRESS_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactMailAddressesByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactMailAddress.setContact(contact);
        contactMailAddressRepository.saveAndFlush(contactMailAddress);
        Long contactId = contact.getId();

        // Get all the contactMailAddressList where contact equals to contactId
        defaultContactMailAddressShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactMailAddressList where contact equals to contactId + 1
        defaultContactMailAddressShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactMailAddressShouldBeFound(String filter) throws Exception {
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactMailAddress.getId().intValue())))
            .andExpect(jsonPath("$.[*].pobox").value(hasItem(DEFAULT_POBOX)))
            .andExpect(jsonPath("$.[*].extendedAddress").value(hasItem(DEFAULT_EXTENDED_ADDRESS)))
            .andExpect(jsonPath("$.[*].street").value(hasItem(DEFAULT_STREET)))
            .andExpect(jsonPath("$.[*].locality").value(hasItem(DEFAULT_LOCALITY)))
            .andExpect(jsonPath("$.[*].region").value(hasItem(DEFAULT_REGION)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].mailAddressType").value(hasItem(DEFAULT_MAIL_ADDRESS_TYPE)));

        // Check, that the count call also returns 1
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactMailAddressShouldNotBeFound(String filter) throws Exception {
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactMailAddress() throws Exception {
        // Get the contactMailAddress
        restContactMailAddressMockMvc.perform(get("/api/contact-mail-addresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactMailAddress() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        int databaseSizeBeforeUpdate = contactMailAddressRepository.findAll().size();

        // Update the contactMailAddress
        ContactMailAddress updatedContactMailAddress = contactMailAddressRepository.findById(contactMailAddress.getId()).get();
        // Disconnect from session so that the updates on updatedContactMailAddress are not directly saved in db
        em.detach(updatedContactMailAddress);
        updatedContactMailAddress
            .pobox(UPDATED_POBOX)
            .extendedAddress(UPDATED_EXTENDED_ADDRESS)
            .street(UPDATED_STREET)
            .locality(UPDATED_LOCALITY)
            .region(UPDATED_REGION)
            .postalCode(UPDATED_POSTAL_CODE)
            .country(UPDATED_COUNTRY)
            .mailAddressType(UPDATED_MAIL_ADDRESS_TYPE);
        ContactMailAddressDTO contactMailAddressDTO = contactMailAddressMapper.toDto(updatedContactMailAddress);

        restContactMailAddressMockMvc.perform(put("/api/contact-mail-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMailAddressDTO)))
            .andExpect(status().isOk());

        // Validate the ContactMailAddress in the database
        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeUpdate);
        ContactMailAddress testContactMailAddress = contactMailAddressList.get(contactMailAddressList.size() - 1);
        assertThat(testContactMailAddress.getPobox()).isEqualTo(UPDATED_POBOX);
        assertThat(testContactMailAddress.getExtendedAddress()).isEqualTo(UPDATED_EXTENDED_ADDRESS);
        assertThat(testContactMailAddress.getStreet()).isEqualTo(UPDATED_STREET);
        assertThat(testContactMailAddress.getLocality()).isEqualTo(UPDATED_LOCALITY);
        assertThat(testContactMailAddress.getRegion()).isEqualTo(UPDATED_REGION);
        assertThat(testContactMailAddress.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testContactMailAddress.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testContactMailAddress.getMailAddressType()).isEqualTo(UPDATED_MAIL_ADDRESS_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactMailAddress() throws Exception {
        int databaseSizeBeforeUpdate = contactMailAddressRepository.findAll().size();

        // Create the ContactMailAddress
        ContactMailAddressDTO contactMailAddressDTO = contactMailAddressMapper.toDto(contactMailAddress);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMailAddressMockMvc.perform(put("/api/contact-mail-addresses")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactMailAddressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactMailAddress in the database
        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactMailAddress() throws Exception {
        // Initialize the database
        contactMailAddressRepository.saveAndFlush(contactMailAddress);

        int databaseSizeBeforeDelete = contactMailAddressRepository.findAll().size();

        // Delete the contactMailAddress
        restContactMailAddressMockMvc.perform(delete("/api/contact-mail-addresses/{id}", contactMailAddress.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactMailAddress> contactMailAddressList = contactMailAddressRepository.findAll();
        assertThat(contactMailAddressList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

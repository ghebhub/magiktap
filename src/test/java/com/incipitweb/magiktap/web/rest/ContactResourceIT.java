package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.domain.ContactAgent;
import com.incipitweb.magiktap.domain.ContactCategories;
import com.incipitweb.magiktap.domain.ContactData;
import com.incipitweb.magiktap.domain.ContactEmail;
import com.incipitweb.magiktap.domain.ContactKeys;
import com.incipitweb.magiktap.domain.ContactMailAddress;
import com.incipitweb.magiktap.domain.ContactNote;
import com.incipitweb.magiktap.domain.ContactPhoneNumber;
import com.incipitweb.magiktap.domain.ContactXtended;
import com.incipitweb.magiktap.repository.ContactRepository;
import com.incipitweb.magiktap.service.ContactService;
import com.incipitweb.magiktap.service.dto.ContactDTO;
import com.incipitweb.magiktap.service.mapper.ContactMapper;
import com.incipitweb.magiktap.service.dto.ContactCriteria;
import com.incipitweb.magiktap.service.ContactQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactResourceIT {

    private static final String DEFAULT_CONTACT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_FN = "AAAAAAAAAA";
    private static final String UPDATED_FN = "BBBBBBBBBB";

    private static final String DEFAULT_N = "AAAAAAAAAA";
    private static final String UPDATED_N = "BBBBBBBBBB";

    private static final String DEFAULT_NICKNAME = "AAAAAAAAAA";
    private static final String UPDATED_NICKNAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_BDAY = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BDAY = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_MAILER = "AAAAAAAAAA";
    private static final String UPDATED_MAILER = "BBBBBBBBBB";

    private static final Integer DEFAULT_TZ = 1;
    private static final Integer UPDATED_TZ = 2;
    private static final Integer SMALLER_TZ = 1 - 1;

    private static final Double DEFAULT_GEO_LAT = 1D;
    private static final Double UPDATED_GEO_LAT = 2D;
    private static final Double SMALLER_GEO_LAT = 1D - 1D;

    private static final Double DEFAULT_GEO_LONG = 1D;
    private static final Double UPDATED_GEO_LONG = 2D;
    private static final Double SMALLER_GEO_LONG = 1D - 1D;

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_ROLE = "AAAAAAAAAA";
    private static final String UPDATED_ROLE = "BBBBBBBBBB";

    private static final String DEFAULT_PROD_ID = "AAAAAAAAAA";
    private static final String UPDATED_PROD_ID = "BBBBBBBBBB";

    private static final String DEFAULT_REV = "AAAAAAAAAA";
    private static final String UPDATED_REV = "BBBBBBBBBB";

    private static final String DEFAULT_SORT_STRING = "AAAAAAAAAA";
    private static final String UPDATED_SORT_STRING = "BBBBBBBBBB";

    private static final String DEFAULT_UID = "AAAAAAAAAA";
    private static final String UPDATED_UID = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_CLASS_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLASS_NAME = "BBBBBBBBBB";

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ContactMapper contactMapper;

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactQueryService contactQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactMockMvc;

    private Contact contact;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contact createEntity(EntityManager em) {
        Contact contact = new Contact()
            .contactId(DEFAULT_CONTACT_ID)
            .fn(DEFAULT_FN)
            .n(DEFAULT_N)
            .nickname(DEFAULT_NICKNAME)
            .bday(DEFAULT_BDAY)
            .mailer(DEFAULT_MAILER)
            .tz(DEFAULT_TZ)
            .geoLat(DEFAULT_GEO_LAT)
            .geoLong(DEFAULT_GEO_LONG)
            .title(DEFAULT_TITLE)
            .role(DEFAULT_ROLE)
            .prodId(DEFAULT_PROD_ID)
            .rev(DEFAULT_REV)
            .sortString(DEFAULT_SORT_STRING)
            .uid(DEFAULT_UID)
            .url(DEFAULT_URL)
            .version(DEFAULT_VERSION)
            .className(DEFAULT_CLASS_NAME);
        return contact;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contact createUpdatedEntity(EntityManager em) {
        Contact contact = new Contact()
            .contactId(UPDATED_CONTACT_ID)
            .fn(UPDATED_FN)
            .n(UPDATED_N)
            .nickname(UPDATED_NICKNAME)
            .bday(UPDATED_BDAY)
            .mailer(UPDATED_MAILER)
            .tz(UPDATED_TZ)
            .geoLat(UPDATED_GEO_LAT)
            .geoLong(UPDATED_GEO_LONG)
            .title(UPDATED_TITLE)
            .role(UPDATED_ROLE)
            .prodId(UPDATED_PROD_ID)
            .rev(UPDATED_REV)
            .sortString(UPDATED_SORT_STRING)
            .uid(UPDATED_UID)
            .url(UPDATED_URL)
            .version(UPDATED_VERSION)
            .className(UPDATED_CLASS_NAME);
        return contact;
    }

    @BeforeEach
    public void initTest() {
        contact = createEntity(em);
    }

    @Test
    @Transactional
    public void createContact() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();
        // Create the Contact
        ContactDTO contactDTO = contactMapper.toDto(contact);
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isCreated());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate + 1);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getContactId()).isEqualTo(DEFAULT_CONTACT_ID);
        assertThat(testContact.getFn()).isEqualTo(DEFAULT_FN);
        assertThat(testContact.getN()).isEqualTo(DEFAULT_N);
        assertThat(testContact.getNickname()).isEqualTo(DEFAULT_NICKNAME);
        assertThat(testContact.getBday()).isEqualTo(DEFAULT_BDAY);
        assertThat(testContact.getMailer()).isEqualTo(DEFAULT_MAILER);
        assertThat(testContact.getTz()).isEqualTo(DEFAULT_TZ);
        assertThat(testContact.getGeoLat()).isEqualTo(DEFAULT_GEO_LAT);
        assertThat(testContact.getGeoLong()).isEqualTo(DEFAULT_GEO_LONG);
        assertThat(testContact.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testContact.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testContact.getProdId()).isEqualTo(DEFAULT_PROD_ID);
        assertThat(testContact.getRev()).isEqualTo(DEFAULT_REV);
        assertThat(testContact.getSortString()).isEqualTo(DEFAULT_SORT_STRING);
        assertThat(testContact.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testContact.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testContact.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testContact.getClassName()).isEqualTo(DEFAULT_CLASS_NAME);
    }

    @Test
    @Transactional
    public void createContactWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();

        // Create the Contact with an existing ID
        contact.setId(1L);
        ContactDTO contactDTO = contactMapper.toDto(contact);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkContactIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setContactId(null);

        // Create the Contact, which fails.
        ContactDTO contactDTO = contactMapper.toDto(contact);


        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFnIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setFn(null);

        // Create the Contact, which fails.
        ContactDTO contactDTO = contactMapper.toDto(contact);


        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setN(null);

        // Create the Contact, which fails.
        ContactDTO contactDTO = contactMapper.toDto(contact);


        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBdayIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setBday(null);

        // Create the Contact, which fails.
        ContactDTO contactDTO = contactMapper.toDto(contact);


        restContactMockMvc.perform(post("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContacts() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].fn").value(hasItem(DEFAULT_FN)))
            .andExpect(jsonPath("$.[*].n").value(hasItem(DEFAULT_N)))
            .andExpect(jsonPath("$.[*].nickname").value(hasItem(DEFAULT_NICKNAME)))
            .andExpect(jsonPath("$.[*].bday").value(hasItem(DEFAULT_BDAY.toString())))
            .andExpect(jsonPath("$.[*].mailer").value(hasItem(DEFAULT_MAILER)))
            .andExpect(jsonPath("$.[*].tz").value(hasItem(DEFAULT_TZ)))
            .andExpect(jsonPath("$.[*].geoLat").value(hasItem(DEFAULT_GEO_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].geoLong").value(hasItem(DEFAULT_GEO_LONG.doubleValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE)))
            .andExpect(jsonPath("$.[*].prodId").value(hasItem(DEFAULT_PROD_ID)))
            .andExpect(jsonPath("$.[*].rev").value(hasItem(DEFAULT_REV)))
            .andExpect(jsonPath("$.[*].sortString").value(hasItem(DEFAULT_SORT_STRING)))
            .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].className").value(hasItem(DEFAULT_CLASS_NAME)));
    }
    
    @Test
    @Transactional
    public void getContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", contact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contact.getId().intValue()))
            .andExpect(jsonPath("$.contactId").value(DEFAULT_CONTACT_ID))
            .andExpect(jsonPath("$.fn").value(DEFAULT_FN))
            .andExpect(jsonPath("$.n").value(DEFAULT_N))
            .andExpect(jsonPath("$.nickname").value(DEFAULT_NICKNAME))
            .andExpect(jsonPath("$.bday").value(DEFAULT_BDAY.toString()))
            .andExpect(jsonPath("$.mailer").value(DEFAULT_MAILER))
            .andExpect(jsonPath("$.tz").value(DEFAULT_TZ))
            .andExpect(jsonPath("$.geoLat").value(DEFAULT_GEO_LAT.doubleValue()))
            .andExpect(jsonPath("$.geoLong").value(DEFAULT_GEO_LONG.doubleValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE))
            .andExpect(jsonPath("$.prodId").value(DEFAULT_PROD_ID))
            .andExpect(jsonPath("$.rev").value(DEFAULT_REV))
            .andExpect(jsonPath("$.sortString").value(DEFAULT_SORT_STRING))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION))
            .andExpect(jsonPath("$.className").value(DEFAULT_CLASS_NAME));
    }


    @Test
    @Transactional
    public void getContactsByIdFiltering() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        Long id = contact.getId();

        defaultContactShouldBeFound("id.equals=" + id);
        defaultContactShouldNotBeFound("id.notEquals=" + id);

        defaultContactShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactShouldNotBeFound("id.greaterThan=" + id);

        defaultContactShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactsByContactIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId equals to DEFAULT_CONTACT_ID
        defaultContactShouldBeFound("contactId.equals=" + DEFAULT_CONTACT_ID);

        // Get all the contactList where contactId equals to UPDATED_CONTACT_ID
        defaultContactShouldNotBeFound("contactId.equals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByContactIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId not equals to DEFAULT_CONTACT_ID
        defaultContactShouldNotBeFound("contactId.notEquals=" + DEFAULT_CONTACT_ID);

        // Get all the contactList where contactId not equals to UPDATED_CONTACT_ID
        defaultContactShouldBeFound("contactId.notEquals=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByContactIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId in DEFAULT_CONTACT_ID or UPDATED_CONTACT_ID
        defaultContactShouldBeFound("contactId.in=" + DEFAULT_CONTACT_ID + "," + UPDATED_CONTACT_ID);

        // Get all the contactList where contactId equals to UPDATED_CONTACT_ID
        defaultContactShouldNotBeFound("contactId.in=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByContactIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId is not null
        defaultContactShouldBeFound("contactId.specified=true");

        // Get all the contactList where contactId is null
        defaultContactShouldNotBeFound("contactId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByContactIdContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId contains DEFAULT_CONTACT_ID
        defaultContactShouldBeFound("contactId.contains=" + DEFAULT_CONTACT_ID);

        // Get all the contactList where contactId contains UPDATED_CONTACT_ID
        defaultContactShouldNotBeFound("contactId.contains=" + UPDATED_CONTACT_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByContactIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactId does not contain DEFAULT_CONTACT_ID
        defaultContactShouldNotBeFound("contactId.doesNotContain=" + DEFAULT_CONTACT_ID);

        // Get all the contactList where contactId does not contain UPDATED_CONTACT_ID
        defaultContactShouldBeFound("contactId.doesNotContain=" + UPDATED_CONTACT_ID);
    }


    @Test
    @Transactional
    public void getAllContactsByFnIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn equals to DEFAULT_FN
        defaultContactShouldBeFound("fn.equals=" + DEFAULT_FN);

        // Get all the contactList where fn equals to UPDATED_FN
        defaultContactShouldNotBeFound("fn.equals=" + UPDATED_FN);
    }

    @Test
    @Transactional
    public void getAllContactsByFnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn not equals to DEFAULT_FN
        defaultContactShouldNotBeFound("fn.notEquals=" + DEFAULT_FN);

        // Get all the contactList where fn not equals to UPDATED_FN
        defaultContactShouldBeFound("fn.notEquals=" + UPDATED_FN);
    }

    @Test
    @Transactional
    public void getAllContactsByFnIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn in DEFAULT_FN or UPDATED_FN
        defaultContactShouldBeFound("fn.in=" + DEFAULT_FN + "," + UPDATED_FN);

        // Get all the contactList where fn equals to UPDATED_FN
        defaultContactShouldNotBeFound("fn.in=" + UPDATED_FN);
    }

    @Test
    @Transactional
    public void getAllContactsByFnIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn is not null
        defaultContactShouldBeFound("fn.specified=true");

        // Get all the contactList where fn is null
        defaultContactShouldNotBeFound("fn.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByFnContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn contains DEFAULT_FN
        defaultContactShouldBeFound("fn.contains=" + DEFAULT_FN);

        // Get all the contactList where fn contains UPDATED_FN
        defaultContactShouldNotBeFound("fn.contains=" + UPDATED_FN);
    }

    @Test
    @Transactional
    public void getAllContactsByFnNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where fn does not contain DEFAULT_FN
        defaultContactShouldNotBeFound("fn.doesNotContain=" + DEFAULT_FN);

        // Get all the contactList where fn does not contain UPDATED_FN
        defaultContactShouldBeFound("fn.doesNotContain=" + UPDATED_FN);
    }


    @Test
    @Transactional
    public void getAllContactsByNIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n equals to DEFAULT_N
        defaultContactShouldBeFound("n.equals=" + DEFAULT_N);

        // Get all the contactList where n equals to UPDATED_N
        defaultContactShouldNotBeFound("n.equals=" + UPDATED_N);
    }

    @Test
    @Transactional
    public void getAllContactsByNIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n not equals to DEFAULT_N
        defaultContactShouldNotBeFound("n.notEquals=" + DEFAULT_N);

        // Get all the contactList where n not equals to UPDATED_N
        defaultContactShouldBeFound("n.notEquals=" + UPDATED_N);
    }

    @Test
    @Transactional
    public void getAllContactsByNIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n in DEFAULT_N or UPDATED_N
        defaultContactShouldBeFound("n.in=" + DEFAULT_N + "," + UPDATED_N);

        // Get all the contactList where n equals to UPDATED_N
        defaultContactShouldNotBeFound("n.in=" + UPDATED_N);
    }

    @Test
    @Transactional
    public void getAllContactsByNIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n is not null
        defaultContactShouldBeFound("n.specified=true");

        // Get all the contactList where n is null
        defaultContactShouldNotBeFound("n.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByNContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n contains DEFAULT_N
        defaultContactShouldBeFound("n.contains=" + DEFAULT_N);

        // Get all the contactList where n contains UPDATED_N
        defaultContactShouldNotBeFound("n.contains=" + UPDATED_N);
    }

    @Test
    @Transactional
    public void getAllContactsByNNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where n does not contain DEFAULT_N
        defaultContactShouldNotBeFound("n.doesNotContain=" + DEFAULT_N);

        // Get all the contactList where n does not contain UPDATED_N
        defaultContactShouldBeFound("n.doesNotContain=" + UPDATED_N);
    }


    @Test
    @Transactional
    public void getAllContactsByNicknameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname equals to DEFAULT_NICKNAME
        defaultContactShouldBeFound("nickname.equals=" + DEFAULT_NICKNAME);

        // Get all the contactList where nickname equals to UPDATED_NICKNAME
        defaultContactShouldNotBeFound("nickname.equals=" + UPDATED_NICKNAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNicknameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname not equals to DEFAULT_NICKNAME
        defaultContactShouldNotBeFound("nickname.notEquals=" + DEFAULT_NICKNAME);

        // Get all the contactList where nickname not equals to UPDATED_NICKNAME
        defaultContactShouldBeFound("nickname.notEquals=" + UPDATED_NICKNAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNicknameIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname in DEFAULT_NICKNAME or UPDATED_NICKNAME
        defaultContactShouldBeFound("nickname.in=" + DEFAULT_NICKNAME + "," + UPDATED_NICKNAME);

        // Get all the contactList where nickname equals to UPDATED_NICKNAME
        defaultContactShouldNotBeFound("nickname.in=" + UPDATED_NICKNAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNicknameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname is not null
        defaultContactShouldBeFound("nickname.specified=true");

        // Get all the contactList where nickname is null
        defaultContactShouldNotBeFound("nickname.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByNicknameContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname contains DEFAULT_NICKNAME
        defaultContactShouldBeFound("nickname.contains=" + DEFAULT_NICKNAME);

        // Get all the contactList where nickname contains UPDATED_NICKNAME
        defaultContactShouldNotBeFound("nickname.contains=" + UPDATED_NICKNAME);
    }

    @Test
    @Transactional
    public void getAllContactsByNicknameNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where nickname does not contain DEFAULT_NICKNAME
        defaultContactShouldNotBeFound("nickname.doesNotContain=" + DEFAULT_NICKNAME);

        // Get all the contactList where nickname does not contain UPDATED_NICKNAME
        defaultContactShouldBeFound("nickname.doesNotContain=" + UPDATED_NICKNAME);
    }


    @Test
    @Transactional
    public void getAllContactsByBdayIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where bday equals to DEFAULT_BDAY
        defaultContactShouldBeFound("bday.equals=" + DEFAULT_BDAY);

        // Get all the contactList where bday equals to UPDATED_BDAY
        defaultContactShouldNotBeFound("bday.equals=" + UPDATED_BDAY);
    }

    @Test
    @Transactional
    public void getAllContactsByBdayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where bday not equals to DEFAULT_BDAY
        defaultContactShouldNotBeFound("bday.notEquals=" + DEFAULT_BDAY);

        // Get all the contactList where bday not equals to UPDATED_BDAY
        defaultContactShouldBeFound("bday.notEquals=" + UPDATED_BDAY);
    }

    @Test
    @Transactional
    public void getAllContactsByBdayIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where bday in DEFAULT_BDAY or UPDATED_BDAY
        defaultContactShouldBeFound("bday.in=" + DEFAULT_BDAY + "," + UPDATED_BDAY);

        // Get all the contactList where bday equals to UPDATED_BDAY
        defaultContactShouldNotBeFound("bday.in=" + UPDATED_BDAY);
    }

    @Test
    @Transactional
    public void getAllContactsByBdayIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where bday is not null
        defaultContactShouldBeFound("bday.specified=true");

        // Get all the contactList where bday is null
        defaultContactShouldNotBeFound("bday.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByMailerIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer equals to DEFAULT_MAILER
        defaultContactShouldBeFound("mailer.equals=" + DEFAULT_MAILER);

        // Get all the contactList where mailer equals to UPDATED_MAILER
        defaultContactShouldNotBeFound("mailer.equals=" + UPDATED_MAILER);
    }

    @Test
    @Transactional
    public void getAllContactsByMailerIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer not equals to DEFAULT_MAILER
        defaultContactShouldNotBeFound("mailer.notEquals=" + DEFAULT_MAILER);

        // Get all the contactList where mailer not equals to UPDATED_MAILER
        defaultContactShouldBeFound("mailer.notEquals=" + UPDATED_MAILER);
    }

    @Test
    @Transactional
    public void getAllContactsByMailerIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer in DEFAULT_MAILER or UPDATED_MAILER
        defaultContactShouldBeFound("mailer.in=" + DEFAULT_MAILER + "," + UPDATED_MAILER);

        // Get all the contactList where mailer equals to UPDATED_MAILER
        defaultContactShouldNotBeFound("mailer.in=" + UPDATED_MAILER);
    }

    @Test
    @Transactional
    public void getAllContactsByMailerIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer is not null
        defaultContactShouldBeFound("mailer.specified=true");

        // Get all the contactList where mailer is null
        defaultContactShouldNotBeFound("mailer.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByMailerContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer contains DEFAULT_MAILER
        defaultContactShouldBeFound("mailer.contains=" + DEFAULT_MAILER);

        // Get all the contactList where mailer contains UPDATED_MAILER
        defaultContactShouldNotBeFound("mailer.contains=" + UPDATED_MAILER);
    }

    @Test
    @Transactional
    public void getAllContactsByMailerNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where mailer does not contain DEFAULT_MAILER
        defaultContactShouldNotBeFound("mailer.doesNotContain=" + DEFAULT_MAILER);

        // Get all the contactList where mailer does not contain UPDATED_MAILER
        defaultContactShouldBeFound("mailer.doesNotContain=" + UPDATED_MAILER);
    }


    @Test
    @Transactional
    public void getAllContactsByTzIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz equals to DEFAULT_TZ
        defaultContactShouldBeFound("tz.equals=" + DEFAULT_TZ);

        // Get all the contactList where tz equals to UPDATED_TZ
        defaultContactShouldNotBeFound("tz.equals=" + UPDATED_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz not equals to DEFAULT_TZ
        defaultContactShouldNotBeFound("tz.notEquals=" + DEFAULT_TZ);

        // Get all the contactList where tz not equals to UPDATED_TZ
        defaultContactShouldBeFound("tz.notEquals=" + UPDATED_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz in DEFAULT_TZ or UPDATED_TZ
        defaultContactShouldBeFound("tz.in=" + DEFAULT_TZ + "," + UPDATED_TZ);

        // Get all the contactList where tz equals to UPDATED_TZ
        defaultContactShouldNotBeFound("tz.in=" + UPDATED_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz is not null
        defaultContactShouldBeFound("tz.specified=true");

        // Get all the contactList where tz is null
        defaultContactShouldNotBeFound("tz.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz is greater than or equal to DEFAULT_TZ
        defaultContactShouldBeFound("tz.greaterThanOrEqual=" + DEFAULT_TZ);

        // Get all the contactList where tz is greater than or equal to UPDATED_TZ
        defaultContactShouldNotBeFound("tz.greaterThanOrEqual=" + UPDATED_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz is less than or equal to DEFAULT_TZ
        defaultContactShouldBeFound("tz.lessThanOrEqual=" + DEFAULT_TZ);

        // Get all the contactList where tz is less than or equal to SMALLER_TZ
        defaultContactShouldNotBeFound("tz.lessThanOrEqual=" + SMALLER_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsLessThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz is less than DEFAULT_TZ
        defaultContactShouldNotBeFound("tz.lessThan=" + DEFAULT_TZ);

        // Get all the contactList where tz is less than UPDATED_TZ
        defaultContactShouldBeFound("tz.lessThan=" + UPDATED_TZ);
    }

    @Test
    @Transactional
    public void getAllContactsByTzIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where tz is greater than DEFAULT_TZ
        defaultContactShouldNotBeFound("tz.greaterThan=" + DEFAULT_TZ);

        // Get all the contactList where tz is greater than SMALLER_TZ
        defaultContactShouldBeFound("tz.greaterThan=" + SMALLER_TZ);
    }


    @Test
    @Transactional
    public void getAllContactsByGeoLatIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat equals to DEFAULT_GEO_LAT
        defaultContactShouldBeFound("geoLat.equals=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat equals to UPDATED_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.equals=" + UPDATED_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat not equals to DEFAULT_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.notEquals=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat not equals to UPDATED_GEO_LAT
        defaultContactShouldBeFound("geoLat.notEquals=" + UPDATED_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat in DEFAULT_GEO_LAT or UPDATED_GEO_LAT
        defaultContactShouldBeFound("geoLat.in=" + DEFAULT_GEO_LAT + "," + UPDATED_GEO_LAT);

        // Get all the contactList where geoLat equals to UPDATED_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.in=" + UPDATED_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat is not null
        defaultContactShouldBeFound("geoLat.specified=true");

        // Get all the contactList where geoLat is null
        defaultContactShouldNotBeFound("geoLat.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat is greater than or equal to DEFAULT_GEO_LAT
        defaultContactShouldBeFound("geoLat.greaterThanOrEqual=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat is greater than or equal to UPDATED_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.greaterThanOrEqual=" + UPDATED_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat is less than or equal to DEFAULT_GEO_LAT
        defaultContactShouldBeFound("geoLat.lessThanOrEqual=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat is less than or equal to SMALLER_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.lessThanOrEqual=" + SMALLER_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsLessThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat is less than DEFAULT_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.lessThan=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat is less than UPDATED_GEO_LAT
        defaultContactShouldBeFound("geoLat.lessThan=" + UPDATED_GEO_LAT);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLatIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLat is greater than DEFAULT_GEO_LAT
        defaultContactShouldNotBeFound("geoLat.greaterThan=" + DEFAULT_GEO_LAT);

        // Get all the contactList where geoLat is greater than SMALLER_GEO_LAT
        defaultContactShouldBeFound("geoLat.greaterThan=" + SMALLER_GEO_LAT);
    }


    @Test
    @Transactional
    public void getAllContactsByGeoLongIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong equals to DEFAULT_GEO_LONG
        defaultContactShouldBeFound("geoLong.equals=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong equals to UPDATED_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.equals=" + UPDATED_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong not equals to DEFAULT_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.notEquals=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong not equals to UPDATED_GEO_LONG
        defaultContactShouldBeFound("geoLong.notEquals=" + UPDATED_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong in DEFAULT_GEO_LONG or UPDATED_GEO_LONG
        defaultContactShouldBeFound("geoLong.in=" + DEFAULT_GEO_LONG + "," + UPDATED_GEO_LONG);

        // Get all the contactList where geoLong equals to UPDATED_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.in=" + UPDATED_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong is not null
        defaultContactShouldBeFound("geoLong.specified=true");

        // Get all the contactList where geoLong is null
        defaultContactShouldNotBeFound("geoLong.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong is greater than or equal to DEFAULT_GEO_LONG
        defaultContactShouldBeFound("geoLong.greaterThanOrEqual=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong is greater than or equal to UPDATED_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.greaterThanOrEqual=" + UPDATED_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong is less than or equal to DEFAULT_GEO_LONG
        defaultContactShouldBeFound("geoLong.lessThanOrEqual=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong is less than or equal to SMALLER_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.lessThanOrEqual=" + SMALLER_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsLessThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong is less than DEFAULT_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.lessThan=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong is less than UPDATED_GEO_LONG
        defaultContactShouldBeFound("geoLong.lessThan=" + UPDATED_GEO_LONG);
    }

    @Test
    @Transactional
    public void getAllContactsByGeoLongIsGreaterThanSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where geoLong is greater than DEFAULT_GEO_LONG
        defaultContactShouldNotBeFound("geoLong.greaterThan=" + DEFAULT_GEO_LONG);

        // Get all the contactList where geoLong is greater than SMALLER_GEO_LONG
        defaultContactShouldBeFound("geoLong.greaterThan=" + SMALLER_GEO_LONG);
    }


    @Test
    @Transactional
    public void getAllContactsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title equals to DEFAULT_TITLE
        defaultContactShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the contactList where title equals to UPDATED_TITLE
        defaultContactShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllContactsByTitleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title not equals to DEFAULT_TITLE
        defaultContactShouldNotBeFound("title.notEquals=" + DEFAULT_TITLE);

        // Get all the contactList where title not equals to UPDATED_TITLE
        defaultContactShouldBeFound("title.notEquals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllContactsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultContactShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the contactList where title equals to UPDATED_TITLE
        defaultContactShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllContactsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title is not null
        defaultContactShouldBeFound("title.specified=true");

        // Get all the contactList where title is null
        defaultContactShouldNotBeFound("title.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByTitleContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title contains DEFAULT_TITLE
        defaultContactShouldBeFound("title.contains=" + DEFAULT_TITLE);

        // Get all the contactList where title contains UPDATED_TITLE
        defaultContactShouldNotBeFound("title.contains=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllContactsByTitleNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where title does not contain DEFAULT_TITLE
        defaultContactShouldNotBeFound("title.doesNotContain=" + DEFAULT_TITLE);

        // Get all the contactList where title does not contain UPDATED_TITLE
        defaultContactShouldBeFound("title.doesNotContain=" + UPDATED_TITLE);
    }


    @Test
    @Transactional
    public void getAllContactsByRoleIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role equals to DEFAULT_ROLE
        defaultContactShouldBeFound("role.equals=" + DEFAULT_ROLE);

        // Get all the contactList where role equals to UPDATED_ROLE
        defaultContactShouldNotBeFound("role.equals=" + UPDATED_ROLE);
    }

    @Test
    @Transactional
    public void getAllContactsByRoleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role not equals to DEFAULT_ROLE
        defaultContactShouldNotBeFound("role.notEquals=" + DEFAULT_ROLE);

        // Get all the contactList where role not equals to UPDATED_ROLE
        defaultContactShouldBeFound("role.notEquals=" + UPDATED_ROLE);
    }

    @Test
    @Transactional
    public void getAllContactsByRoleIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role in DEFAULT_ROLE or UPDATED_ROLE
        defaultContactShouldBeFound("role.in=" + DEFAULT_ROLE + "," + UPDATED_ROLE);

        // Get all the contactList where role equals to UPDATED_ROLE
        defaultContactShouldNotBeFound("role.in=" + UPDATED_ROLE);
    }

    @Test
    @Transactional
    public void getAllContactsByRoleIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role is not null
        defaultContactShouldBeFound("role.specified=true");

        // Get all the contactList where role is null
        defaultContactShouldNotBeFound("role.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByRoleContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role contains DEFAULT_ROLE
        defaultContactShouldBeFound("role.contains=" + DEFAULT_ROLE);

        // Get all the contactList where role contains UPDATED_ROLE
        defaultContactShouldNotBeFound("role.contains=" + UPDATED_ROLE);
    }

    @Test
    @Transactional
    public void getAllContactsByRoleNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where role does not contain DEFAULT_ROLE
        defaultContactShouldNotBeFound("role.doesNotContain=" + DEFAULT_ROLE);

        // Get all the contactList where role does not contain UPDATED_ROLE
        defaultContactShouldBeFound("role.doesNotContain=" + UPDATED_ROLE);
    }


    @Test
    @Transactional
    public void getAllContactsByProdIdIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId equals to DEFAULT_PROD_ID
        defaultContactShouldBeFound("prodId.equals=" + DEFAULT_PROD_ID);

        // Get all the contactList where prodId equals to UPDATED_PROD_ID
        defaultContactShouldNotBeFound("prodId.equals=" + UPDATED_PROD_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByProdIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId not equals to DEFAULT_PROD_ID
        defaultContactShouldNotBeFound("prodId.notEquals=" + DEFAULT_PROD_ID);

        // Get all the contactList where prodId not equals to UPDATED_PROD_ID
        defaultContactShouldBeFound("prodId.notEquals=" + UPDATED_PROD_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByProdIdIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId in DEFAULT_PROD_ID or UPDATED_PROD_ID
        defaultContactShouldBeFound("prodId.in=" + DEFAULT_PROD_ID + "," + UPDATED_PROD_ID);

        // Get all the contactList where prodId equals to UPDATED_PROD_ID
        defaultContactShouldNotBeFound("prodId.in=" + UPDATED_PROD_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByProdIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId is not null
        defaultContactShouldBeFound("prodId.specified=true");

        // Get all the contactList where prodId is null
        defaultContactShouldNotBeFound("prodId.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByProdIdContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId contains DEFAULT_PROD_ID
        defaultContactShouldBeFound("prodId.contains=" + DEFAULT_PROD_ID);

        // Get all the contactList where prodId contains UPDATED_PROD_ID
        defaultContactShouldNotBeFound("prodId.contains=" + UPDATED_PROD_ID);
    }

    @Test
    @Transactional
    public void getAllContactsByProdIdNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where prodId does not contain DEFAULT_PROD_ID
        defaultContactShouldNotBeFound("prodId.doesNotContain=" + DEFAULT_PROD_ID);

        // Get all the contactList where prodId does not contain UPDATED_PROD_ID
        defaultContactShouldBeFound("prodId.doesNotContain=" + UPDATED_PROD_ID);
    }


    @Test
    @Transactional
    public void getAllContactsByRevIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev equals to DEFAULT_REV
        defaultContactShouldBeFound("rev.equals=" + DEFAULT_REV);

        // Get all the contactList where rev equals to UPDATED_REV
        defaultContactShouldNotBeFound("rev.equals=" + UPDATED_REV);
    }

    @Test
    @Transactional
    public void getAllContactsByRevIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev not equals to DEFAULT_REV
        defaultContactShouldNotBeFound("rev.notEquals=" + DEFAULT_REV);

        // Get all the contactList where rev not equals to UPDATED_REV
        defaultContactShouldBeFound("rev.notEquals=" + UPDATED_REV);
    }

    @Test
    @Transactional
    public void getAllContactsByRevIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev in DEFAULT_REV or UPDATED_REV
        defaultContactShouldBeFound("rev.in=" + DEFAULT_REV + "," + UPDATED_REV);

        // Get all the contactList where rev equals to UPDATED_REV
        defaultContactShouldNotBeFound("rev.in=" + UPDATED_REV);
    }

    @Test
    @Transactional
    public void getAllContactsByRevIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev is not null
        defaultContactShouldBeFound("rev.specified=true");

        // Get all the contactList where rev is null
        defaultContactShouldNotBeFound("rev.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByRevContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev contains DEFAULT_REV
        defaultContactShouldBeFound("rev.contains=" + DEFAULT_REV);

        // Get all the contactList where rev contains UPDATED_REV
        defaultContactShouldNotBeFound("rev.contains=" + UPDATED_REV);
    }

    @Test
    @Transactional
    public void getAllContactsByRevNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where rev does not contain DEFAULT_REV
        defaultContactShouldNotBeFound("rev.doesNotContain=" + DEFAULT_REV);

        // Get all the contactList where rev does not contain UPDATED_REV
        defaultContactShouldBeFound("rev.doesNotContain=" + UPDATED_REV);
    }


    @Test
    @Transactional
    public void getAllContactsBySortStringIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString equals to DEFAULT_SORT_STRING
        defaultContactShouldBeFound("sortString.equals=" + DEFAULT_SORT_STRING);

        // Get all the contactList where sortString equals to UPDATED_SORT_STRING
        defaultContactShouldNotBeFound("sortString.equals=" + UPDATED_SORT_STRING);
    }

    @Test
    @Transactional
    public void getAllContactsBySortStringIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString not equals to DEFAULT_SORT_STRING
        defaultContactShouldNotBeFound("sortString.notEquals=" + DEFAULT_SORT_STRING);

        // Get all the contactList where sortString not equals to UPDATED_SORT_STRING
        defaultContactShouldBeFound("sortString.notEquals=" + UPDATED_SORT_STRING);
    }

    @Test
    @Transactional
    public void getAllContactsBySortStringIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString in DEFAULT_SORT_STRING or UPDATED_SORT_STRING
        defaultContactShouldBeFound("sortString.in=" + DEFAULT_SORT_STRING + "," + UPDATED_SORT_STRING);

        // Get all the contactList where sortString equals to UPDATED_SORT_STRING
        defaultContactShouldNotBeFound("sortString.in=" + UPDATED_SORT_STRING);
    }

    @Test
    @Transactional
    public void getAllContactsBySortStringIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString is not null
        defaultContactShouldBeFound("sortString.specified=true");

        // Get all the contactList where sortString is null
        defaultContactShouldNotBeFound("sortString.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsBySortStringContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString contains DEFAULT_SORT_STRING
        defaultContactShouldBeFound("sortString.contains=" + DEFAULT_SORT_STRING);

        // Get all the contactList where sortString contains UPDATED_SORT_STRING
        defaultContactShouldNotBeFound("sortString.contains=" + UPDATED_SORT_STRING);
    }

    @Test
    @Transactional
    public void getAllContactsBySortStringNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where sortString does not contain DEFAULT_SORT_STRING
        defaultContactShouldNotBeFound("sortString.doesNotContain=" + DEFAULT_SORT_STRING);

        // Get all the contactList where sortString does not contain UPDATED_SORT_STRING
        defaultContactShouldBeFound("sortString.doesNotContain=" + UPDATED_SORT_STRING);
    }


    @Test
    @Transactional
    public void getAllContactsByUidIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid equals to DEFAULT_UID
        defaultContactShouldBeFound("uid.equals=" + DEFAULT_UID);

        // Get all the contactList where uid equals to UPDATED_UID
        defaultContactShouldNotBeFound("uid.equals=" + UPDATED_UID);
    }

    @Test
    @Transactional
    public void getAllContactsByUidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid not equals to DEFAULT_UID
        defaultContactShouldNotBeFound("uid.notEquals=" + DEFAULT_UID);

        // Get all the contactList where uid not equals to UPDATED_UID
        defaultContactShouldBeFound("uid.notEquals=" + UPDATED_UID);
    }

    @Test
    @Transactional
    public void getAllContactsByUidIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid in DEFAULT_UID or UPDATED_UID
        defaultContactShouldBeFound("uid.in=" + DEFAULT_UID + "," + UPDATED_UID);

        // Get all the contactList where uid equals to UPDATED_UID
        defaultContactShouldNotBeFound("uid.in=" + UPDATED_UID);
    }

    @Test
    @Transactional
    public void getAllContactsByUidIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid is not null
        defaultContactShouldBeFound("uid.specified=true");

        // Get all the contactList where uid is null
        defaultContactShouldNotBeFound("uid.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByUidContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid contains DEFAULT_UID
        defaultContactShouldBeFound("uid.contains=" + DEFAULT_UID);

        // Get all the contactList where uid contains UPDATED_UID
        defaultContactShouldNotBeFound("uid.contains=" + UPDATED_UID);
    }

    @Test
    @Transactional
    public void getAllContactsByUidNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where uid does not contain DEFAULT_UID
        defaultContactShouldNotBeFound("uid.doesNotContain=" + DEFAULT_UID);

        // Get all the contactList where uid does not contain UPDATED_UID
        defaultContactShouldBeFound("uid.doesNotContain=" + UPDATED_UID);
    }


    @Test
    @Transactional
    public void getAllContactsByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url equals to DEFAULT_URL
        defaultContactShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the contactList where url equals to UPDATED_URL
        defaultContactShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByUrlIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url not equals to DEFAULT_URL
        defaultContactShouldNotBeFound("url.notEquals=" + DEFAULT_URL);

        // Get all the contactList where url not equals to UPDATED_URL
        defaultContactShouldBeFound("url.notEquals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url in DEFAULT_URL or UPDATED_URL
        defaultContactShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the contactList where url equals to UPDATED_URL
        defaultContactShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url is not null
        defaultContactShouldBeFound("url.specified=true");

        // Get all the contactList where url is null
        defaultContactShouldNotBeFound("url.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByUrlContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url contains DEFAULT_URL
        defaultContactShouldBeFound("url.contains=" + DEFAULT_URL);

        // Get all the contactList where url contains UPDATED_URL
        defaultContactShouldNotBeFound("url.contains=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByUrlNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where url does not contain DEFAULT_URL
        defaultContactShouldNotBeFound("url.doesNotContain=" + DEFAULT_URL);

        // Get all the contactList where url does not contain UPDATED_URL
        defaultContactShouldBeFound("url.doesNotContain=" + UPDATED_URL);
    }


    @Test
    @Transactional
    public void getAllContactsByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version equals to DEFAULT_VERSION
        defaultContactShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the contactList where version equals to UPDATED_VERSION
        defaultContactShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllContactsByVersionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version not equals to DEFAULT_VERSION
        defaultContactShouldNotBeFound("version.notEquals=" + DEFAULT_VERSION);

        // Get all the contactList where version not equals to UPDATED_VERSION
        defaultContactShouldBeFound("version.notEquals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllContactsByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultContactShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the contactList where version equals to UPDATED_VERSION
        defaultContactShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllContactsByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version is not null
        defaultContactShouldBeFound("version.specified=true");

        // Get all the contactList where version is null
        defaultContactShouldNotBeFound("version.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByVersionContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version contains DEFAULT_VERSION
        defaultContactShouldBeFound("version.contains=" + DEFAULT_VERSION);

        // Get all the contactList where version contains UPDATED_VERSION
        defaultContactShouldNotBeFound("version.contains=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllContactsByVersionNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where version does not contain DEFAULT_VERSION
        defaultContactShouldNotBeFound("version.doesNotContain=" + DEFAULT_VERSION);

        // Get all the contactList where version does not contain UPDATED_VERSION
        defaultContactShouldBeFound("version.doesNotContain=" + UPDATED_VERSION);
    }


    @Test
    @Transactional
    public void getAllContactsByClassNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className equals to DEFAULT_CLASS_NAME
        defaultContactShouldBeFound("className.equals=" + DEFAULT_CLASS_NAME);

        // Get all the contactList where className equals to UPDATED_CLASS_NAME
        defaultContactShouldNotBeFound("className.equals=" + UPDATED_CLASS_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByClassNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className not equals to DEFAULT_CLASS_NAME
        defaultContactShouldNotBeFound("className.notEquals=" + DEFAULT_CLASS_NAME);

        // Get all the contactList where className not equals to UPDATED_CLASS_NAME
        defaultContactShouldBeFound("className.notEquals=" + UPDATED_CLASS_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByClassNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className in DEFAULT_CLASS_NAME or UPDATED_CLASS_NAME
        defaultContactShouldBeFound("className.in=" + DEFAULT_CLASS_NAME + "," + UPDATED_CLASS_NAME);

        // Get all the contactList where className equals to UPDATED_CLASS_NAME
        defaultContactShouldNotBeFound("className.in=" + UPDATED_CLASS_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByClassNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className is not null
        defaultContactShouldBeFound("className.specified=true");

        // Get all the contactList where className is null
        defaultContactShouldNotBeFound("className.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactsByClassNameContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className contains DEFAULT_CLASS_NAME
        defaultContactShouldBeFound("className.contains=" + DEFAULT_CLASS_NAME);

        // Get all the contactList where className contains UPDATED_CLASS_NAME
        defaultContactShouldNotBeFound("className.contains=" + UPDATED_CLASS_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByClassNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where className does not contain DEFAULT_CLASS_NAME
        defaultContactShouldNotBeFound("className.doesNotContain=" + DEFAULT_CLASS_NAME);

        // Get all the contactList where className does not contain UPDATED_CLASS_NAME
        defaultContactShouldBeFound("className.doesNotContain=" + UPDATED_CLASS_NAME);
    }


    @Test
    @Transactional
    public void getAllContactsByContactAgentIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactAgent contactAgent = ContactAgentResourceIT.createEntity(em);
        em.persist(contactAgent);
        em.flush();
        contact.addContactAgent(contactAgent);
        contactRepository.saveAndFlush(contact);
        Long contactAgentId = contactAgent.getId();

        // Get all the contactList where contactAgent equals to contactAgentId
        defaultContactShouldBeFound("contactAgentId.equals=" + contactAgentId);

        // Get all the contactList where contactAgent equals to contactAgentId + 1
        defaultContactShouldNotBeFound("contactAgentId.equals=" + (contactAgentId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactCategoriesIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactCategories contactCategories = ContactCategoriesResourceIT.createEntity(em);
        em.persist(contactCategories);
        em.flush();
        contact.addContactCategories(contactCategories);
        contactRepository.saveAndFlush(contact);
        Long contactCategoriesId = contactCategories.getId();

        // Get all the contactList where contactCategories equals to contactCategoriesId
        defaultContactShouldBeFound("contactCategoriesId.equals=" + contactCategoriesId);

        // Get all the contactList where contactCategories equals to contactCategoriesId + 1
        defaultContactShouldNotBeFound("contactCategoriesId.equals=" + (contactCategoriesId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactDataIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactData contactData = ContactDataResourceIT.createEntity(em);
        em.persist(contactData);
        em.flush();
        contact.addContactData(contactData);
        contactRepository.saveAndFlush(contact);
        Long contactDataId = contactData.getId();

        // Get all the contactList where contactData equals to contactDataId
        defaultContactShouldBeFound("contactDataId.equals=" + contactDataId);

        // Get all the contactList where contactData equals to contactDataId + 1
        defaultContactShouldNotBeFound("contactDataId.equals=" + (contactDataId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactEmail contactEmail = ContactEmailResourceIT.createEntity(em);
        em.persist(contactEmail);
        em.flush();
        contact.addContactEmail(contactEmail);
        contactRepository.saveAndFlush(contact);
        Long contactEmailId = contactEmail.getId();

        // Get all the contactList where contactEmail equals to contactEmailId
        defaultContactShouldBeFound("contactEmailId.equals=" + contactEmailId);

        // Get all the contactList where contactEmail equals to contactEmailId + 1
        defaultContactShouldNotBeFound("contactEmailId.equals=" + (contactEmailId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactKeysIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactKeys contactKeys = ContactKeysResourceIT.createEntity(em);
        em.persist(contactKeys);
        em.flush();
        contact.addContactKeys(contactKeys);
        contactRepository.saveAndFlush(contact);
        Long contactKeysId = contactKeys.getId();

        // Get all the contactList where contactKeys equals to contactKeysId
        defaultContactShouldBeFound("contactKeysId.equals=" + contactKeysId);

        // Get all the contactList where contactKeys equals to contactKeysId + 1
        defaultContactShouldNotBeFound("contactKeysId.equals=" + (contactKeysId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactMailAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactMailAddress contactMailAddress = ContactMailAddressResourceIT.createEntity(em);
        em.persist(contactMailAddress);
        em.flush();
        contact.addContactMailAddress(contactMailAddress);
        contactRepository.saveAndFlush(contact);
        Long contactMailAddressId = contactMailAddress.getId();

        // Get all the contactList where contactMailAddress equals to contactMailAddressId
        defaultContactShouldBeFound("contactMailAddressId.equals=" + contactMailAddressId);

        // Get all the contactList where contactMailAddress equals to contactMailAddressId + 1
        defaultContactShouldNotBeFound("contactMailAddressId.equals=" + (contactMailAddressId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactNote contactNote = ContactNoteResourceIT.createEntity(em);
        em.persist(contactNote);
        em.flush();
        contact.addContactNote(contactNote);
        contactRepository.saveAndFlush(contact);
        Long contactNoteId = contactNote.getId();

        // Get all the contactList where contactNote equals to contactNoteId
        defaultContactShouldBeFound("contactNoteId.equals=" + contactNoteId);

        // Get all the contactList where contactNote equals to contactNoteId + 1
        defaultContactShouldNotBeFound("contactNoteId.equals=" + (contactNoteId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactPhoneNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactPhoneNumber contactPhoneNumber = ContactPhoneNumberResourceIT.createEntity(em);
        em.persist(contactPhoneNumber);
        em.flush();
        contact.addContactPhoneNumber(contactPhoneNumber);
        contactRepository.saveAndFlush(contact);
        Long contactPhoneNumberId = contactPhoneNumber.getId();

        // Get all the contactList where contactPhoneNumber equals to contactPhoneNumberId
        defaultContactShouldBeFound("contactPhoneNumberId.equals=" + contactPhoneNumberId);

        // Get all the contactList where contactPhoneNumber equals to contactPhoneNumberId + 1
        defaultContactShouldNotBeFound("contactPhoneNumberId.equals=" + (contactPhoneNumberId + 1));
    }


    @Test
    @Transactional
    public void getAllContactsByContactXtendedIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);
        ContactXtended contactXtended = ContactXtendedResourceIT.createEntity(em);
        em.persist(contactXtended);
        em.flush();
        contact.addContactXtended(contactXtended);
        contactRepository.saveAndFlush(contact);
        Long contactXtendedId = contactXtended.getId();

        // Get all the contactList where contactXtended equals to contactXtendedId
        defaultContactShouldBeFound("contactXtendedId.equals=" + contactXtendedId);

        // Get all the contactList where contactXtended equals to contactXtendedId + 1
        defaultContactShouldNotBeFound("contactXtendedId.equals=" + (contactXtendedId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactShouldBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactId").value(hasItem(DEFAULT_CONTACT_ID)))
            .andExpect(jsonPath("$.[*].fn").value(hasItem(DEFAULT_FN)))
            .andExpect(jsonPath("$.[*].n").value(hasItem(DEFAULT_N)))
            .andExpect(jsonPath("$.[*].nickname").value(hasItem(DEFAULT_NICKNAME)))
            .andExpect(jsonPath("$.[*].bday").value(hasItem(DEFAULT_BDAY.toString())))
            .andExpect(jsonPath("$.[*].mailer").value(hasItem(DEFAULT_MAILER)))
            .andExpect(jsonPath("$.[*].tz").value(hasItem(DEFAULT_TZ)))
            .andExpect(jsonPath("$.[*].geoLat").value(hasItem(DEFAULT_GEO_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].geoLong").value(hasItem(DEFAULT_GEO_LONG.doubleValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE)))
            .andExpect(jsonPath("$.[*].prodId").value(hasItem(DEFAULT_PROD_ID)))
            .andExpect(jsonPath("$.[*].rev").value(hasItem(DEFAULT_REV)))
            .andExpect(jsonPath("$.[*].sortString").value(hasItem(DEFAULT_SORT_STRING)))
            .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].className").value(hasItem(DEFAULT_CLASS_NAME)));

        // Check, that the count call also returns 1
        restContactMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactShouldNotBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactMockMvc.perform(get("/api/contacts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContact() throws Exception {
        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Update the contact
        Contact updatedContact = contactRepository.findById(contact.getId()).get();
        // Disconnect from session so that the updates on updatedContact are not directly saved in db
        em.detach(updatedContact);
        updatedContact
            .contactId(UPDATED_CONTACT_ID)
            .fn(UPDATED_FN)
            .n(UPDATED_N)
            .nickname(UPDATED_NICKNAME)
            .bday(UPDATED_BDAY)
            .mailer(UPDATED_MAILER)
            .tz(UPDATED_TZ)
            .geoLat(UPDATED_GEO_LAT)
            .geoLong(UPDATED_GEO_LONG)
            .title(UPDATED_TITLE)
            .role(UPDATED_ROLE)
            .prodId(UPDATED_PROD_ID)
            .rev(UPDATED_REV)
            .sortString(UPDATED_SORT_STRING)
            .uid(UPDATED_UID)
            .url(UPDATED_URL)
            .version(UPDATED_VERSION)
            .className(UPDATED_CLASS_NAME);
        ContactDTO contactDTO = contactMapper.toDto(updatedContact);

        restContactMockMvc.perform(put("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isOk());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getContactId()).isEqualTo(UPDATED_CONTACT_ID);
        assertThat(testContact.getFn()).isEqualTo(UPDATED_FN);
        assertThat(testContact.getN()).isEqualTo(UPDATED_N);
        assertThat(testContact.getNickname()).isEqualTo(UPDATED_NICKNAME);
        assertThat(testContact.getBday()).isEqualTo(UPDATED_BDAY);
        assertThat(testContact.getMailer()).isEqualTo(UPDATED_MAILER);
        assertThat(testContact.getTz()).isEqualTo(UPDATED_TZ);
        assertThat(testContact.getGeoLat()).isEqualTo(UPDATED_GEO_LAT);
        assertThat(testContact.getGeoLong()).isEqualTo(UPDATED_GEO_LONG);
        assertThat(testContact.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testContact.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testContact.getProdId()).isEqualTo(UPDATED_PROD_ID);
        assertThat(testContact.getRev()).isEqualTo(UPDATED_REV);
        assertThat(testContact.getSortString()).isEqualTo(UPDATED_SORT_STRING);
        assertThat(testContact.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testContact.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testContact.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testContact.getClassName()).isEqualTo(UPDATED_CLASS_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingContact() throws Exception {
        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Create the Contact
        ContactDTO contactDTO = contactMapper.toDto(contact);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactMockMvc.perform(put("/api/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        int databaseSizeBeforeDelete = contactRepository.findAll().size();

        // Delete the contact
        restContactMockMvc.perform(delete("/api/contacts/{id}", contact.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

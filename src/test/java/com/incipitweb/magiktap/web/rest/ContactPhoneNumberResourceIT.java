package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactPhoneNumber;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactPhoneNumberRepository;
import com.incipitweb.magiktap.service.ContactPhoneNumberService;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberDTO;
import com.incipitweb.magiktap.service.mapper.ContactPhoneNumberMapper;
import com.incipitweb.magiktap.service.dto.ContactPhoneNumberCriteria;
import com.incipitweb.magiktap.service.ContactPhoneNumberQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactPhoneNumberResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactPhoneNumberResourceIT {

    private static final String DEFAULT_LOCAL_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_LOCAL_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_NUMBER_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_NUMBER_TYPE = "BBBBBBBBBB";

    @Autowired
    private ContactPhoneNumberRepository contactPhoneNumberRepository;

    @Autowired
    private ContactPhoneNumberMapper contactPhoneNumberMapper;

    @Autowired
    private ContactPhoneNumberService contactPhoneNumberService;

    @Autowired
    private ContactPhoneNumberQueryService contactPhoneNumberQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactPhoneNumberMockMvc;

    private ContactPhoneNumber contactPhoneNumber;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactPhoneNumber createEntity(EntityManager em) {
        ContactPhoneNumber contactPhoneNumber = new ContactPhoneNumber()
            .localNumber(DEFAULT_LOCAL_NUMBER)
            .phoneNumberType(DEFAULT_PHONE_NUMBER_TYPE);
        return contactPhoneNumber;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactPhoneNumber createUpdatedEntity(EntityManager em) {
        ContactPhoneNumber contactPhoneNumber = new ContactPhoneNumber()
            .localNumber(UPDATED_LOCAL_NUMBER)
            .phoneNumberType(UPDATED_PHONE_NUMBER_TYPE);
        return contactPhoneNumber;
    }

    @BeforeEach
    public void initTest() {
        contactPhoneNumber = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactPhoneNumber() throws Exception {
        int databaseSizeBeforeCreate = contactPhoneNumberRepository.findAll().size();
        // Create the ContactPhoneNumber
        ContactPhoneNumberDTO contactPhoneNumberDTO = contactPhoneNumberMapper.toDto(contactPhoneNumber);
        restContactPhoneNumberMockMvc.perform(post("/api/contact-phone-numbers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactPhoneNumberDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactPhoneNumber in the database
        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeCreate + 1);
        ContactPhoneNumber testContactPhoneNumber = contactPhoneNumberList.get(contactPhoneNumberList.size() - 1);
        assertThat(testContactPhoneNumber.getLocalNumber()).isEqualTo(DEFAULT_LOCAL_NUMBER);
        assertThat(testContactPhoneNumber.getPhoneNumberType()).isEqualTo(DEFAULT_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void createContactPhoneNumberWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactPhoneNumberRepository.findAll().size();

        // Create the ContactPhoneNumber with an existing ID
        contactPhoneNumber.setId(1L);
        ContactPhoneNumberDTO contactPhoneNumberDTO = contactPhoneNumberMapper.toDto(contactPhoneNumber);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactPhoneNumberMockMvc.perform(post("/api/contact-phone-numbers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactPhoneNumberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactPhoneNumber in the database
        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkLocalNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactPhoneNumberRepository.findAll().size();
        // set the field null
        contactPhoneNumber.setLocalNumber(null);

        // Create the ContactPhoneNumber, which fails.
        ContactPhoneNumberDTO contactPhoneNumberDTO = contactPhoneNumberMapper.toDto(contactPhoneNumber);


        restContactPhoneNumberMockMvc.perform(post("/api/contact-phone-numbers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactPhoneNumberDTO)))
            .andExpect(status().isBadRequest());

        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbers() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactPhoneNumber.getId().intValue())))
            .andExpect(jsonPath("$.[*].localNumber").value(hasItem(DEFAULT_LOCAL_NUMBER)))
            .andExpect(jsonPath("$.[*].phoneNumberType").value(hasItem(DEFAULT_PHONE_NUMBER_TYPE)));
    }
    
    @Test
    @Transactional
    public void getContactPhoneNumber() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get the contactPhoneNumber
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers/{id}", contactPhoneNumber.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactPhoneNumber.getId().intValue()))
            .andExpect(jsonPath("$.localNumber").value(DEFAULT_LOCAL_NUMBER))
            .andExpect(jsonPath("$.phoneNumberType").value(DEFAULT_PHONE_NUMBER_TYPE));
    }


    @Test
    @Transactional
    public void getContactPhoneNumbersByIdFiltering() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        Long id = contactPhoneNumber.getId();

        defaultContactPhoneNumberShouldBeFound("id.equals=" + id);
        defaultContactPhoneNumberShouldNotBeFound("id.notEquals=" + id);

        defaultContactPhoneNumberShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactPhoneNumberShouldNotBeFound("id.greaterThan=" + id);

        defaultContactPhoneNumberShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactPhoneNumberShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber equals to DEFAULT_LOCAL_NUMBER
        defaultContactPhoneNumberShouldBeFound("localNumber.equals=" + DEFAULT_LOCAL_NUMBER);

        // Get all the contactPhoneNumberList where localNumber equals to UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldNotBeFound("localNumber.equals=" + UPDATED_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber not equals to DEFAULT_LOCAL_NUMBER
        defaultContactPhoneNumberShouldNotBeFound("localNumber.notEquals=" + DEFAULT_LOCAL_NUMBER);

        // Get all the contactPhoneNumberList where localNumber not equals to UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldBeFound("localNumber.notEquals=" + UPDATED_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberIsInShouldWork() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber in DEFAULT_LOCAL_NUMBER or UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldBeFound("localNumber.in=" + DEFAULT_LOCAL_NUMBER + "," + UPDATED_LOCAL_NUMBER);

        // Get all the contactPhoneNumberList where localNumber equals to UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldNotBeFound("localNumber.in=" + UPDATED_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber is not null
        defaultContactPhoneNumberShouldBeFound("localNumber.specified=true");

        // Get all the contactPhoneNumberList where localNumber is null
        defaultContactPhoneNumberShouldNotBeFound("localNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberContainsSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber contains DEFAULT_LOCAL_NUMBER
        defaultContactPhoneNumberShouldBeFound("localNumber.contains=" + DEFAULT_LOCAL_NUMBER);

        // Get all the contactPhoneNumberList where localNumber contains UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldNotBeFound("localNumber.contains=" + UPDATED_LOCAL_NUMBER);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByLocalNumberNotContainsSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where localNumber does not contain DEFAULT_LOCAL_NUMBER
        defaultContactPhoneNumberShouldNotBeFound("localNumber.doesNotContain=" + DEFAULT_LOCAL_NUMBER);

        // Get all the contactPhoneNumberList where localNumber does not contain UPDATED_LOCAL_NUMBER
        defaultContactPhoneNumberShouldBeFound("localNumber.doesNotContain=" + UPDATED_LOCAL_NUMBER);
    }


    @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType equals to DEFAULT_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.equals=" + DEFAULT_PHONE_NUMBER_TYPE);

        // Get all the contactPhoneNumberList where phoneNumberType equals to UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.equals=" + UPDATED_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType not equals to DEFAULT_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.notEquals=" + DEFAULT_PHONE_NUMBER_TYPE);

        // Get all the contactPhoneNumberList where phoneNumberType not equals to UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.notEquals=" + UPDATED_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType in DEFAULT_PHONE_NUMBER_TYPE or UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.in=" + DEFAULT_PHONE_NUMBER_TYPE + "," + UPDATED_PHONE_NUMBER_TYPE);

        // Get all the contactPhoneNumberList where phoneNumberType equals to UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.in=" + UPDATED_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType is not null
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.specified=true");

        // Get all the contactPhoneNumberList where phoneNumberType is null
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeContainsSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType contains DEFAULT_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.contains=" + DEFAULT_PHONE_NUMBER_TYPE);

        // Get all the contactPhoneNumberList where phoneNumberType contains UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.contains=" + UPDATED_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactPhoneNumbersByPhoneNumberTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        // Get all the contactPhoneNumberList where phoneNumberType does not contain DEFAULT_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldNotBeFound("phoneNumberType.doesNotContain=" + DEFAULT_PHONE_NUMBER_TYPE);

        // Get all the contactPhoneNumberList where phoneNumberType does not contain UPDATED_PHONE_NUMBER_TYPE
        defaultContactPhoneNumberShouldBeFound("phoneNumberType.doesNotContain=" + UPDATED_PHONE_NUMBER_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactPhoneNumbersByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactPhoneNumber.setContact(contact);
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);
        Long contactId = contact.getId();

        // Get all the contactPhoneNumberList where contact equals to contactId
        defaultContactPhoneNumberShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactPhoneNumberList where contact equals to contactId + 1
        defaultContactPhoneNumberShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactPhoneNumberShouldBeFound(String filter) throws Exception {
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactPhoneNumber.getId().intValue())))
            .andExpect(jsonPath("$.[*].localNumber").value(hasItem(DEFAULT_LOCAL_NUMBER)))
            .andExpect(jsonPath("$.[*].phoneNumberType").value(hasItem(DEFAULT_PHONE_NUMBER_TYPE)));

        // Check, that the count call also returns 1
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactPhoneNumberShouldNotBeFound(String filter) throws Exception {
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactPhoneNumber() throws Exception {
        // Get the contactPhoneNumber
        restContactPhoneNumberMockMvc.perform(get("/api/contact-phone-numbers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactPhoneNumber() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        int databaseSizeBeforeUpdate = contactPhoneNumberRepository.findAll().size();

        // Update the contactPhoneNumber
        ContactPhoneNumber updatedContactPhoneNumber = contactPhoneNumberRepository.findById(contactPhoneNumber.getId()).get();
        // Disconnect from session so that the updates on updatedContactPhoneNumber are not directly saved in db
        em.detach(updatedContactPhoneNumber);
        updatedContactPhoneNumber
            .localNumber(UPDATED_LOCAL_NUMBER)
            .phoneNumberType(UPDATED_PHONE_NUMBER_TYPE);
        ContactPhoneNumberDTO contactPhoneNumberDTO = contactPhoneNumberMapper.toDto(updatedContactPhoneNumber);

        restContactPhoneNumberMockMvc.perform(put("/api/contact-phone-numbers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactPhoneNumberDTO)))
            .andExpect(status().isOk());

        // Validate the ContactPhoneNumber in the database
        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeUpdate);
        ContactPhoneNumber testContactPhoneNumber = contactPhoneNumberList.get(contactPhoneNumberList.size() - 1);
        assertThat(testContactPhoneNumber.getLocalNumber()).isEqualTo(UPDATED_LOCAL_NUMBER);
        assertThat(testContactPhoneNumber.getPhoneNumberType()).isEqualTo(UPDATED_PHONE_NUMBER_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactPhoneNumber() throws Exception {
        int databaseSizeBeforeUpdate = contactPhoneNumberRepository.findAll().size();

        // Create the ContactPhoneNumber
        ContactPhoneNumberDTO contactPhoneNumberDTO = contactPhoneNumberMapper.toDto(contactPhoneNumber);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactPhoneNumberMockMvc.perform(put("/api/contact-phone-numbers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactPhoneNumberDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactPhoneNumber in the database
        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactPhoneNumber() throws Exception {
        // Initialize the database
        contactPhoneNumberRepository.saveAndFlush(contactPhoneNumber);

        int databaseSizeBeforeDelete = contactPhoneNumberRepository.findAll().size();

        // Delete the contactPhoneNumber
        restContactPhoneNumberMockMvc.perform(delete("/api/contact-phone-numbers/{id}", contactPhoneNumber.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactPhoneNumber> contactPhoneNumberList = contactPhoneNumberRepository.findAll();
        assertThat(contactPhoneNumberList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactNote;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactNoteRepository;
import com.incipitweb.magiktap.service.ContactNoteService;
import com.incipitweb.magiktap.service.dto.ContactNoteDTO;
import com.incipitweb.magiktap.service.mapper.ContactNoteMapper;
import com.incipitweb.magiktap.service.dto.ContactNoteCriteria;
import com.incipitweb.magiktap.service.ContactNoteQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactNoteResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactNoteResourceIT {

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_NOTE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_NOTE_TYPE = "BBBBBBBBBB";

    @Autowired
    private ContactNoteRepository contactNoteRepository;

    @Autowired
    private ContactNoteMapper contactNoteMapper;

    @Autowired
    private ContactNoteService contactNoteService;

    @Autowired
    private ContactNoteQueryService contactNoteQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactNoteMockMvc;

    private ContactNote contactNote;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactNote createEntity(EntityManager em) {
        ContactNote contactNote = new ContactNote()
            .note(DEFAULT_NOTE)
            .contactNoteType(DEFAULT_CONTACT_NOTE_TYPE);
        return contactNote;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactNote createUpdatedEntity(EntityManager em) {
        ContactNote contactNote = new ContactNote()
            .note(UPDATED_NOTE)
            .contactNoteType(UPDATED_CONTACT_NOTE_TYPE);
        return contactNote;
    }

    @BeforeEach
    public void initTest() {
        contactNote = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactNote() throws Exception {
        int databaseSizeBeforeCreate = contactNoteRepository.findAll().size();
        // Create the ContactNote
        ContactNoteDTO contactNoteDTO = contactNoteMapper.toDto(contactNote);
        restContactNoteMockMvc.perform(post("/api/contact-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactNoteDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactNote in the database
        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeCreate + 1);
        ContactNote testContactNote = contactNoteList.get(contactNoteList.size() - 1);
        assertThat(testContactNote.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testContactNote.getContactNoteType()).isEqualTo(DEFAULT_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void createContactNoteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactNoteRepository.findAll().size();

        // Create the ContactNote with an existing ID
        contactNote.setId(1L);
        ContactNoteDTO contactNoteDTO = contactNoteMapper.toDto(contactNote);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactNoteMockMvc.perform(post("/api/contact-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactNoteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactNote in the database
        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNoteIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactNoteRepository.findAll().size();
        // set the field null
        contactNote.setNote(null);

        // Create the ContactNote, which fails.
        ContactNoteDTO contactNoteDTO = contactNoteMapper.toDto(contactNote);


        restContactNoteMockMvc.perform(post("/api/contact-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactNoteDTO)))
            .andExpect(status().isBadRequest());

        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactNotes() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList
        restContactNoteMockMvc.perform(get("/api/contact-notes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactNote.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].contactNoteType").value(hasItem(DEFAULT_CONTACT_NOTE_TYPE)));
    }
    
    @Test
    @Transactional
    public void getContactNote() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get the contactNote
        restContactNoteMockMvc.perform(get("/api/contact-notes/{id}", contactNote.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactNote.getId().intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.contactNoteType").value(DEFAULT_CONTACT_NOTE_TYPE));
    }


    @Test
    @Transactional
    public void getContactNotesByIdFiltering() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        Long id = contactNote.getId();

        defaultContactNoteShouldBeFound("id.equals=" + id);
        defaultContactNoteShouldNotBeFound("id.notEquals=" + id);

        defaultContactNoteShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactNoteShouldNotBeFound("id.greaterThan=" + id);

        defaultContactNoteShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactNoteShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactNotesByNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note equals to DEFAULT_NOTE
        defaultContactNoteShouldBeFound("note.equals=" + DEFAULT_NOTE);

        // Get all the contactNoteList where note equals to UPDATED_NOTE
        defaultContactNoteShouldNotBeFound("note.equals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByNoteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note not equals to DEFAULT_NOTE
        defaultContactNoteShouldNotBeFound("note.notEquals=" + DEFAULT_NOTE);

        // Get all the contactNoteList where note not equals to UPDATED_NOTE
        defaultContactNoteShouldBeFound("note.notEquals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByNoteIsInShouldWork() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note in DEFAULT_NOTE or UPDATED_NOTE
        defaultContactNoteShouldBeFound("note.in=" + DEFAULT_NOTE + "," + UPDATED_NOTE);

        // Get all the contactNoteList where note equals to UPDATED_NOTE
        defaultContactNoteShouldNotBeFound("note.in=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note is not null
        defaultContactNoteShouldBeFound("note.specified=true");

        // Get all the contactNoteList where note is null
        defaultContactNoteShouldNotBeFound("note.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactNotesByNoteContainsSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note contains DEFAULT_NOTE
        defaultContactNoteShouldBeFound("note.contains=" + DEFAULT_NOTE);

        // Get all the contactNoteList where note contains UPDATED_NOTE
        defaultContactNoteShouldNotBeFound("note.contains=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByNoteNotContainsSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where note does not contain DEFAULT_NOTE
        defaultContactNoteShouldNotBeFound("note.doesNotContain=" + DEFAULT_NOTE);

        // Get all the contactNoteList where note does not contain UPDATED_NOTE
        defaultContactNoteShouldBeFound("note.doesNotContain=" + UPDATED_NOTE);
    }


    @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType equals to DEFAULT_CONTACT_NOTE_TYPE
        defaultContactNoteShouldBeFound("contactNoteType.equals=" + DEFAULT_CONTACT_NOTE_TYPE);

        // Get all the contactNoteList where contactNoteType equals to UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldNotBeFound("contactNoteType.equals=" + UPDATED_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType not equals to DEFAULT_CONTACT_NOTE_TYPE
        defaultContactNoteShouldNotBeFound("contactNoteType.notEquals=" + DEFAULT_CONTACT_NOTE_TYPE);

        // Get all the contactNoteList where contactNoteType not equals to UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldBeFound("contactNoteType.notEquals=" + UPDATED_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeIsInShouldWork() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType in DEFAULT_CONTACT_NOTE_TYPE or UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldBeFound("contactNoteType.in=" + DEFAULT_CONTACT_NOTE_TYPE + "," + UPDATED_CONTACT_NOTE_TYPE);

        // Get all the contactNoteList where contactNoteType equals to UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldNotBeFound("contactNoteType.in=" + UPDATED_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType is not null
        defaultContactNoteShouldBeFound("contactNoteType.specified=true");

        // Get all the contactNoteList where contactNoteType is null
        defaultContactNoteShouldNotBeFound("contactNoteType.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeContainsSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType contains DEFAULT_CONTACT_NOTE_TYPE
        defaultContactNoteShouldBeFound("contactNoteType.contains=" + DEFAULT_CONTACT_NOTE_TYPE);

        // Get all the contactNoteList where contactNoteType contains UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldNotBeFound("contactNoteType.contains=" + UPDATED_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void getAllContactNotesByContactNoteTypeNotContainsSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        // Get all the contactNoteList where contactNoteType does not contain DEFAULT_CONTACT_NOTE_TYPE
        defaultContactNoteShouldNotBeFound("contactNoteType.doesNotContain=" + DEFAULT_CONTACT_NOTE_TYPE);

        // Get all the contactNoteList where contactNoteType does not contain UPDATED_CONTACT_NOTE_TYPE
        defaultContactNoteShouldBeFound("contactNoteType.doesNotContain=" + UPDATED_CONTACT_NOTE_TYPE);
    }


    @Test
    @Transactional
    public void getAllContactNotesByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactNote.setContact(contact);
        contactNoteRepository.saveAndFlush(contactNote);
        Long contactId = contact.getId();

        // Get all the contactNoteList where contact equals to contactId
        defaultContactNoteShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactNoteList where contact equals to contactId + 1
        defaultContactNoteShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactNoteShouldBeFound(String filter) throws Exception {
        restContactNoteMockMvc.perform(get("/api/contact-notes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactNote.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].contactNoteType").value(hasItem(DEFAULT_CONTACT_NOTE_TYPE)));

        // Check, that the count call also returns 1
        restContactNoteMockMvc.perform(get("/api/contact-notes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactNoteShouldNotBeFound(String filter) throws Exception {
        restContactNoteMockMvc.perform(get("/api/contact-notes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactNoteMockMvc.perform(get("/api/contact-notes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactNote() throws Exception {
        // Get the contactNote
        restContactNoteMockMvc.perform(get("/api/contact-notes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactNote() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        int databaseSizeBeforeUpdate = contactNoteRepository.findAll().size();

        // Update the contactNote
        ContactNote updatedContactNote = contactNoteRepository.findById(contactNote.getId()).get();
        // Disconnect from session so that the updates on updatedContactNote are not directly saved in db
        em.detach(updatedContactNote);
        updatedContactNote
            .note(UPDATED_NOTE)
            .contactNoteType(UPDATED_CONTACT_NOTE_TYPE);
        ContactNoteDTO contactNoteDTO = contactNoteMapper.toDto(updatedContactNote);

        restContactNoteMockMvc.perform(put("/api/contact-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactNoteDTO)))
            .andExpect(status().isOk());

        // Validate the ContactNote in the database
        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeUpdate);
        ContactNote testContactNote = contactNoteList.get(contactNoteList.size() - 1);
        assertThat(testContactNote.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testContactNote.getContactNoteType()).isEqualTo(UPDATED_CONTACT_NOTE_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactNote() throws Exception {
        int databaseSizeBeforeUpdate = contactNoteRepository.findAll().size();

        // Create the ContactNote
        ContactNoteDTO contactNoteDTO = contactNoteMapper.toDto(contactNote);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactNoteMockMvc.perform(put("/api/contact-notes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactNoteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactNote in the database
        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactNote() throws Exception {
        // Initialize the database
        contactNoteRepository.saveAndFlush(contactNote);

        int databaseSizeBeforeDelete = contactNoteRepository.findAll().size();

        // Delete the contactNote
        restContactNoteMockMvc.perform(delete("/api/contact-notes/{id}", contactNote.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactNote> contactNoteList = contactNoteRepository.findAll();
        assertThat(contactNoteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

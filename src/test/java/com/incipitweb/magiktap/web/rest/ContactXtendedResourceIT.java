package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactXtended;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactXtendedRepository;
import com.incipitweb.magiktap.service.ContactXtendedService;
import com.incipitweb.magiktap.service.dto.ContactXtendedDTO;
import com.incipitweb.magiktap.service.mapper.ContactXtendedMapper;
import com.incipitweb.magiktap.service.dto.ContactXtendedCriteria;
import com.incipitweb.magiktap.service.ContactXtendedQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactXtendedResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactXtendedResourceIT {

    private static final String DEFAULT_XNAME = "AAAAAAAAAA";
    private static final String UPDATED_XNAME = "BBBBBBBBBB";

    private static final String DEFAULT_XVALUE = "AAAAAAAAAA";
    private static final String UPDATED_XVALUE = "BBBBBBBBBB";

    @Autowired
    private ContactXtendedRepository contactXtendedRepository;

    @Autowired
    private ContactXtendedMapper contactXtendedMapper;

    @Autowired
    private ContactXtendedService contactXtendedService;

    @Autowired
    private ContactXtendedQueryService contactXtendedQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactXtendedMockMvc;

    private ContactXtended contactXtended;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactXtended createEntity(EntityManager em) {
        ContactXtended contactXtended = new ContactXtended()
            .xname(DEFAULT_XNAME)
            .xvalue(DEFAULT_XVALUE);
        return contactXtended;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactXtended createUpdatedEntity(EntityManager em) {
        ContactXtended contactXtended = new ContactXtended()
            .xname(UPDATED_XNAME)
            .xvalue(UPDATED_XVALUE);
        return contactXtended;
    }

    @BeforeEach
    public void initTest() {
        contactXtended = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactXtended() throws Exception {
        int databaseSizeBeforeCreate = contactXtendedRepository.findAll().size();
        // Create the ContactXtended
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(contactXtended);
        restContactXtendedMockMvc.perform(post("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactXtended in the database
        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeCreate + 1);
        ContactXtended testContactXtended = contactXtendedList.get(contactXtendedList.size() - 1);
        assertThat(testContactXtended.getXname()).isEqualTo(DEFAULT_XNAME);
        assertThat(testContactXtended.getXvalue()).isEqualTo(DEFAULT_XVALUE);
    }

    @Test
    @Transactional
    public void createContactXtendedWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactXtendedRepository.findAll().size();

        // Create the ContactXtended with an existing ID
        contactXtended.setId(1L);
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(contactXtended);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactXtendedMockMvc.perform(post("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactXtended in the database
        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkXnameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactXtendedRepository.findAll().size();
        // set the field null
        contactXtended.setXname(null);

        // Create the ContactXtended, which fails.
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(contactXtended);


        restContactXtendedMockMvc.perform(post("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isBadRequest());

        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkXvalueIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactXtendedRepository.findAll().size();
        // set the field null
        contactXtended.setXvalue(null);

        // Create the ContactXtended, which fails.
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(contactXtended);


        restContactXtendedMockMvc.perform(post("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isBadRequest());

        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactXtendeds() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactXtended.getId().intValue())))
            .andExpect(jsonPath("$.[*].xname").value(hasItem(DEFAULT_XNAME)))
            .andExpect(jsonPath("$.[*].xvalue").value(hasItem(DEFAULT_XVALUE)));
    }
    
    @Test
    @Transactional
    public void getContactXtended() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get the contactXtended
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds/{id}", contactXtended.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactXtended.getId().intValue()))
            .andExpect(jsonPath("$.xname").value(DEFAULT_XNAME))
            .andExpect(jsonPath("$.xvalue").value(DEFAULT_XVALUE));
    }


    @Test
    @Transactional
    public void getContactXtendedsByIdFiltering() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        Long id = contactXtended.getId();

        defaultContactXtendedShouldBeFound("id.equals=" + id);
        defaultContactXtendedShouldNotBeFound("id.notEquals=" + id);

        defaultContactXtendedShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactXtendedShouldNotBeFound("id.greaterThan=" + id);

        defaultContactXtendedShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactXtendedShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactXtendedsByXnameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname equals to DEFAULT_XNAME
        defaultContactXtendedShouldBeFound("xname.equals=" + DEFAULT_XNAME);

        // Get all the contactXtendedList where xname equals to UPDATED_XNAME
        defaultContactXtendedShouldNotBeFound("xname.equals=" + UPDATED_XNAME);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXnameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname not equals to DEFAULT_XNAME
        defaultContactXtendedShouldNotBeFound("xname.notEquals=" + DEFAULT_XNAME);

        // Get all the contactXtendedList where xname not equals to UPDATED_XNAME
        defaultContactXtendedShouldBeFound("xname.notEquals=" + UPDATED_XNAME);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXnameIsInShouldWork() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname in DEFAULT_XNAME or UPDATED_XNAME
        defaultContactXtendedShouldBeFound("xname.in=" + DEFAULT_XNAME + "," + UPDATED_XNAME);

        // Get all the contactXtendedList where xname equals to UPDATED_XNAME
        defaultContactXtendedShouldNotBeFound("xname.in=" + UPDATED_XNAME);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXnameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname is not null
        defaultContactXtendedShouldBeFound("xname.specified=true");

        // Get all the contactXtendedList where xname is null
        defaultContactXtendedShouldNotBeFound("xname.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactXtendedsByXnameContainsSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname contains DEFAULT_XNAME
        defaultContactXtendedShouldBeFound("xname.contains=" + DEFAULT_XNAME);

        // Get all the contactXtendedList where xname contains UPDATED_XNAME
        defaultContactXtendedShouldNotBeFound("xname.contains=" + UPDATED_XNAME);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXnameNotContainsSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xname does not contain DEFAULT_XNAME
        defaultContactXtendedShouldNotBeFound("xname.doesNotContain=" + DEFAULT_XNAME);

        // Get all the contactXtendedList where xname does not contain UPDATED_XNAME
        defaultContactXtendedShouldBeFound("xname.doesNotContain=" + UPDATED_XNAME);
    }


    @Test
    @Transactional
    public void getAllContactXtendedsByXvalueIsEqualToSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue equals to DEFAULT_XVALUE
        defaultContactXtendedShouldBeFound("xvalue.equals=" + DEFAULT_XVALUE);

        // Get all the contactXtendedList where xvalue equals to UPDATED_XVALUE
        defaultContactXtendedShouldNotBeFound("xvalue.equals=" + UPDATED_XVALUE);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXvalueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue not equals to DEFAULT_XVALUE
        defaultContactXtendedShouldNotBeFound("xvalue.notEquals=" + DEFAULT_XVALUE);

        // Get all the contactXtendedList where xvalue not equals to UPDATED_XVALUE
        defaultContactXtendedShouldBeFound("xvalue.notEquals=" + UPDATED_XVALUE);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXvalueIsInShouldWork() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue in DEFAULT_XVALUE or UPDATED_XVALUE
        defaultContactXtendedShouldBeFound("xvalue.in=" + DEFAULT_XVALUE + "," + UPDATED_XVALUE);

        // Get all the contactXtendedList where xvalue equals to UPDATED_XVALUE
        defaultContactXtendedShouldNotBeFound("xvalue.in=" + UPDATED_XVALUE);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXvalueIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue is not null
        defaultContactXtendedShouldBeFound("xvalue.specified=true");

        // Get all the contactXtendedList where xvalue is null
        defaultContactXtendedShouldNotBeFound("xvalue.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactXtendedsByXvalueContainsSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue contains DEFAULT_XVALUE
        defaultContactXtendedShouldBeFound("xvalue.contains=" + DEFAULT_XVALUE);

        // Get all the contactXtendedList where xvalue contains UPDATED_XVALUE
        defaultContactXtendedShouldNotBeFound("xvalue.contains=" + UPDATED_XVALUE);
    }

    @Test
    @Transactional
    public void getAllContactXtendedsByXvalueNotContainsSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        // Get all the contactXtendedList where xvalue does not contain DEFAULT_XVALUE
        defaultContactXtendedShouldNotBeFound("xvalue.doesNotContain=" + DEFAULT_XVALUE);

        // Get all the contactXtendedList where xvalue does not contain UPDATED_XVALUE
        defaultContactXtendedShouldBeFound("xvalue.doesNotContain=" + UPDATED_XVALUE);
    }


    @Test
    @Transactional
    public void getAllContactXtendedsByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactXtended.setContact(contact);
        contactXtendedRepository.saveAndFlush(contactXtended);
        Long contactId = contact.getId();

        // Get all the contactXtendedList where contact equals to contactId
        defaultContactXtendedShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactXtendedList where contact equals to contactId + 1
        defaultContactXtendedShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactXtendedShouldBeFound(String filter) throws Exception {
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactXtended.getId().intValue())))
            .andExpect(jsonPath("$.[*].xname").value(hasItem(DEFAULT_XNAME)))
            .andExpect(jsonPath("$.[*].xvalue").value(hasItem(DEFAULT_XVALUE)));

        // Check, that the count call also returns 1
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactXtendedShouldNotBeFound(String filter) throws Exception {
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactXtended() throws Exception {
        // Get the contactXtended
        restContactXtendedMockMvc.perform(get("/api/contact-xtendeds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactXtended() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        int databaseSizeBeforeUpdate = contactXtendedRepository.findAll().size();

        // Update the contactXtended
        ContactXtended updatedContactXtended = contactXtendedRepository.findById(contactXtended.getId()).get();
        // Disconnect from session so that the updates on updatedContactXtended are not directly saved in db
        em.detach(updatedContactXtended);
        updatedContactXtended
            .xname(UPDATED_XNAME)
            .xvalue(UPDATED_XVALUE);
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(updatedContactXtended);

        restContactXtendedMockMvc.perform(put("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isOk());

        // Validate the ContactXtended in the database
        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeUpdate);
        ContactXtended testContactXtended = contactXtendedList.get(contactXtendedList.size() - 1);
        assertThat(testContactXtended.getXname()).isEqualTo(UPDATED_XNAME);
        assertThat(testContactXtended.getXvalue()).isEqualTo(UPDATED_XVALUE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactXtended() throws Exception {
        int databaseSizeBeforeUpdate = contactXtendedRepository.findAll().size();

        // Create the ContactXtended
        ContactXtendedDTO contactXtendedDTO = contactXtendedMapper.toDto(contactXtended);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactXtendedMockMvc.perform(put("/api/contact-xtendeds")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactXtendedDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactXtended in the database
        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactXtended() throws Exception {
        // Initialize the database
        contactXtendedRepository.saveAndFlush(contactXtended);

        int databaseSizeBeforeDelete = contactXtendedRepository.findAll().size();

        // Delete the contactXtended
        restContactXtendedMockMvc.perform(delete("/api/contact-xtendeds/{id}", contactXtended.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactXtended> contactXtendedList = contactXtendedRepository.findAll();
        assertThat(contactXtendedList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

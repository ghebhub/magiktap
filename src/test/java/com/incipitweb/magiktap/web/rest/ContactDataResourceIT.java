package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactData;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactDataRepository;
import com.incipitweb.magiktap.service.ContactDataService;
import com.incipitweb.magiktap.service.dto.ContactDataDTO;
import com.incipitweb.magiktap.service.mapper.ContactDataMapper;
import com.incipitweb.magiktap.service.dto.ContactDataCriteria;
import com.incipitweb.magiktap.service.ContactDataQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactDataResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactDataResourceIT {

    private static final String DEFAULT_DATA_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DATA_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_INLINE = "A";
    private static final String UPDATED_INLINE = "B";

    private static final String DEFAULT_CONTACT_TYPES = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_TYPES = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_ENCODING_TYPES = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ENCODING_TYPES = "BBBBBBBBBB";

    private static final byte[] DEFAULT_CONTACT_DATA = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_CONTACT_DATA = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_CONTACT_DATA_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_CONTACT_DATA_CONTENT_TYPE = "image/png";

    @Autowired
    private ContactDataRepository contactDataRepository;

    @Autowired
    private ContactDataMapper contactDataMapper;

    @Autowired
    private ContactDataService contactDataService;

    @Autowired
    private ContactDataQueryService contactDataQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactDataMockMvc;

    private ContactData contactData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactData createEntity(EntityManager em) {
        ContactData contactData = new ContactData()
            .dataName(DEFAULT_DATA_NAME)
            .url(DEFAULT_URL)
            .inline(DEFAULT_INLINE)
            .contactTypes(DEFAULT_CONTACT_TYPES)
            .contactEncodingTypes(DEFAULT_CONTACT_ENCODING_TYPES)
            .contactData(DEFAULT_CONTACT_DATA)
            .contactDataContentType(DEFAULT_CONTACT_DATA_CONTENT_TYPE);
        return contactData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactData createUpdatedEntity(EntityManager em) {
        ContactData contactData = new ContactData()
            .dataName(UPDATED_DATA_NAME)
            .url(UPDATED_URL)
            .inline(UPDATED_INLINE)
            .contactTypes(UPDATED_CONTACT_TYPES)
            .contactEncodingTypes(UPDATED_CONTACT_ENCODING_TYPES)
            .contactData(UPDATED_CONTACT_DATA)
            .contactDataContentType(UPDATED_CONTACT_DATA_CONTENT_TYPE);
        return contactData;
    }

    @BeforeEach
    public void initTest() {
        contactData = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactData() throws Exception {
        int databaseSizeBeforeCreate = contactDataRepository.findAll().size();
        // Create the ContactData
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);
        restContactDataMockMvc.perform(post("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactData in the database
        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeCreate + 1);
        ContactData testContactData = contactDataList.get(contactDataList.size() - 1);
        assertThat(testContactData.getDataName()).isEqualTo(DEFAULT_DATA_NAME);
        assertThat(testContactData.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testContactData.getInline()).isEqualTo(DEFAULT_INLINE);
        assertThat(testContactData.getContactTypes()).isEqualTo(DEFAULT_CONTACT_TYPES);
        assertThat(testContactData.getContactEncodingTypes()).isEqualTo(DEFAULT_CONTACT_ENCODING_TYPES);
        assertThat(testContactData.getContactData()).isEqualTo(DEFAULT_CONTACT_DATA);
        assertThat(testContactData.getContactDataContentType()).isEqualTo(DEFAULT_CONTACT_DATA_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createContactDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactDataRepository.findAll().size();

        // Create the ContactData with an existing ID
        contactData.setId(1L);
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactDataMockMvc.perform(post("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactData in the database
        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDataNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactDataRepository.findAll().size();
        // set the field null
        contactData.setDataName(null);

        // Create the ContactData, which fails.
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);


        restContactDataMockMvc.perform(post("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isBadRequest());

        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactTypesIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactDataRepository.findAll().size();
        // set the field null
        contactData.setContactTypes(null);

        // Create the ContactData, which fails.
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);


        restContactDataMockMvc.perform(post("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isBadRequest());

        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactEncodingTypesIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactDataRepository.findAll().size();
        // set the field null
        contactData.setContactEncodingTypes(null);

        // Create the ContactData, which fails.
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);


        restContactDataMockMvc.perform(post("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isBadRequest());

        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactData() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList
        restContactDataMockMvc.perform(get("/api/contact-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactData.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataName").value(hasItem(DEFAULT_DATA_NAME)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].inline").value(hasItem(DEFAULT_INLINE)))
            .andExpect(jsonPath("$.[*].contactTypes").value(hasItem(DEFAULT_CONTACT_TYPES)))
            .andExpect(jsonPath("$.[*].contactEncodingTypes").value(hasItem(DEFAULT_CONTACT_ENCODING_TYPES)))
            .andExpect(jsonPath("$.[*].contactDataContentType").value(hasItem(DEFAULT_CONTACT_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].contactData").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTACT_DATA))));
    }
    
    @Test
    @Transactional
    public void getContactData() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get the contactData
        restContactDataMockMvc.perform(get("/api/contact-data/{id}", contactData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactData.getId().intValue()))
            .andExpect(jsonPath("$.dataName").value(DEFAULT_DATA_NAME))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL))
            .andExpect(jsonPath("$.inline").value(DEFAULT_INLINE))
            .andExpect(jsonPath("$.contactTypes").value(DEFAULT_CONTACT_TYPES))
            .andExpect(jsonPath("$.contactEncodingTypes").value(DEFAULT_CONTACT_ENCODING_TYPES))
            .andExpect(jsonPath("$.contactDataContentType").value(DEFAULT_CONTACT_DATA_CONTENT_TYPE))
            .andExpect(jsonPath("$.contactData").value(Base64Utils.encodeToString(DEFAULT_CONTACT_DATA)));
    }


    @Test
    @Transactional
    public void getContactDataByIdFiltering() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        Long id = contactData.getId();

        defaultContactDataShouldBeFound("id.equals=" + id);
        defaultContactDataShouldNotBeFound("id.notEquals=" + id);

        defaultContactDataShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactDataShouldNotBeFound("id.greaterThan=" + id);

        defaultContactDataShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactDataShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactDataByDataNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName equals to DEFAULT_DATA_NAME
        defaultContactDataShouldBeFound("dataName.equals=" + DEFAULT_DATA_NAME);

        // Get all the contactDataList where dataName equals to UPDATED_DATA_NAME
        defaultContactDataShouldNotBeFound("dataName.equals=" + UPDATED_DATA_NAME);
    }

    @Test
    @Transactional
    public void getAllContactDataByDataNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName not equals to DEFAULT_DATA_NAME
        defaultContactDataShouldNotBeFound("dataName.notEquals=" + DEFAULT_DATA_NAME);

        // Get all the contactDataList where dataName not equals to UPDATED_DATA_NAME
        defaultContactDataShouldBeFound("dataName.notEquals=" + UPDATED_DATA_NAME);
    }

    @Test
    @Transactional
    public void getAllContactDataByDataNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName in DEFAULT_DATA_NAME or UPDATED_DATA_NAME
        defaultContactDataShouldBeFound("dataName.in=" + DEFAULT_DATA_NAME + "," + UPDATED_DATA_NAME);

        // Get all the contactDataList where dataName equals to UPDATED_DATA_NAME
        defaultContactDataShouldNotBeFound("dataName.in=" + UPDATED_DATA_NAME);
    }

    @Test
    @Transactional
    public void getAllContactDataByDataNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName is not null
        defaultContactDataShouldBeFound("dataName.specified=true");

        // Get all the contactDataList where dataName is null
        defaultContactDataShouldNotBeFound("dataName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactDataByDataNameContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName contains DEFAULT_DATA_NAME
        defaultContactDataShouldBeFound("dataName.contains=" + DEFAULT_DATA_NAME);

        // Get all the contactDataList where dataName contains UPDATED_DATA_NAME
        defaultContactDataShouldNotBeFound("dataName.contains=" + UPDATED_DATA_NAME);
    }

    @Test
    @Transactional
    public void getAllContactDataByDataNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where dataName does not contain DEFAULT_DATA_NAME
        defaultContactDataShouldNotBeFound("dataName.doesNotContain=" + DEFAULT_DATA_NAME);

        // Get all the contactDataList where dataName does not contain UPDATED_DATA_NAME
        defaultContactDataShouldBeFound("dataName.doesNotContain=" + UPDATED_DATA_NAME);
    }


    @Test
    @Transactional
    public void getAllContactDataByUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url equals to DEFAULT_URL
        defaultContactDataShouldBeFound("url.equals=" + DEFAULT_URL);

        // Get all the contactDataList where url equals to UPDATED_URL
        defaultContactDataShouldNotBeFound("url.equals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactDataByUrlIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url not equals to DEFAULT_URL
        defaultContactDataShouldNotBeFound("url.notEquals=" + DEFAULT_URL);

        // Get all the contactDataList where url not equals to UPDATED_URL
        defaultContactDataShouldBeFound("url.notEquals=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactDataByUrlIsInShouldWork() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url in DEFAULT_URL or UPDATED_URL
        defaultContactDataShouldBeFound("url.in=" + DEFAULT_URL + "," + UPDATED_URL);

        // Get all the contactDataList where url equals to UPDATED_URL
        defaultContactDataShouldNotBeFound("url.in=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactDataByUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url is not null
        defaultContactDataShouldBeFound("url.specified=true");

        // Get all the contactDataList where url is null
        defaultContactDataShouldNotBeFound("url.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactDataByUrlContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url contains DEFAULT_URL
        defaultContactDataShouldBeFound("url.contains=" + DEFAULT_URL);

        // Get all the contactDataList where url contains UPDATED_URL
        defaultContactDataShouldNotBeFound("url.contains=" + UPDATED_URL);
    }

    @Test
    @Transactional
    public void getAllContactDataByUrlNotContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where url does not contain DEFAULT_URL
        defaultContactDataShouldNotBeFound("url.doesNotContain=" + DEFAULT_URL);

        // Get all the contactDataList where url does not contain UPDATED_URL
        defaultContactDataShouldBeFound("url.doesNotContain=" + UPDATED_URL);
    }


    @Test
    @Transactional
    public void getAllContactDataByInlineIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline equals to DEFAULT_INLINE
        defaultContactDataShouldBeFound("inline.equals=" + DEFAULT_INLINE);

        // Get all the contactDataList where inline equals to UPDATED_INLINE
        defaultContactDataShouldNotBeFound("inline.equals=" + UPDATED_INLINE);
    }

    @Test
    @Transactional
    public void getAllContactDataByInlineIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline not equals to DEFAULT_INLINE
        defaultContactDataShouldNotBeFound("inline.notEquals=" + DEFAULT_INLINE);

        // Get all the contactDataList where inline not equals to UPDATED_INLINE
        defaultContactDataShouldBeFound("inline.notEquals=" + UPDATED_INLINE);
    }

    @Test
    @Transactional
    public void getAllContactDataByInlineIsInShouldWork() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline in DEFAULT_INLINE or UPDATED_INLINE
        defaultContactDataShouldBeFound("inline.in=" + DEFAULT_INLINE + "," + UPDATED_INLINE);

        // Get all the contactDataList where inline equals to UPDATED_INLINE
        defaultContactDataShouldNotBeFound("inline.in=" + UPDATED_INLINE);
    }

    @Test
    @Transactional
    public void getAllContactDataByInlineIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline is not null
        defaultContactDataShouldBeFound("inline.specified=true");

        // Get all the contactDataList where inline is null
        defaultContactDataShouldNotBeFound("inline.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactDataByInlineContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline contains DEFAULT_INLINE
        defaultContactDataShouldBeFound("inline.contains=" + DEFAULT_INLINE);

        // Get all the contactDataList where inline contains UPDATED_INLINE
        defaultContactDataShouldNotBeFound("inline.contains=" + UPDATED_INLINE);
    }

    @Test
    @Transactional
    public void getAllContactDataByInlineNotContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where inline does not contain DEFAULT_INLINE
        defaultContactDataShouldNotBeFound("inline.doesNotContain=" + DEFAULT_INLINE);

        // Get all the contactDataList where inline does not contain UPDATED_INLINE
        defaultContactDataShouldBeFound("inline.doesNotContain=" + UPDATED_INLINE);
    }


    @Test
    @Transactional
    public void getAllContactDataByContactTypesIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes equals to DEFAULT_CONTACT_TYPES
        defaultContactDataShouldBeFound("contactTypes.equals=" + DEFAULT_CONTACT_TYPES);

        // Get all the contactDataList where contactTypes equals to UPDATED_CONTACT_TYPES
        defaultContactDataShouldNotBeFound("contactTypes.equals=" + UPDATED_CONTACT_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactTypesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes not equals to DEFAULT_CONTACT_TYPES
        defaultContactDataShouldNotBeFound("contactTypes.notEquals=" + DEFAULT_CONTACT_TYPES);

        // Get all the contactDataList where contactTypes not equals to UPDATED_CONTACT_TYPES
        defaultContactDataShouldBeFound("contactTypes.notEquals=" + UPDATED_CONTACT_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactTypesIsInShouldWork() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes in DEFAULT_CONTACT_TYPES or UPDATED_CONTACT_TYPES
        defaultContactDataShouldBeFound("contactTypes.in=" + DEFAULT_CONTACT_TYPES + "," + UPDATED_CONTACT_TYPES);

        // Get all the contactDataList where contactTypes equals to UPDATED_CONTACT_TYPES
        defaultContactDataShouldNotBeFound("contactTypes.in=" + UPDATED_CONTACT_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactTypesIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes is not null
        defaultContactDataShouldBeFound("contactTypes.specified=true");

        // Get all the contactDataList where contactTypes is null
        defaultContactDataShouldNotBeFound("contactTypes.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactDataByContactTypesContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes contains DEFAULT_CONTACT_TYPES
        defaultContactDataShouldBeFound("contactTypes.contains=" + DEFAULT_CONTACT_TYPES);

        // Get all the contactDataList where contactTypes contains UPDATED_CONTACT_TYPES
        defaultContactDataShouldNotBeFound("contactTypes.contains=" + UPDATED_CONTACT_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactTypesNotContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactTypes does not contain DEFAULT_CONTACT_TYPES
        defaultContactDataShouldNotBeFound("contactTypes.doesNotContain=" + DEFAULT_CONTACT_TYPES);

        // Get all the contactDataList where contactTypes does not contain UPDATED_CONTACT_TYPES
        defaultContactDataShouldBeFound("contactTypes.doesNotContain=" + UPDATED_CONTACT_TYPES);
    }


    @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes equals to DEFAULT_CONTACT_ENCODING_TYPES
        defaultContactDataShouldBeFound("contactEncodingTypes.equals=" + DEFAULT_CONTACT_ENCODING_TYPES);

        // Get all the contactDataList where contactEncodingTypes equals to UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldNotBeFound("contactEncodingTypes.equals=" + UPDATED_CONTACT_ENCODING_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes not equals to DEFAULT_CONTACT_ENCODING_TYPES
        defaultContactDataShouldNotBeFound("contactEncodingTypes.notEquals=" + DEFAULT_CONTACT_ENCODING_TYPES);

        // Get all the contactDataList where contactEncodingTypes not equals to UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldBeFound("contactEncodingTypes.notEquals=" + UPDATED_CONTACT_ENCODING_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesIsInShouldWork() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes in DEFAULT_CONTACT_ENCODING_TYPES or UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldBeFound("contactEncodingTypes.in=" + DEFAULT_CONTACT_ENCODING_TYPES + "," + UPDATED_CONTACT_ENCODING_TYPES);

        // Get all the contactDataList where contactEncodingTypes equals to UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldNotBeFound("contactEncodingTypes.in=" + UPDATED_CONTACT_ENCODING_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes is not null
        defaultContactDataShouldBeFound("contactEncodingTypes.specified=true");

        // Get all the contactDataList where contactEncodingTypes is null
        defaultContactDataShouldNotBeFound("contactEncodingTypes.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes contains DEFAULT_CONTACT_ENCODING_TYPES
        defaultContactDataShouldBeFound("contactEncodingTypes.contains=" + DEFAULT_CONTACT_ENCODING_TYPES);

        // Get all the contactDataList where contactEncodingTypes contains UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldNotBeFound("contactEncodingTypes.contains=" + UPDATED_CONTACT_ENCODING_TYPES);
    }

    @Test
    @Transactional
    public void getAllContactDataByContactEncodingTypesNotContainsSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        // Get all the contactDataList where contactEncodingTypes does not contain DEFAULT_CONTACT_ENCODING_TYPES
        defaultContactDataShouldNotBeFound("contactEncodingTypes.doesNotContain=" + DEFAULT_CONTACT_ENCODING_TYPES);

        // Get all the contactDataList where contactEncodingTypes does not contain UPDATED_CONTACT_ENCODING_TYPES
        defaultContactDataShouldBeFound("contactEncodingTypes.doesNotContain=" + UPDATED_CONTACT_ENCODING_TYPES);
    }


    @Test
    @Transactional
    public void getAllContactDataByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactData.setContact(contact);
        contactDataRepository.saveAndFlush(contactData);
        Long contactId = contact.getId();

        // Get all the contactDataList where contact equals to contactId
        defaultContactDataShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactDataList where contact equals to contactId + 1
        defaultContactDataShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactDataShouldBeFound(String filter) throws Exception {
        restContactDataMockMvc.perform(get("/api/contact-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactData.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataName").value(hasItem(DEFAULT_DATA_NAME)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].inline").value(hasItem(DEFAULT_INLINE)))
            .andExpect(jsonPath("$.[*].contactTypes").value(hasItem(DEFAULT_CONTACT_TYPES)))
            .andExpect(jsonPath("$.[*].contactEncodingTypes").value(hasItem(DEFAULT_CONTACT_ENCODING_TYPES)))
            .andExpect(jsonPath("$.[*].contactDataContentType").value(hasItem(DEFAULT_CONTACT_DATA_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].contactData").value(hasItem(Base64Utils.encodeToString(DEFAULT_CONTACT_DATA))));

        // Check, that the count call also returns 1
        restContactDataMockMvc.perform(get("/api/contact-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactDataShouldNotBeFound(String filter) throws Exception {
        restContactDataMockMvc.perform(get("/api/contact-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactDataMockMvc.perform(get("/api/contact-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactData() throws Exception {
        // Get the contactData
        restContactDataMockMvc.perform(get("/api/contact-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactData() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        int databaseSizeBeforeUpdate = contactDataRepository.findAll().size();

        // Update the contactData
        ContactData updatedContactData = contactDataRepository.findById(contactData.getId()).get();
        // Disconnect from session so that the updates on updatedContactData are not directly saved in db
        em.detach(updatedContactData);
        updatedContactData
            .dataName(UPDATED_DATA_NAME)
            .url(UPDATED_URL)
            .inline(UPDATED_INLINE)
            .contactTypes(UPDATED_CONTACT_TYPES)
            .contactEncodingTypes(UPDATED_CONTACT_ENCODING_TYPES)
            .contactData(UPDATED_CONTACT_DATA)
            .contactDataContentType(UPDATED_CONTACT_DATA_CONTENT_TYPE);
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(updatedContactData);

        restContactDataMockMvc.perform(put("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isOk());

        // Validate the ContactData in the database
        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeUpdate);
        ContactData testContactData = contactDataList.get(contactDataList.size() - 1);
        assertThat(testContactData.getDataName()).isEqualTo(UPDATED_DATA_NAME);
        assertThat(testContactData.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testContactData.getInline()).isEqualTo(UPDATED_INLINE);
        assertThat(testContactData.getContactTypes()).isEqualTo(UPDATED_CONTACT_TYPES);
        assertThat(testContactData.getContactEncodingTypes()).isEqualTo(UPDATED_CONTACT_ENCODING_TYPES);
        assertThat(testContactData.getContactData()).isEqualTo(UPDATED_CONTACT_DATA);
        assertThat(testContactData.getContactDataContentType()).isEqualTo(UPDATED_CONTACT_DATA_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingContactData() throws Exception {
        int databaseSizeBeforeUpdate = contactDataRepository.findAll().size();

        // Create the ContactData
        ContactDataDTO contactDataDTO = contactDataMapper.toDto(contactData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactDataMockMvc.perform(put("/api/contact-data")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactData in the database
        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactData() throws Exception {
        // Initialize the database
        contactDataRepository.saveAndFlush(contactData);

        int databaseSizeBeforeDelete = contactDataRepository.findAll().size();

        // Delete the contactData
        restContactDataMockMvc.perform(delete("/api/contact-data/{id}", contactData.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactData> contactDataList = contactDataRepository.findAll();
        assertThat(contactDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.incipitweb.magiktap.web.rest;

import com.incipitweb.magiktap.MagiktapApp;
import com.incipitweb.magiktap.domain.ContactCategories;
import com.incipitweb.magiktap.domain.Contact;
import com.incipitweb.magiktap.repository.ContactCategoriesRepository;
import com.incipitweb.magiktap.service.ContactCategoriesService;
import com.incipitweb.magiktap.service.dto.ContactCategoriesDTO;
import com.incipitweb.magiktap.service.mapper.ContactCategoriesMapper;
import com.incipitweb.magiktap.service.dto.ContactCategoriesCriteria;
import com.incipitweb.magiktap.service.ContactCategoriesQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ContactCategoriesResource} REST controller.
 */
@SpringBootTest(classes = MagiktapApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ContactCategoriesResourceIT {

    private static final String DEFAULT_CATEGORY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY_NAME = "BBBBBBBBBB";

    @Autowired
    private ContactCategoriesRepository contactCategoriesRepository;

    @Autowired
    private ContactCategoriesMapper contactCategoriesMapper;

    @Autowired
    private ContactCategoriesService contactCategoriesService;

    @Autowired
    private ContactCategoriesQueryService contactCategoriesQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restContactCategoriesMockMvc;

    private ContactCategories contactCategories;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactCategories createEntity(EntityManager em) {
        ContactCategories contactCategories = new ContactCategories()
            .categoryName(DEFAULT_CATEGORY_NAME);
        return contactCategories;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactCategories createUpdatedEntity(EntityManager em) {
        ContactCategories contactCategories = new ContactCategories()
            .categoryName(UPDATED_CATEGORY_NAME);
        return contactCategories;
    }

    @BeforeEach
    public void initTest() {
        contactCategories = createEntity(em);
    }

    @Test
    @Transactional
    public void createContactCategories() throws Exception {
        int databaseSizeBeforeCreate = contactCategoriesRepository.findAll().size();
        // Create the ContactCategories
        ContactCategoriesDTO contactCategoriesDTO = contactCategoriesMapper.toDto(contactCategories);
        restContactCategoriesMockMvc.perform(post("/api/contact-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactCategoriesDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactCategories in the database
        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeCreate + 1);
        ContactCategories testContactCategories = contactCategoriesList.get(contactCategoriesList.size() - 1);
        assertThat(testContactCategories.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void createContactCategoriesWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactCategoriesRepository.findAll().size();

        // Create the ContactCategories with an existing ID
        contactCategories.setId(1L);
        ContactCategoriesDTO contactCategoriesDTO = contactCategoriesMapper.toDto(contactCategories);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactCategoriesMockMvc.perform(post("/api/contact-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactCategoriesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactCategories in the database
        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCategoryNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactCategoriesRepository.findAll().size();
        // set the field null
        contactCategories.setCategoryName(null);

        // Create the ContactCategories, which fails.
        ContactCategoriesDTO contactCategoriesDTO = contactCategoriesMapper.toDto(contactCategories);


        restContactCategoriesMockMvc.perform(post("/api/contact-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactCategoriesDTO)))
            .andExpect(status().isBadRequest());

        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContactCategories() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList
        restContactCategoriesMockMvc.perform(get("/api/contact-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactCategories.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME)));
    }
    
    @Test
    @Transactional
    public void getContactCategories() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get the contactCategories
        restContactCategoriesMockMvc.perform(get("/api/contact-categories/{id}", contactCategories.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(contactCategories.getId().intValue()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME));
    }


    @Test
    @Transactional
    public void getContactCategoriesByIdFiltering() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        Long id = contactCategories.getId();

        defaultContactCategoriesShouldBeFound("id.equals=" + id);
        defaultContactCategoriesShouldNotBeFound("id.notEquals=" + id);

        defaultContactCategoriesShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultContactCategoriesShouldNotBeFound("id.greaterThan=" + id);

        defaultContactCategoriesShouldBeFound("id.lessThanOrEqual=" + id);
        defaultContactCategoriesShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName equals to DEFAULT_CATEGORY_NAME
        defaultContactCategoriesShouldBeFound("categoryName.equals=" + DEFAULT_CATEGORY_NAME);

        // Get all the contactCategoriesList where categoryName equals to UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldNotBeFound("categoryName.equals=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName not equals to DEFAULT_CATEGORY_NAME
        defaultContactCategoriesShouldNotBeFound("categoryName.notEquals=" + DEFAULT_CATEGORY_NAME);

        // Get all the contactCategoriesList where categoryName not equals to UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldBeFound("categoryName.notEquals=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName in DEFAULT_CATEGORY_NAME or UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldBeFound("categoryName.in=" + DEFAULT_CATEGORY_NAME + "," + UPDATED_CATEGORY_NAME);

        // Get all the contactCategoriesList where categoryName equals to UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldNotBeFound("categoryName.in=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName is not null
        defaultContactCategoriesShouldBeFound("categoryName.specified=true");

        // Get all the contactCategoriesList where categoryName is null
        defaultContactCategoriesShouldNotBeFound("categoryName.specified=false");
    }
                @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameContainsSomething() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName contains DEFAULT_CATEGORY_NAME
        defaultContactCategoriesShouldBeFound("categoryName.contains=" + DEFAULT_CATEGORY_NAME);

        // Get all the contactCategoriesList where categoryName contains UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldNotBeFound("categoryName.contains=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllContactCategoriesByCategoryNameNotContainsSomething() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        // Get all the contactCategoriesList where categoryName does not contain DEFAULT_CATEGORY_NAME
        defaultContactCategoriesShouldNotBeFound("categoryName.doesNotContain=" + DEFAULT_CATEGORY_NAME);

        // Get all the contactCategoriesList where categoryName does not contain UPDATED_CATEGORY_NAME
        defaultContactCategoriesShouldBeFound("categoryName.doesNotContain=" + UPDATED_CATEGORY_NAME);
    }


    @Test
    @Transactional
    public void getAllContactCategoriesByContactIsEqualToSomething() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);
        Contact contact = ContactResourceIT.createEntity(em);
        em.persist(contact);
        em.flush();
        contactCategories.setContact(contact);
        contactCategoriesRepository.saveAndFlush(contactCategories);
        Long contactId = contact.getId();

        // Get all the contactCategoriesList where contact equals to contactId
        defaultContactCategoriesShouldBeFound("contactId.equals=" + contactId);

        // Get all the contactCategoriesList where contact equals to contactId + 1
        defaultContactCategoriesShouldNotBeFound("contactId.equals=" + (contactId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultContactCategoriesShouldBeFound(String filter) throws Exception {
        restContactCategoriesMockMvc.perform(get("/api/contact-categories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactCategories.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME)));

        // Check, that the count call also returns 1
        restContactCategoriesMockMvc.perform(get("/api/contact-categories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultContactCategoriesShouldNotBeFound(String filter) throws Exception {
        restContactCategoriesMockMvc.perform(get("/api/contact-categories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restContactCategoriesMockMvc.perform(get("/api/contact-categories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingContactCategories() throws Exception {
        // Get the contactCategories
        restContactCategoriesMockMvc.perform(get("/api/contact-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContactCategories() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        int databaseSizeBeforeUpdate = contactCategoriesRepository.findAll().size();

        // Update the contactCategories
        ContactCategories updatedContactCategories = contactCategoriesRepository.findById(contactCategories.getId()).get();
        // Disconnect from session so that the updates on updatedContactCategories are not directly saved in db
        em.detach(updatedContactCategories);
        updatedContactCategories
            .categoryName(UPDATED_CATEGORY_NAME);
        ContactCategoriesDTO contactCategoriesDTO = contactCategoriesMapper.toDto(updatedContactCategories);

        restContactCategoriesMockMvc.perform(put("/api/contact-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactCategoriesDTO)))
            .andExpect(status().isOk());

        // Validate the ContactCategories in the database
        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeUpdate);
        ContactCategories testContactCategories = contactCategoriesList.get(contactCategoriesList.size() - 1);
        assertThat(testContactCategories.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingContactCategories() throws Exception {
        int databaseSizeBeforeUpdate = contactCategoriesRepository.findAll().size();

        // Create the ContactCategories
        ContactCategoriesDTO contactCategoriesDTO = contactCategoriesMapper.toDto(contactCategories);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restContactCategoriesMockMvc.perform(put("/api/contact-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(contactCategoriesDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactCategories in the database
        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContactCategories() throws Exception {
        // Initialize the database
        contactCategoriesRepository.saveAndFlush(contactCategories);

        int databaseSizeBeforeDelete = contactCategoriesRepository.findAll().size();

        // Delete the contactCategories
        restContactCategoriesMockMvc.perform(delete("/api/contact-categories/{id}", contactCategories.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ContactCategories> contactCategoriesList = contactCategoriesRepository.findAll();
        assertThat(contactCategoriesList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

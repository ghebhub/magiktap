package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactCategoriesMapperTest {

    private ContactCategoriesMapper contactCategoriesMapper;

    @BeforeEach
    public void setUp() {
        contactCategoriesMapper = new ContactCategoriesMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactCategoriesMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactCategoriesMapper.fromId(null)).isNull();
    }
}

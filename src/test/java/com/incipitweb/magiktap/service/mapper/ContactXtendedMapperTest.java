package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactXtendedMapperTest {

    private ContactXtendedMapper contactXtendedMapper;

    @BeforeEach
    public void setUp() {
        contactXtendedMapper = new ContactXtendedMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactXtendedMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactXtendedMapper.fromId(null)).isNull();
    }
}

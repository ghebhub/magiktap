package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactDataMapperTest {

    private ContactDataMapper contactDataMapper;

    @BeforeEach
    public void setUp() {
        contactDataMapper = new ContactDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactDataMapper.fromId(null)).isNull();
    }
}

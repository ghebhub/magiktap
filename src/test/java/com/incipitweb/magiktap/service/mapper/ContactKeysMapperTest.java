package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactKeysMapperTest {

    private ContactKeysMapper contactKeysMapper;

    @BeforeEach
    public void setUp() {
        contactKeysMapper = new ContactKeysMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactKeysMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactKeysMapper.fromId(null)).isNull();
    }
}

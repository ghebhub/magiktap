package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactNoteMapperTest {

    private ContactNoteMapper contactNoteMapper;

    @BeforeEach
    public void setUp() {
        contactNoteMapper = new ContactNoteMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactNoteMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactNoteMapper.fromId(null)).isNull();
    }
}

package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactPhoneNumberMapperTest {

    private ContactPhoneNumberMapper contactPhoneNumberMapper;

    @BeforeEach
    public void setUp() {
        contactPhoneNumberMapper = new ContactPhoneNumberMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactPhoneNumberMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactPhoneNumberMapper.fromId(null)).isNull();
    }
}

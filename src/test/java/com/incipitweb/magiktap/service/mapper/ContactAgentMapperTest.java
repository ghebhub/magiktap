package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactAgentMapperTest {

    private ContactAgentMapper contactAgentMapper;

    @BeforeEach
    public void setUp() {
        contactAgentMapper = new ContactAgentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactAgentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactAgentMapper.fromId(null)).isNull();
    }
}

package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactEmailMapperTest {

    private ContactEmailMapper contactEmailMapper;

    @BeforeEach
    public void setUp() {
        contactEmailMapper = new ContactEmailMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactEmailMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactEmailMapper.fromId(null)).isNull();
    }
}

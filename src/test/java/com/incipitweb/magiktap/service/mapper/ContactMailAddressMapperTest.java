package com.incipitweb.magiktap.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactMailAddressMapperTest {

    private ContactMailAddressMapper contactMailAddressMapper;

    @BeforeEach
    public void setUp() {
        contactMailAddressMapper = new ContactMailAddressMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(contactMailAddressMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(contactMailAddressMapper.fromId(null)).isNull();
    }
}

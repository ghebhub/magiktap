package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactXtendedDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactXtendedDTO.class);
        ContactXtendedDTO contactXtendedDTO1 = new ContactXtendedDTO();
        contactXtendedDTO1.setId(1L);
        ContactXtendedDTO contactXtendedDTO2 = new ContactXtendedDTO();
        assertThat(contactXtendedDTO1).isNotEqualTo(contactXtendedDTO2);
        contactXtendedDTO2.setId(contactXtendedDTO1.getId());
        assertThat(contactXtendedDTO1).isEqualTo(contactXtendedDTO2);
        contactXtendedDTO2.setId(2L);
        assertThat(contactXtendedDTO1).isNotEqualTo(contactXtendedDTO2);
        contactXtendedDTO1.setId(null);
        assertThat(contactXtendedDTO1).isNotEqualTo(contactXtendedDTO2);
    }
}

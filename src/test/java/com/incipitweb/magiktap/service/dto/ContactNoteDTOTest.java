package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactNoteDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactNoteDTO.class);
        ContactNoteDTO contactNoteDTO1 = new ContactNoteDTO();
        contactNoteDTO1.setId(1L);
        ContactNoteDTO contactNoteDTO2 = new ContactNoteDTO();
        assertThat(contactNoteDTO1).isNotEqualTo(contactNoteDTO2);
        contactNoteDTO2.setId(contactNoteDTO1.getId());
        assertThat(contactNoteDTO1).isEqualTo(contactNoteDTO2);
        contactNoteDTO2.setId(2L);
        assertThat(contactNoteDTO1).isNotEqualTo(contactNoteDTO2);
        contactNoteDTO1.setId(null);
        assertThat(contactNoteDTO1).isNotEqualTo(contactNoteDTO2);
    }
}

package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactCategoriesDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactCategoriesDTO.class);
        ContactCategoriesDTO contactCategoriesDTO1 = new ContactCategoriesDTO();
        contactCategoriesDTO1.setId(1L);
        ContactCategoriesDTO contactCategoriesDTO2 = new ContactCategoriesDTO();
        assertThat(contactCategoriesDTO1).isNotEqualTo(contactCategoriesDTO2);
        contactCategoriesDTO2.setId(contactCategoriesDTO1.getId());
        assertThat(contactCategoriesDTO1).isEqualTo(contactCategoriesDTO2);
        contactCategoriesDTO2.setId(2L);
        assertThat(contactCategoriesDTO1).isNotEqualTo(contactCategoriesDTO2);
        contactCategoriesDTO1.setId(null);
        assertThat(contactCategoriesDTO1).isNotEqualTo(contactCategoriesDTO2);
    }
}

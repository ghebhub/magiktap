package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactKeysDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactKeysDTO.class);
        ContactKeysDTO contactKeysDTO1 = new ContactKeysDTO();
        contactKeysDTO1.setId(1L);
        ContactKeysDTO contactKeysDTO2 = new ContactKeysDTO();
        assertThat(contactKeysDTO1).isNotEqualTo(contactKeysDTO2);
        contactKeysDTO2.setId(contactKeysDTO1.getId());
        assertThat(contactKeysDTO1).isEqualTo(contactKeysDTO2);
        contactKeysDTO2.setId(2L);
        assertThat(contactKeysDTO1).isNotEqualTo(contactKeysDTO2);
        contactKeysDTO1.setId(null);
        assertThat(contactKeysDTO1).isNotEqualTo(contactKeysDTO2);
    }
}

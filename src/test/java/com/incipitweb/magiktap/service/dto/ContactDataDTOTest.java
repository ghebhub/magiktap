package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactDataDTO.class);
        ContactDataDTO contactDataDTO1 = new ContactDataDTO();
        contactDataDTO1.setId(1L);
        ContactDataDTO contactDataDTO2 = new ContactDataDTO();
        assertThat(contactDataDTO1).isNotEqualTo(contactDataDTO2);
        contactDataDTO2.setId(contactDataDTO1.getId());
        assertThat(contactDataDTO1).isEqualTo(contactDataDTO2);
        contactDataDTO2.setId(2L);
        assertThat(contactDataDTO1).isNotEqualTo(contactDataDTO2);
        contactDataDTO1.setId(null);
        assertThat(contactDataDTO1).isNotEqualTo(contactDataDTO2);
    }
}

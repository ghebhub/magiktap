package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactAgentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactAgentDTO.class);
        ContactAgentDTO contactAgentDTO1 = new ContactAgentDTO();
        contactAgentDTO1.setId(1L);
        ContactAgentDTO contactAgentDTO2 = new ContactAgentDTO();
        assertThat(contactAgentDTO1).isNotEqualTo(contactAgentDTO2);
        contactAgentDTO2.setId(contactAgentDTO1.getId());
        assertThat(contactAgentDTO1).isEqualTo(contactAgentDTO2);
        contactAgentDTO2.setId(2L);
        assertThat(contactAgentDTO1).isNotEqualTo(contactAgentDTO2);
        contactAgentDTO1.setId(null);
        assertThat(contactAgentDTO1).isNotEqualTo(contactAgentDTO2);
    }
}

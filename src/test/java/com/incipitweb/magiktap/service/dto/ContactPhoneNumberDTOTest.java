package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactPhoneNumberDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactPhoneNumberDTO.class);
        ContactPhoneNumberDTO contactPhoneNumberDTO1 = new ContactPhoneNumberDTO();
        contactPhoneNumberDTO1.setId(1L);
        ContactPhoneNumberDTO contactPhoneNumberDTO2 = new ContactPhoneNumberDTO();
        assertThat(contactPhoneNumberDTO1).isNotEqualTo(contactPhoneNumberDTO2);
        contactPhoneNumberDTO2.setId(contactPhoneNumberDTO1.getId());
        assertThat(contactPhoneNumberDTO1).isEqualTo(contactPhoneNumberDTO2);
        contactPhoneNumberDTO2.setId(2L);
        assertThat(contactPhoneNumberDTO1).isNotEqualTo(contactPhoneNumberDTO2);
        contactPhoneNumberDTO1.setId(null);
        assertThat(contactPhoneNumberDTO1).isNotEqualTo(contactPhoneNumberDTO2);
    }
}

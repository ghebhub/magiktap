package com.incipitweb.magiktap.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactMailAddressDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMailAddressDTO.class);
        ContactMailAddressDTO contactMailAddressDTO1 = new ContactMailAddressDTO();
        contactMailAddressDTO1.setId(1L);
        ContactMailAddressDTO contactMailAddressDTO2 = new ContactMailAddressDTO();
        assertThat(contactMailAddressDTO1).isNotEqualTo(contactMailAddressDTO2);
        contactMailAddressDTO2.setId(contactMailAddressDTO1.getId());
        assertThat(contactMailAddressDTO1).isEqualTo(contactMailAddressDTO2);
        contactMailAddressDTO2.setId(2L);
        assertThat(contactMailAddressDTO1).isNotEqualTo(contactMailAddressDTO2);
        contactMailAddressDTO1.setId(null);
        assertThat(contactMailAddressDTO1).isNotEqualTo(contactMailAddressDTO2);
    }
}

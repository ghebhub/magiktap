package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactMailAddressTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactMailAddress.class);
        ContactMailAddress contactMailAddress1 = new ContactMailAddress();
        contactMailAddress1.setId(1L);
        ContactMailAddress contactMailAddress2 = new ContactMailAddress();
        contactMailAddress2.setId(contactMailAddress1.getId());
        assertThat(contactMailAddress1).isEqualTo(contactMailAddress2);
        contactMailAddress2.setId(2L);
        assertThat(contactMailAddress1).isNotEqualTo(contactMailAddress2);
        contactMailAddress1.setId(null);
        assertThat(contactMailAddress1).isNotEqualTo(contactMailAddress2);
    }
}

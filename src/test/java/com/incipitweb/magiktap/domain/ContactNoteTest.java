package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactNoteTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactNote.class);
        ContactNote contactNote1 = new ContactNote();
        contactNote1.setId(1L);
        ContactNote contactNote2 = new ContactNote();
        contactNote2.setId(contactNote1.getId());
        assertThat(contactNote1).isEqualTo(contactNote2);
        contactNote2.setId(2L);
        assertThat(contactNote1).isNotEqualTo(contactNote2);
        contactNote1.setId(null);
        assertThat(contactNote1).isNotEqualTo(contactNote2);
    }
}

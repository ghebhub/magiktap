package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactPhoneNumberTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactPhoneNumber.class);
        ContactPhoneNumber contactPhoneNumber1 = new ContactPhoneNumber();
        contactPhoneNumber1.setId(1L);
        ContactPhoneNumber contactPhoneNumber2 = new ContactPhoneNumber();
        contactPhoneNumber2.setId(contactPhoneNumber1.getId());
        assertThat(contactPhoneNumber1).isEqualTo(contactPhoneNumber2);
        contactPhoneNumber2.setId(2L);
        assertThat(contactPhoneNumber1).isNotEqualTo(contactPhoneNumber2);
        contactPhoneNumber1.setId(null);
        assertThat(contactPhoneNumber1).isNotEqualTo(contactPhoneNumber2);
    }
}

package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactAgentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactAgent.class);
        ContactAgent contactAgent1 = new ContactAgent();
        contactAgent1.setId(1L);
        ContactAgent contactAgent2 = new ContactAgent();
        contactAgent2.setId(contactAgent1.getId());
        assertThat(contactAgent1).isEqualTo(contactAgent2);
        contactAgent2.setId(2L);
        assertThat(contactAgent1).isNotEqualTo(contactAgent2);
        contactAgent1.setId(null);
        assertThat(contactAgent1).isNotEqualTo(contactAgent2);
    }
}

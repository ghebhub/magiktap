package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactKeysTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactKeys.class);
        ContactKeys contactKeys1 = new ContactKeys();
        contactKeys1.setId(1L);
        ContactKeys contactKeys2 = new ContactKeys();
        contactKeys2.setId(contactKeys1.getId());
        assertThat(contactKeys1).isEqualTo(contactKeys2);
        contactKeys2.setId(2L);
        assertThat(contactKeys1).isNotEqualTo(contactKeys2);
        contactKeys1.setId(null);
        assertThat(contactKeys1).isNotEqualTo(contactKeys2);
    }
}

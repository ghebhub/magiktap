package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactXtendedTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactXtended.class);
        ContactXtended contactXtended1 = new ContactXtended();
        contactXtended1.setId(1L);
        ContactXtended contactXtended2 = new ContactXtended();
        contactXtended2.setId(contactXtended1.getId());
        assertThat(contactXtended1).isEqualTo(contactXtended2);
        contactXtended2.setId(2L);
        assertThat(contactXtended1).isNotEqualTo(contactXtended2);
        contactXtended1.setId(null);
        assertThat(contactXtended1).isNotEqualTo(contactXtended2);
    }
}

package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactCategoriesTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactCategories.class);
        ContactCategories contactCategories1 = new ContactCategories();
        contactCategories1.setId(1L);
        ContactCategories contactCategories2 = new ContactCategories();
        contactCategories2.setId(contactCategories1.getId());
        assertThat(contactCategories1).isEqualTo(contactCategories2);
        contactCategories2.setId(2L);
        assertThat(contactCategories1).isNotEqualTo(contactCategories2);
        contactCategories1.setId(null);
        assertThat(contactCategories1).isNotEqualTo(contactCategories2);
    }
}

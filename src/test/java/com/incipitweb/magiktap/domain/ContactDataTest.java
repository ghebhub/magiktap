package com.incipitweb.magiktap.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.incipitweb.magiktap.web.rest.TestUtil;

public class ContactDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactData.class);
        ContactData contactData1 = new ContactData();
        contactData1.setId(1L);
        ContactData contactData2 = new ContactData();
        contactData2.setId(contactData1.getId());
        assertThat(contactData1).isEqualTo(contactData2);
        contactData2.setId(2L);
        assertThat(contactData1).isNotEqualTo(contactData2);
        contactData1.setId(null);
        assertThat(contactData1).isNotEqualTo(contactData2);
    }
}
